use crate::ast::Spanned;
use crate::graph::Graph;
use itertools::Itertools;
use strum::VariantNames;

use std::io::ErrorKind;
use std::{thread, time};

use super::ast;
use super::ast::Node;
use super::graph;
use super::graph::{EdgeIdx, EdgesView, GraphEdge, GraphNode, NodeIdx, NodeView};
use super::token::Op;
use super::util::ScopedSymTable;
use ariadne::ReportBuilder;
use ariadne::{Color, ColorGenerator, Fmt, Label, Report, ReportKind, Source};
use chumsky::container::Container;

use std::cell::RefCell;
use std::collections::HashMap;
use std::error::Error;
use std::println;
use std::todo;
use std::unreachable;
use std::{
    collections::{hash_map::DefaultHasher, HashSet},
    fmt, print,
    rc::Rc,
};

#[derive(Debug)]
pub enum Port {
    Select(),
    Flow(u32),
}

#[derive(Debug, Clone)]
pub struct Signal<'a> {
    pub ident: &'a str,
    pub point: &'a Spanned<ast::Node>,
    pub id: Rc<RefCell<u32>>,
    // We retain a pointer to the SigUser node that needs this signal
    // in the signal use graph for error reporting.
    pub used_in: NodeIdx,
}

#[derive(Debug, Clone)]
struct SigUser<'a> {
    cst_node: NodeIdx,
    point: &'a Spanned<Node>,
}

// 'a is the lifetime of the AST that we will build from
#[derive(Debug)]
pub struct Channel<'a> {
    // TODO stop using HashMaps here. Easy implementation, but the number of allocations and
    // space requirements is horrible. Refactor to use arena.
    pub requires: HashMap<&'a str, Signal<'a>>,
    pub provides: HashMap<&'a str, Signal<'a>>,
    pub defines: HashSet<&'a str>,
    pub src_port: Port,
    pub dst_port: Port,
}

impl Channel<'_> {
    pub fn new() -> Self {
        Self {
            requires: HashMap::new(),
            provides: HashMap::new(),
            defines: HashSet::new(),
            src_port: Port::Flow(0),
            dst_port: Port::Flow(0),
        }
    }
}

#[derive(Debug)]
pub struct Controller<'a> {
    pub ast_node: &'a Spanned<ast::Node>, // will hold ast node index later
    pub sym_table: HashMap<&'a str, &'a Spanned<ast::Node>>,
    pub kind: ControllerKind<'a>,
    // TODO: replace these hashmaps.
    pub requires: HashMap<&'a str, Signal<'a>>,
    pub provides: HashMap<&'a str, Signal<'a>>,
}

impl<'a> Controller<'a> {
    fn new(
        ast_node: &'a Spanned<ast::Node>,
        sym_table: HashMap<&'a str, &'a Spanned<ast::Node>>,
        kind: ControllerKind<'a>,
    ) -> Self {
        Self {
            ast_node,
            sym_table,
            kind,
            requires: HashMap::new(),
            provides: HashMap::new(),
        }
    }
}

#[derive(Debug)]
pub enum ControllerKind<'a> {
    Merge {},
    Join {},
    Fork {},
    Mux {},
    Demux {},
    // TODO replace these hashmaps
    Reg {
        defs: HashMap<&'a str, Signal<'a>>,
    },
    NamedChan {
        defs: HashMap<&'a str, Signal<'a>>,
    },
    Sink {
        uses: HashMap<&'a str, Signal<'a>>,
    },
    Source {
        defs: HashMap<&'a str, Signal<'a>>,
    },
    Output {
        name: &'a str,
        uses: HashMap<&'a str, Signal<'a>>,
        blackbox_id: Option<u32>,
    },
    Input {
        name: &'a str,
        defs: HashMap<&'a str, Signal<'a>>,
        blackbox_id: Option<u32>,
    },
    Comb {
        defs: HashMap<&'a str, Signal<'a>>,
        uses: HashMap<&'a str, Signal<'a>>,
    },
    Blackbox {
        name: &'a str,
    },
}

impl<'a> ControllerKind<'a> {
    pub fn type_string(&self) -> &str {
        use ControllerKind::*;
        match self {
            Merge { .. } => "Merge",
            Join { .. } => "Join",
            Fork { .. } => "Fork",
            Mux { .. } => "Mux",
            Demux { .. } => "Demux",
            Reg { .. } => "Reg",
            Comb { .. } => "Comb",
            NamedChan { .. } => "NamedChan",
            Input { .. } => "Input",
            Output { .. } => "Output",
            Sink { .. } => "Sink",
            Source { .. } => "Source",
            Blackbox { .. } => "Blackbox",
        }
    }
}

#[derive(Clone)]
enum Shape<'a> {
    None,
    Node(NodeIdx),
    Aggr(Vec<PipeIo<'a>>),
}

#[derive(Clone)]
pub struct PipeIo<'a> {
    input: Shape<'a>,
    output: Shape<'a>,
    node: &'a Spanned<ast::Node>,
}

impl<'a> PipeIo<'a> {
    fn input_repr(&self) -> String {
        match &self.input {
            Shape::None => "None".to_string(),
            Shape::Node(_) => "c".to_string(),
            Shape::Aggr(inner) => {
                let element_repr = inner
                    .iter()
                    .map(PipeIo::input_repr)
                    .collect::<Vec<_>>()
                    .join(",");
                format!("[{element_repr}]")
            }
        }
    }

    fn output_repr(&self) -> String {
        match &self.output {
            Shape::None => "None".to_string(),
            Shape::Node(_) => "c".to_string(),
            Shape::Aggr(inner) => {
                let element_repr = inner
                    .iter()
                    .map(PipeIo::output_repr)
                    .collect::<Vec<_>>()
                    .join(",");
                format!("[{element_repr}]")
            }
        }
    }
}

pub type ControllerNode<'a> = GraphNode<Controller<'a>>;
pub type ChannelEdge<'a> = GraphEdge<Channel<'a>>;

#[derive(Debug)]
pub struct BuildErr {
    details: String,
}

impl BuildErr {
    fn new(msg: &str) -> BuildErr {
        BuildErr {
            details: msg.to_string(),
        }
    }
}

impl fmt::Display for BuildErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for BuildErr {
    fn description(&self) -> &str {
        &self.details
    }
}

struct CombBuilder<'a> {
    sym_table: ScopedSymTable<&'a Spanned<ast::Node>>,
}

pub struct CstBuilder<'a> {
    sym_table: ScopedSymTable<NodeIdx>,
    fun_table: ScopedSymTable<&'a Spanned<ast::Node>>,
    pub g: Graph<Controller<'a>, Channel<'a>>,
    pub diagnostics: Vec<ariadne::Report<'a>>,
    sig_uses: Graph<SigUser<'a>, ()>,
    id_counter: u32,
    blackbox_id_counter: u32,
    pub blackboxes: HashMap<u32, String>,
    parsing_blackbox: bool,
}

impl<'a> CstBuilder<'a> {
    pub fn new() -> Self {
        let mut builder = Self {
            sym_table: ScopedSymTable::new(),
            fun_table: ScopedSymTable::new(),
            g: Graph::new(),
            diagnostics: Vec::new(),
            sig_uses: Graph::new(),
            id_counter: 0,
            blackbox_id_counter: 0,
            blackboxes: HashMap::new(),
            parsing_blackbox: false,
        };

        builder.sym_table.enter_scope();
        builder.fun_table.enter_scope();

        builder
    }

    fn gen_match_error(&mut self, lhs: &PipeIo, rhs: &PipeIo) {
        self.diagnostics.push(
            Report::build(ReportKind::Error, (), lhs.node.span.start)
                .with_message(
                    "Cannot connect pipe expressions as input and output dimensions do not match",
                )
                .with_label(
                    Label::new(lhs.node.span.into())
                        .with_message(format!(
                            "This produces an output with shape {}",
                            lhs.output_repr()
                        ))
                        .with_color(Color::Red),
                )
                .with_label(
                    Label::new(rhs.node.span.into())
                        .with_message(format!(
                            "This expects an input with shape {}",
                            rhs.input_repr()
                        ))
                        .with_color(Color::Red),
                )
                .finish(),
        );
    }

    pub fn connect_pipes<'b>(
        &mut self,
        lhs: &PipeIo<'b>,
        rhs: &PipeIo<'b>,
        pipe_node: &'b Spanned<ast::Node>,
    ) -> Result<PipeIo<'b>, BuildErr> {
        use Shape::*;

        match lhs.output {
            None => {
                self.gen_match_error(lhs, rhs);
                Err(BuildErr {
                    details: "error provided in diagnostic".to_string(),
                })
            }
            Node(left_idx) => match rhs.input {
                Node(right_idx) => {
                    if left_idx == right_idx {
                        self.diagnostics.push(
                            Report::build(ReportKind::Error, (), lhs.node.span.start)
                                .with_message("Attempting to connect node to itself")
                                .with_label(
                                    Label::new(lhs.node.span.into())
                                        .with_message("This node")
                                        .with_color(Color::Red),
                                )
                                .with_label(
                                    Label::new(rhs.node.span.into())
                                        .with_message("Is the same as this node")
                                        .with_color(Color::Red),
                                )
                                .finish(),
                        );
                        Err(BuildErr {
                            details: "error provided in diagnostic".to_string(),
                        })
                    } else {
                        self.g.add_edge(left_idx, right_idx, Channel::new());
                        Ok(PipeIo {
                            input: lhs.input.clone(),
                            output: rhs.output.clone(),
                            node: pipe_node,
                        })
                    }
                }
                _ => {
                    self.gen_match_error(lhs, rhs);
                    Err(BuildErr {
                        details: "error provided in diagnostic".to_string(),
                    })
                }
            },
            Aggr(ref lhs_pipes) => match &rhs.input {
                Aggr(rhs_pipes) => {
                    if rhs_pipes.len() != lhs_pipes.len() {
                        self.gen_match_error(lhs, rhs);
                        Err(BuildErr {
                            details: "error provided in diagnostic".to_string(),
                        })
                    } else {
                        lhs_pipes
                            .iter()
                            .zip(rhs_pipes.iter())
                            .map(|(lhs_pipe, rhs_pipe)| {
                                self.connect_pipes(lhs_pipe, rhs_pipe, pipe_node)
                            })
                            .collect::<Result<Vec<_>, _>>()
                            .map(|res| PipeIo {
                                input: lhs.input.clone(),
                                output: rhs.output.clone(),
                                node: pipe_node,
                            })
                    }
                }
                _ => {
                    self.gen_match_error(lhs, rhs);
                    Err(BuildErr {
                        details: "error provided in diagnostic".to_string(),
                    })
                }
            },
        }
    }

    // This function is called with a comb node in the cst construction process
    // in order to find the signals that this node requires to have available
    // on its input and which signals this node defines.
    fn collect_comb_needs_defines(
        node: &'a ast::Spanned<ast::Node>,
        uses_defs: &mut Vec<(&'a str, &'a Spanned<Node>)>,
    ) {
        use ast::Node;

        match &node.inner {
            Node::Stmt {} => {}
            Node::CompoundStmt { stmts } => {
                stmts
                    .iter()
                    .for_each(|stmt| Self::collect_comb_needs_defines(stmt, uses_defs));
            }
            Node::BinOpExpr { op, lhs, rhs } => {
                Self::collect_comb_needs_defines(rhs, uses_defs);
                Self::collect_comb_needs_defines(lhs, uses_defs);
            }
            Node::NumLitExpr { val } => {}
            Node::VarExpr { name } => {
                uses_defs.push((name.as_str(), node));
            }
            Node::Slice { high, low } => {
                Self::collect_comb_needs_defines(high, uses_defs);
                Self::collect_comb_needs_defines(low, uses_defs);
            }
            Node::SliceExpr { expr, slice } => {
                Self::collect_comb_needs_defines(expr, uses_defs);
                Self::collect_comb_needs_defines(slice, uses_defs);
            }
            Node::AggrExpr { exprs } => {
                unreachable!("Illegal aggr expr in comb node should have been caught earlier")
            }
            Node::TypeSpec { ident, slice } => {}
            Node::ChanDeclExpr { name, sigs } => {
                unreachable!("chan decl cannot exist in comb");
            }
            Node::AggrType { typespec } => {}
            Node::FunDeclExpr {
                name,
                inputs,
                sides,
                outputs,
                body,
            } => {}
            Node::IfStmt {
                cond,
                body,
                elsebody,
            } => {
                Self::collect_comb_needs_defines(cond, uses_defs);
                Self::collect_comb_needs_defines(body, uses_defs);
                if let Some(elsebody) = elsebody {
                    Self::collect_comb_needs_defines(elsebody, uses_defs);
                }
            }
            Node::FunCall { name, args } => {}
            Node::Comb { stmt } => {
                unreachable!("Illegal comb in comb node should have been caught earlier")
            }
            Node::SigDeclExpr {
                ident,
                typespec,
                default,
                slice,
            } => {
                // TODO: Possible bug here when using constants for default init which
                // aren't signals? Can impact scope resolution
                if let Some(default) = default {
                    Self::collect_comb_needs_defines(default, uses_defs);
                }
                uses_defs.push((ident.as_str(), node));
            }
            Node::ErrorNode {} => {
                todo!()
            }
            _ => todo!(),
        }
    }

    fn prop_detail(&mut self) -> bool {
        let mut updated = false;
        for i in 0..self.g.nodes.len() {
            let node = &mut self.g.node(NodeIdx(i));

            use ControllerKind::*;

            let update_id = |from: &Signal<'a>, to: &Signal<'a>| {
                if *from.id.borrow() != *to.id.borrow() {
                    from.id.replace(*to.id.borrow());
                    true
                } else {
                    false
                }
            };

            // For the controllers with multiple/single inputs and a single output
            // simply duplicate the needs from the outputs onto the inputs.
            // For the controllers with multiple outputs (fork, demux) we create
            // a new signal on the input for every new signal we observe on the
            // outputs

            // Need this to serve as a reference to empty set
            let empty_placeholder: HashMap<&'a str, Signal<'a>> = HashMap::new();

            let (defs, uses) = match &node.data.kind {
                Comb { defs, uses } => (defs, uses),
                NamedChan { defs } => (defs, defs),
                Sink { uses } => (&empty_placeholder, uses),
                Source { defs } => (defs, &empty_placeholder),
                Output {
                    name,
                    uses,
                    blackbox_id,
                } => (&empty_placeholder, uses),
                Input {
                    name,
                    defs,
                    blackbox_id,
                } => (defs, &empty_placeholder),
                Reg { defs } => (defs, defs),
                Mux {} | Demux {} => {
                    // find the input select edge. If there is none, it is an error.

                    let mut sel_edges: Vec<_> = node
                        .edges
                        .incs_iter_mut()
                        .filter(|edge| matches!(edge.data.dst_port, Port::Select()))
                        .collect();

                    if sel_edges.len() != 1 {
                        self.diagnostics
				.push(Report::build(ReportKind::Error, (), node.data.ast_node.span.start)
				      .with_message("Demux and Mux nodes require a select input edge, but none was found")
				      .finish());
                        todo!("Missing select edge error not handled yet");
                    }

                    let sel_edge = &mut sel_edges[0];
                    if let Some(existing_need) = sel_edge.data.requires.get("s") {
                    } else {
                        sel_edge.data.requires.insert(
                            "s",
                            Signal {
                                ident: "s",
                                point: node.data.ast_node,
                                id: Rc::new(RefCell::new(self.id_counter)),
                                used_in: NodeIdx(0),
                            },
                        );
                    }

                    // place the s signal need on this edge

                    (&empty_placeholder, &empty_placeholder)
                }
                _ => (&empty_placeholder, &empty_placeholder),
            };

            let can_provide = match &node.data.kind {
                Join {} => {
                    let mut can_provide: HashMap<&'a str, (Signal<'a>, usize)> = HashMap::new();
                    let mut edge_idx = 0;
                    node.edges.incs_iter().for_each(|edge| {
                        edge.data.provides.iter().for_each(|(name, sig)| {
                            if let Some((existing_sig, idx)) =
                                can_provide.insert(*name, (sig.clone(), edge_idx))
                            {
                                // we inserted a signal that we have already seen on one of the
                                // join inputs. This is an error because the elements on the join
                                // must be mutually exclusive.
                                self.diagnostics.push(
					Report::build(ReportKind::Error, (), node.data.ast_node.span.start)
					    .with_message("Input signals to a join component must be mutually exclusive")
					    .with_label(Label::new(node.data.ast_node.span.into())
							.with_message(
							    format!("A signal named {} exists on both inputs", *name)))
					    .with_label(Label::new(sig.point.span.into())
							.with_message("Defined once here".to_string()))
					    .with_label(Label::new(existing_sig.point.span.into())
							.with_message("And once here".to_string()))
					    .finish()
				    );
                                todo!("Fix returns on error")
                            }
                        });

                        edge_idx += 1;
                    });

                    // Now we know that we have collected only the provided signals which are
                    // provided by exactly one input arm of the join. We proceed with removing
                    // any needs of this signal on all the other arms.
                    edge_idx = 0;
                    node.edges.incs_iter_mut().for_each(|edge| {
                        can_provide
                            .iter()
                            .filter(|(name, (sig, idx))| *idx != edge_idx)
                            .for_each(|(name, (sig, idx))| {
                                edge.data.requires.remove(*name);
                            });
                        edge_idx += 1;
                    });

                    can_provide
                        .into_iter()
                        .map(|(name, (sig, idx))| (name, sig))
                        .collect()
                }
                _ => {
                    let mut can_provide: HashMap<&'a str, Signal<'a>> = HashMap::new();

                    node.edges.incs_iter_mut().for_each(|edge| {
                        edge.data.provides.iter().for_each(|(name, sig)| {
                            if let Some(existing_prov) = can_provide.get(*name) {
                                update_id(sig, existing_prov);
                            } else {
                                can_provide.insert(*name, sig.clone());
                            }
                        });
                    });

                    can_provide.extend(defs.iter().map(|(name, sig)| (*name, sig.clone())));

                    can_provide
                }
            };

            node.data.requires.iter().for_each(|(name, sig)| {
                if let Some(prov) = &mut can_provide.get(*name) {
                    updated |= update_id(sig, prov);
                    node.data.provides.insert(name, prov.clone());
                } else {
                    node.edges
                        .incs_iter_mut()
                        .filter(|edge| matches!(edge.data.dst_port, Port::Flow(..)))
                        .for_each(|edge| {
                            if let Some(existing_need) = edge.data.requires.get(*name) {
                                updated |= update_id(existing_need, sig);
                            } else {
                                updated = true;
                                edge.data.requires.insert(name, sig.clone());
                            }
                        });
                }
            });

            let post_needs: HashSet<&str> = node
                .edges
                .outs_iter()
                .flat_map(|edge| edge.data.requires.iter().map(|(name, sig)| *name))
                .collect();

            let needs_to_remove: Vec<_> = node
                .data
                .requires
                .iter()
                .filter(|(name, sig)| !(post_needs.contains(*name) || uses.contains_key(*name)))
                .map(|(name, sig)| *name)
                .collect();

            needs_to_remove.iter().for_each(|name| {
                node.data.requires.remove(name);
            });

            node.edges.outs_iter_mut().for_each(|edge| {
                edge.data.requires.iter().for_each(|(name, sig)| {
                    if let Some(prov) = &mut edge.data.provides.get(*name) {
                        updated |= update_id(sig, prov);
                    } else if let Some(prov) = &mut node.data.provides.get(*name) {
                        updated |= update_id(sig, prov);
                        edge.data.provides.insert(name, prov.clone());
                    } else if let Some(existing_need) = &mut node.data.requires.get(*name) {
                        updated |= update_id(sig, existing_need);
                    } else {
                        updated = true;
                        node.data.requires.insert(name, sig.clone());
                    }
                })
            });

            uses.iter().for_each(|(name, sig)| {
                node.edges
                    .incs_iter_mut()
                    .filter(|edge| matches!(edge.data.dst_port, Port::Flow(..)))
                    .for_each(|edge| {
                        if let Some(existing_need) = edge.data.requires.get(*name) {
                            updated |= update_id(existing_need, sig);
                        } else {
                            updated = true;
                            edge.data.requires.insert(name, sig.clone());
                        }
                    });
            });
        }

        updated
    }

    pub fn propagate2(&mut self) -> bool {
        let mut updated = true;
        while updated {
            updated = false;
            updated |= self.prop_detail();
            updated |= self.prop_detail();
        }

        updated
    }

    fn propagate(&mut self) -> bool {
        let mut updated = true;
        while updated {
            updated = false;
            dbg!(&self.g.nodes[2]);
            for i in 0..self.g.nodes.len() {
                let node = &mut self.g.node(NodeIdx(i));

                use ControllerKind::*;

                // For the controllers with multiple/single inputs and a single output
                // simply duplicate the needs from the outputs onto the inputs.
                // For the controllers with multiple outputs (fork, demux) we create
                // a new signal on the input for every new signal we observe on the
                // outputs

                // Need this to serve as a reference to empty set
                let empty_placeholder: HashMap<&'a str, Signal<'a>> = HashMap::new();

                let (defs, uses) = match &node.data.kind {
                    Comb { defs, uses } => (defs, uses),
                    NamedChan { defs } => (defs, defs),
                    Sink { uses } => (&empty_placeholder, uses),
                    Source { defs } => (defs, &empty_placeholder),
                    Output {
                        name,
                        uses,
                        blackbox_id,
                    } => (&empty_placeholder, uses),
                    Input {
                        name,
                        defs,
                        blackbox_id,
                    } => (defs, &empty_placeholder),
                    _ => todo!(),
                };

                let mut unmet_uses: HashMap<&'a str, Signal<'a>> = node
                    .edges
                    .incs_iter()
                    .flat_map(|edge| {
                        uses.iter()
                            .filter(|&(name, sig)| !edge.data.provides.contains_key(name))
                    })
                    .map(|(name, sig)| (*name, sig.clone()))
                    .collect();

                unmet_uses.iter_mut().for_each(|(name, unmet_sig)| {
                    node.edges.incs_iter_mut().for_each(|edge| {
                        if let Some(existing_need) = &mut edge.data.requires.get(*name) {
                            if *unmet_sig.id.borrow() != *existing_need.id.borrow() {
                                updated = true;
                            }
                            unmet_sig.id.replace(*existing_need.id.borrow());
                        } else {
                            updated = true;
                            edge.data.requires.insert(name, unmet_sig.clone());
                        }
                    })
                });

                // First we get just the unmet needs as this will make the rest a bit
                // easier.
                let mut unmet: HashMap<&'a str, Signal<'a>> = node
                    .edges
                    .outs_iter()
                    .flat_map(|edge| {
                        edge.data
                            .requires
                            .iter()
                            .filter(|&(name, sig)| !edge.data.provides.contains_key(name))
                    })
                    .map(|(name, sig)| (*name, sig.clone()))
                    .collect();

                let mut met: HashMap<&'a str, Signal<'a>> = HashMap::new();

                unmet.iter_mut().for_each(|(name, sig)| {
                    if let Some(def) = &mut defs.get(*name) {
                        if *sig.id.borrow() != *def.id.borrow() {
                            updated = true;
                            met.insert(*name, def.clone());
                        }
                        sig.id.replace(*def.id.borrow());
                    } else {
                        node.edges.incs_iter_mut().for_each(|edge| {
                            if let Some(need) = &mut edge.data.requires.get(*name) {
                                if *sig.id.borrow() != *need.id.borrow() {
                                    updated = true;
                                }
                                sig.id.replace(*need.id.borrow());
                            } else {
                                updated = true;
                                edge.data.requires.insert(name, sig.clone());
                            }

                            if let Some(prov) = &mut edge.data.provides.get(*name) {
                                updated = true;
                                met.insert(*name, prov.clone());
                            }
                        })
                    }
                });
                met.iter().for_each(|(name, sig)| {
                    node.edges.outs_iter_mut().for_each(|edge| {
                        edge.data.provides.insert(*name, sig.clone());
                        edge.data
                            .requires
                            .get(*name)
                            .unwrap()
                            .id
                            .replace(*sig.id.borrow());
                    });
                });
            }
            thread::sleep(time::Duration::from_secs(1));
        }

        updated
    }

    fn build_from_comb_node(&mut self, node: &'a ast::Spanned<ast::Node>) -> NodeIdx {
        use ast::Node;

        let mut uses_defs = Vec::<(&'a str, &'a Spanned<Node>)>::new();

        Self::collect_comb_needs_defines(node, &mut uses_defs);

        let (uses, defs) = uses_defs
            .iter()
            .unique_by(|(name, node)| name)
            .map(|(name, node)| {
                (
                    *name,
                    Signal {
                        ident: name,
                        point: node,
                        id: Rc::new(RefCell::new(self.id_counter)),
                        used_in: NodeIdx(0),
                    },
                )
            })
            .partition(|(name, sig)| matches!(sig.point.inner, Node::VarExpr { .. }));

        self.id_counter += 1;

        // TODO: find a better way than just putting in the node and taking
        // a ref back to it to insert controller node idx as the point of use.
        let cst_idx = self.g.add_node(Controller::new(
            node,
            HashMap::new(),
            ControllerKind::Comb { uses, defs },
        ));

        let cst_node = &mut self.g[cst_idx].data;
        if let ControllerKind::Comb { defs, uses } = &mut cst_node.kind {
            defs.iter_mut().chain(uses.iter_mut()).for_each(|def| {
                let sig_use_idx = self.sig_uses.add_node(SigUser {
                    cst_node: cst_idx,
                    point: def.1.point,
                });
                def.1.used_in = sig_use_idx
            });
        } else {
            unreachable!("Extracted CST node cannot be any other kind than Comb")
        }

        cst_idx
    }

    pub fn build_from_structural_node(
        &mut self,
        node: &'a ast::Spanned<ast::Node>,
    ) -> Result<PipeIo<'a>, BuildErr> {
        use ast::Node;

        // TODO don't use this hacky method
        let aggr_from_idx = |idx| {
            Shape::Aggr(vec![
                PipeIo {
                    input: Shape::Node(idx),
                    output: Shape::Node(idx),
                    node,
                },
                PipeIo {
                    input: Shape::Node(idx),
                    output: Shape::Node(idx),
                    node,
                },
            ])
        };

        match &node.inner {
            Node::Stmt {} => unreachable!("Stmts aren't used"),
            Node::CompoundStmt { stmts } => {
                self.sym_table.enter_scope();
                stmts.iter().for_each(|x| {
                    let _ = self.build_from_structural_node(x);
                });
                Ok(PipeIo {
                    input: Shape::None,
                    output: Shape::None,
                    node,
                })
            }
            Node::BinOpExpr {
                op,
                ref lhs,
                ref rhs,
            } => {
                if let Op::Pipe = op {
                    let lhs_shape = self.build_from_structural_node(lhs)?;
                    let rhs_shape = self.build_from_structural_node(rhs)?;
                    self.connect_pipes(&lhs_shape, &rhs_shape, node)
                } else {
                    unreachable!("Non-pipe binary op should not have parsed!");
                }
            }
            Node::NumLitExpr { val } => todo!(),
            Node::VarExpr { name } => {
                let sym = self.sym_table.get(name);
                if let Some(sym_idx) = sym {
                    Ok(PipeIo {
                        input: Shape::Node(*sym_idx),
                        output: Shape::Node(*sym_idx),
                        node,
                    })
                } else {
                    let report = Report::build(ReportKind::Error, (), node.span.start)
                        .with_message("Undefined channel or signal")
                        .with_label(
                            Label::new(node.span.into())
                                .with_message("Not previously defined")
                                .with_color(Color::Red),
                        )
                        .finish();
                    self.diagnostics.push(report);
                    Err(BuildErr {
                        details: "error provided in diagnostic".to_string(),
                    })
                }
            }
            Node::Slice { high, low } => {
                todo!()
            }
            Node::AggrExpr { exprs } => {
                let res: Result<Vec<_>, _> = exprs
                    .iter()
                    .map(|expr| self.build_from_structural_node(expr))
                    .collect();

                res.map_or(
                    Err(BuildErr {
                        details: "error provided in diagnostic".to_string(),
                    }),
                    |exprs| {
                        Ok(PipeIo {
                            input: Shape::Aggr(exprs.clone()),
                            output: Shape::Aggr(exprs),
                            node,
                        })
                    },
                )
            }
            Node::TypeSpec { ident, slice } => {
                todo!()
            }
            Node::ChanDeclExpr { name, sigs } => {
                // Channel declarations create a named edge in the cg that we can refer to later
                // The Channel declaration may have a type constraint.
                // If the type constraint exists we are going to verify that the type is indeed
                // a list of signal declarations with further optional type specifications.
                // For Channels it is not allowed to provide default values so we check that as well
                let (defines, sym_table) = sigs
                    .as_ref()
                    .map(|typespec| {
                        let mut sym_table = HashMap::new();
                        let mut defines = HashMap::new();
                        typespec.iter().for_each(|sig| {
                            if let Node::SigDeclExpr {
                                ident,
                                typespec,
                                default,
                                slice,
                            } = &sig.inner
                            {
                                // TODO fix ugly hack self.g.nodes.len() use here
                                defines.insert(
                                    ident.as_str(),
                                    Signal {
                                        ident: ident.as_str(),
                                        point: sig,
                                        id: Rc::new(RefCell::new(self.id_counter)),
                                        used_in: NodeIdx(self.g.nodes.len()),
                                    },
                                );
                                self.id_counter += 1;
                                sym_table.insert(ident.as_str(), sig);
                            }
                        });
                        (defines, sym_table)
                    })
                    .unwrap_or_default();

                let chan =
                    Controller::new(node, sym_table, ControllerKind::NamedChan { defs: defines });

                let sym = self.g.add_node(chan);
                self.sym_table.insert_sym(name.to_string(), sym);

                Ok(PipeIo {
                    input: Shape::Node(sym),
                    output: Shape::Node(sym),
                    node,
                })
            }
            Node::AggrType { typespec } => {
                todo!()
            }
            Node::FunDeclExpr {
                name,
                inputs,
                sides,
                outputs,
                body,
            } => {
                self.fun_table.insert_sym(name.clone(), node);
                Ok(PipeIo {
                    input: Shape::None,
                    output: Shape::None,
                    node,
                })
            }
            Node::IfStmt {
                cond,
                body,
                elsebody,
            } => {
                todo!()
            }
            Node::FunCall { name, args } => {
                // First we have to find the function
                if let Some(Spanned::<ast::Node> {
                    inner:
                        ast::Node::FunDeclExpr {
                            name,
                            inputs,
                            sides,
                            outputs,
                            body,
                        },
                    span: decl_span,
                }) = self.fun_table.get(name)
                {
                    // stamp out the channel I/O, push them to a new spoke and elaborate the component
                    // in that scope
                    self.sym_table.enter_scope();
                    let inputs = inputs
                        .iter()
                        .map(|input| self.build_from_structural_node(input))
                        .collect::<Result<Vec<_>, _>>()?;

                    let outputs = outputs
                        .iter()
                        .map(|output| self.build_from_structural_node(output))
                        .collect::<Result<Vec<_>, _>>()?;

                    let side_chans = sides
                        .iter()
                        .map(|side| self.build_from_structural_node(side))
                        .collect::<Result<Vec<_>, _>>()?;

                    let arg_chans = args
                        .iter()
                        .map(|arg| self.build_from_structural_node(arg))
                        .collect::<Result<Vec<_>, _>>()?;

                    if side_chans.len() != arg_chans.len() {
                        self.diagnostics.push(
                            Report::build(ReportKind::Error, (), decl_span.start)
                                .with_message("Component side channel mismatch")
                                .with_label(
                                    Label::new(node.span.into())
                                        .with_message(format!(
                                            "Component instantiated here with {} side channels",
                                            side_chans.len()
                                        ))
                                        .with_color(Color::Red),
                                )
                                .with_label(
                                    Label::new(
                                        sides
                                            .first()
                                            .map_or(decl_span.start + name.len(), |x| x.span.start)
                                            ..sides // range ..
                                                .last()
                                                .map_or(decl_span.start + name.len(), |x| {
                                                    x.span.end
                                                }),
                                    )
                                    .with_message(format!(
                                        "Component expects {} side channels defined here",
                                        sides.len()
                                    )),
                                )
                                .finish(),
                        );
                        return Err(BuildErr {
                            details: "error provided in diagnostic".to_string(),
                        });
                    }

                    let side_pipes = arg_chans
                        .iter()
                        .zip(side_chans.iter())
                        .map(|(arg_chan, side_chan)| self.connect_pipes(arg_chan, side_chan, node))
                        .collect::<Result<Vec<_>, _>>()?;

                    self.build_from_structural_node(body)?;

                    self.sym_table.leave_scope();

                    let input: Vec<_> = inputs
                        .iter()
                        .map(|x| PipeIo {
                            input: x.input.clone(),
                            output: x.input.clone(),
                            node,
                        })
                        .collect();

                    let input = if input.is_empty() {
                        Shape::None
                    } else if input.len() == 1 {
                        input[0].input.clone()
                    } else {
                        Shape::Aggr(input)
                    };

                    let output: Vec<_> = outputs
                        .iter()
                        .map(|x| PipeIo {
                            input: x.input.clone(),
                            output: x.output.clone(),
                            node,
                        })
                        .collect();

                    let output = if output.is_empty() {
                        Shape::None
                    } else if output.len() == 1 {
                        output[0].output.clone()
                    } else {
                        Shape::Aggr(output)
                    };

                    Ok(PipeIo {
                        input,
                        output,
                        node,
                    })
                } else {
                    // TODO handle builtins in a different way
                    match name.as_str() {
                        "join" => {
                            let idx = self.g.add_node(Controller::new(
                                node,
                                HashMap::new(),
                                ControllerKind::Join {},
                            ));

                            Ok(PipeIo {
                                input: aggr_from_idx(idx),
                                output: Shape::Node(idx),
                                node,
                            })
                        }
                        "merge" => {
                            let idx = self.g.add_node(Controller::new(
                                node,
                                HashMap::new(),
                                ControllerKind::Merge {},
                            ));

                            Ok(PipeIo {
                                input: aggr_from_idx(idx),
                                output: Shape::Node(idx),
                                node,
                            })
                        }
                        "fork" => {
                            let idx = self.g.add_node(Controller::new(
                                node,
                                HashMap::new(),
                                ControllerKind::Fork {},
                            ));

                            Ok(PipeIo {
                                input: Shape::Node(idx),
                                output: aggr_from_idx(idx),
                                node,
                            })
                        }
                        "mux" => {
                            let arg_chans = args
                                .iter()
                                .map(|arg| self.build_from_structural_node(arg))
                                .collect::<Result<Vec<_>, _>>()?;

                            let mux_idx = self.g.add_node(Controller::new(
                                node,
                                HashMap::new(),
                                ControllerKind::Mux {},
                            ));

                            if arg_chans.len() != 1 {
                                self.diagnostics.push(
				Report::build(ReportKind::Error, (), node.span.start)
				    .with_message("Component side channel mismatch")
				    .with_label(
					Label::new(node.span.into())
					    .with_message(format!(
						"Component instantiated here with {} side channels",
						arg_chans.len()
					    ))
					    .with_color(Color::Red),
				    )
					.with_note("Builtin mux() expects exactly 1 side channel to be used as input select")
				    .finish(),);

                                Err(BuildErr {
                                    details: "error provided in diagnostic".to_string(),
                                })
                            } else if let Shape::Node(arg_idx) = arg_chans[0].input {
                                let mut chan = Channel::new();
                                chan.dst_port = Port::Select();
                                self.g.add_edge(arg_idx, mux_idx, chan);

                                Ok(PipeIo {
                                    input: aggr_from_idx(mux_idx),
                                    output: Shape::Node(mux_idx),
                                    node,
                                })
                            } else {
                                self.gen_match_error(
                                    &arg_chans[0],
                                    &PipeIo {
                                        input: Shape::Node(mux_idx),
                                        output: Shape::None,
                                        node,
                                    },
                                );
                                return Err(BuildErr {
                                    details: "error provided in diagnostic".to_string(),
                                });
                            }
                        }
                        "demux" => {
                            let arg_chans = args
                                .iter()
                                .map(|arg| self.build_from_structural_node(arg))
                                .collect::<Result<Vec<_>, _>>()?;

                            let mux_idx = self.g.add_node(Controller::new(
                                node,
                                HashMap::new(),
                                ControllerKind::Demux {},
                            ));

                            if arg_chans.len() != 1 {
                                self.diagnostics.push(
				Report::build(ReportKind::Error, (), node.span.start)
				    .with_message("Component side channel mismatch")
				    .with_label(
					Label::new(node.span.into())
					    .with_message(format!(
						"Component instantiated here with {} side channels",
						arg_chans.len()
					    ))
					    .with_color(Color::Red),
				    )
					.with_note("Builtin demux() expects exactly 1 side channel to be used as output select")
				    .finish(),);

                                Err(BuildErr {
                                    details: "error provided in diagnostic".to_string(),
                                })
                            } else if let Shape::Node(arg_idx) = arg_chans[0].input {
                                let mut chan = Channel::new();
                                chan.dst_port = Port::Select();
                                self.g.add_edge(arg_idx, mux_idx, chan);

                                Ok(PipeIo {
                                    input: Shape::Node(mux_idx),
                                    output: aggr_from_idx(mux_idx),
                                    node,
                                })
                            } else {
                                self.gen_match_error(
                                    &arg_chans[0],
                                    &PipeIo {
                                        input: Shape::Node(mux_idx),
                                        output: Shape::None,
                                        node,
                                    },
                                );
                                return Err(BuildErr {
                                    details: "error provided in diagnostic".to_string(),
                                });
                            }
                        }
                        "reg" => {
                            let (defines, sym_table) = {
                                let mut sym_table = HashMap::new();
                                let mut defines = HashMap::new();
                                args.iter().for_each(|sig| {
                                    if let Node::SigDeclExpr {
                                        ident,
                                        typespec,
                                        default,
                                        slice,
                                    } = &sig.inner
                                    {
                                        defines.insert(
                                            ident.as_str(),
                                            Signal {
                                                ident: ident.as_str(),
                                                point: sig,
                                                id: Rc::new(RefCell::new(self.id_counter)),
                                                used_in: NodeIdx(self.g.nodes.len()),
                                            },
                                        );
                                        self.id_counter += 1;
                                        sym_table.insert(ident.as_str(), sig);
                                    }
                                });
                                (defines, sym_table)
                            };

                            let chan = Controller::new(
                                node,
                                sym_table,
                                ControllerKind::Reg { defs: defines },
                            );

                            let sym = self.g.add_node(chan);
                            self.sym_table.insert_sym(name.to_string(), sym);

                            Ok(PipeIo {
                                input: Shape::Node(sym),
                                output: Shape::Node(sym),
                                node,
                            })
                        }
                        "blackbox" => {
                            self.parsing_blackbox = true;
                            if args.len() < 3 {
                                todo!("Report error for incorrect blackbox usage");
                            }
                            let name_expr = &args[0];
                            let ast::Node::VarExpr { name } = &name_expr.inner
                            else {
                                todo!("Report error for blackbox")
                            };

                            let outputs = args
                                .iter()
                                .skip(1)
                                .filter_map(|output| {
                                    if let Node::InputDecl { chan } = &output.inner {
                                        Some(self.build_from_structural_node(output))
                                    } else {
                                        None
                                    }
                                })
                                .collect::<Result<Vec<_>, _>>()?;

                            let inputs = args
                                .iter()
                                .skip(1)
                                .filter_map(|output| {
                                    if let Node::OutputDecl { chan } = &output.inner {
                                        Some(self.build_from_structural_node(output))
                                    } else {
                                        None
                                    }
                                })
                                .collect::<Result<Vec<_>, _>>()?;

                            let input: Vec<_> = inputs
                                .iter()
                                .map(|x| PipeIo {
                                    input: x.input.clone(),
                                    output: x.input.clone(),
                                    node,
                                })
                                .collect();

                            let input = if input.is_empty() {
                                Shape::None
                            } else if input.len() == 1 {
                                input[0].input.clone()
                            } else {
                                Shape::Aggr(input)
                            };

                            let output: Vec<_> = outputs
                                .iter()
                                .map(|x| PipeIo {
                                    input: x.input.clone(),
                                    output: x.output.clone(),
                                    node,
                                })
                                .collect();

                            let output = if output.is_empty() {
                                Shape::None
                            } else if output.len() == 1 {
                                output[0].output.clone()
                            } else {
                                Shape::Aggr(output)
                            };

                            self.parsing_blackbox = false;
                            self.blackbox_id_counter += 1;

                            Ok(PipeIo {
                                input,
                                output,
                                node,
                            })
                        }
                        _ => {
                            self.diagnostics.push(
                                Report::build(ReportKind::Error, (), node.span.start)
                                    .with_message("No such component")
                                    .with_label(
                                        Label::new(node.span.into())
                                            .with_message("This component has not been declared")
                                            .with_color(Color::Red),
                                    )
                                    .finish(),
                            );

                            Err(BuildErr {
                                details: "error provided in diagnostic".to_string(),
                            })
                        }
                    }
                }

                // We stamp out the function template and get the
                // input/side/output nodes

                // Connect the side nodes here since they are provided as args
            }
            Node::Comb { stmt } => {
                let comb_idx = self.build_from_comb_node(stmt);
                Ok(PipeIo {
                    input: Shape::Node(comb_idx),
                    output: Shape::Node(comb_idx),
                    node: stmt,
                })
            }
            Node::SigDeclExpr {
                ident,
                typespec,
                default,
                slice,
            } => {
                todo!()
            }
            Node::ErrorNode {} => {
                todo!()
            }
            Node::InputDecl { chan } => {
                if let ast::Node::ChanDeclExpr { name, sigs } = &chan.inner {
                    let (defines, sym_table) = sigs
                        .as_ref()
                        .map(|typespec| {
                            let mut sym_table = HashMap::new();
                            let mut defines = HashMap::new();
                            typespec.iter().for_each(|sig| {
                                if let Node::SigDeclExpr {
                                    ident,
                                    typespec,
                                    default,
                                    slice,
                                } = &sig.inner
                                {
                                    defines.insert(
                                        ident.as_str(),
                                        Signal {
                                            ident: ident.as_str(),
                                            point: sig,
                                            id: Rc::new(RefCell::new(self.id_counter)),
                                            used_in: NodeIdx(self.g.nodes.len()),
                                        },
                                    );
                                    self.id_counter += 1;
                                    sym_table.insert(ident.as_str(), sig);
                                }
                            });
                            (defines, sym_table)
                        })
                        .unwrap_or_default();

                    let chan = Controller::new(
                        node,
                        sym_table,
                        ControllerKind::Input {
                            name,
                            defs: defines,
                            blackbox_id: if self.parsing_blackbox {
                                Some(self.blackbox_id_counter)
                            } else {
                                None
                            },
                        },
                    );

                    let sym = self.g.add_node(chan);
                    self.sym_table.insert_sym(name.to_string(), sym);

                    Ok(PipeIo {
                        input: Shape::Node(sym),
                        output: Shape::Node(sym),
                        node,
                    })
                } else {
                    Err(BuildErr {
                        details: "error provided in diagnostic".to_string(),
                    })
                }
            }
            Node::OutputDecl { chan } => {
                if let ast::Node::ChanDeclExpr { name, sigs } = &chan.inner {
                    let (uses, sym_table) = sigs
                        .as_ref()
                        .map(|typespec| {
                            let mut sym_table = HashMap::new();
                            let mut uses = HashMap::new();
                            typespec.iter().for_each(|sig| {
                                if let Node::SigDeclExpr {
                                    ident,
                                    typespec,
                                    default,
                                    slice,
                                } = &sig.inner
                                {
                                    uses.insert(
                                        ident.as_str(),
                                        Signal {
                                            ident: ident.as_str(),
                                            point: sig,
                                            id: Rc::new(RefCell::new(self.id_counter)),
                                            used_in: NodeIdx(self.g.nodes.len()),
                                        },
                                    );
                                    sym_table.insert(ident.as_str(), sig);
                                }
                            });
                            (uses, sym_table)
                        })
                        .unwrap_or_default();

                    let chan = Controller::new(
                        node,
                        sym_table,
                        ControllerKind::Output {
                            name,
                            uses,
                            blackbox_id: if self.parsing_blackbox {
                                Some(self.blackbox_id_counter)
                            } else {
                                None
                            },
                        },
                    );

                    let sym = self.g.add_node(chan);
                    self.sym_table.insert_sym(name.to_string(), sym);

                    Ok(PipeIo {
                        input: Shape::Node(sym),
                        output: Shape::Node(sym),
                        node,
                    })
                } else {
                    Err(BuildErr {
                        details: "error provided in diagnostic".to_string(),
                    })
                }
            }
            _ => todo!("{:?}", node),
        }
    }
}

fn print_dot_controller(c: &ControllerNode, idx: usize) {
    let node_type = c.data.kind.type_string();
    let reqs = c
        .data
        .requires
        .iter()
        .fold("".to_string(), |lhs, (name, sig)| {
            format!("{}, {}", lhs, *name)
        });

    let provs = c
        .data
        .provides
        .iter()
        .fold("".to_string(), |lhs, (name, sig)| {
            format!("{}, {}", lhs, *name)
        });
    println!("{idx} [label=\"{node_type}\nrequires: {reqs}\nprovides: {provs}\"];");
}

fn print_dot_channel(e: &ChannelEdge) {
    let reqs = e
        .data
        .requires
        .iter()
        .fold("".to_string(), |lhs, (name, sig)| {
            format!("{}, {}", lhs, *name)
        });

    let provs = e
        .data
        .provides
        .iter()
        .fold("".to_string(), |lhs, (name, sig)| {
            format!("{}, {}", lhs, *name)
        });

    let port_type = match e.data.dst_port {
        Port::Flow(..) => "flow",
        Port::Select() => "select",
    };

    println!(
        "{} -> {} [label=\"type: {port_type}\nrequires: {reqs}\nprovides: {provs}\"];",
        e.src.0, e.dst.0
    );
}

pub fn print_dot(g: &Graph<Controller, Channel>) {
    println!("Digraph G {{");
    g.nodes
        .iter()
        .enumerate()
        .for_each(|(idx, node)| print_dot_controller(node, idx));
    g.edges.iter().for_each(print_dot_channel);
    println!("}}");
}

#[cfg(test)]
pub mod test {
    use super::*;
    use crate::token;
    use chumsky::extra;
    use chumsky::prelude::*;
    use extra::Err;
    use std::eprint;
    use std::matches;

    #[test]
    fn parse_undefined_var() {
        let source = r###"
a;
        "###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        assert!(!output.has_errors());

        let mut builder = CstBuilder::new();
        let _ = builder.build_from_structural_node(output.output().unwrap());

        builder.diagnostics.iter().for_each(|diag| {
            let _ = diag.print(Source::from(source));
        });
    }

    #[test]
    fn parse_pipe_1() {
        let source = r###"
[[chan a,chan b], chan c] ->
[[chan e, [[chan x, chan d], chan foo]], chan f];
        "###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        assert!(!output.has_errors());

        let mut builder = CstBuilder::new();
        let _ = builder.build_from_structural_node(output.output().unwrap());

        builder.diagnostics.iter().for_each(|diag| {
            let _ = diag.print(Source::from(source));
        });
    }

    #[test]
    fn parse_gcd() {
        let source = r###"
{
chan a : {sig a : logic[12:0]};
chan b : {sig b : logic[12:0]};
chan loop_s;

input chan a : {sig a : logic[12:0]};
input chan b : {sig b : logic[12:0]};

[[a, b] -> join(), chan loop_val] -> mux(loop_s) -> fork() ->
[
        comb {sig s : logic[0:0] = a != b;} -> fork() -> [reg(sig s : logic[0:0] = 1) -> loop_s, chan output_s],
        demux(output_s) -> [output chan o : {sig a : logic[12:0], sig b : logic[12:0]},
                           fork() -> [comb {sig s : logic[0:0] = a > b;} -> chan branch_s,
                                     demux(branch_s) -> [
                                                        comb {a[7:0] = a[7:0] - b[7:0];},
                                                        comb {b = b - a;}
                                                        ]
                                     -> merge() -> loop_val
                                     ]
                           ]
];
}
"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        //assert!(!output.has_errors());
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }

    // TODO: this doesn't need to be mut really. Only because Graph::node is mut
    pub fn all_needs_met(g: &mut Graph<Controller, Channel>) -> bool {
        for n in 0..g.nodes.len() {
            let node = g.node(NodeIdx(n));
            node.edges.outs_iter().for_each(|edge| {
                edge.data.requires.iter().for_each(|(name, sig)| {
                    assert!(edge.data.provides.contains_key(*name));
                });
            });

            node.data.requires.iter().for_each(|(name, sig)| {
                assert!(node.data.provides.contains_key(*name));
            });
        }

        true
    }

    #[test]
    fn extract_comb_signals() {
        let source = r###"
{
comb {sig a = 0; sig b = 0;} -> comb {sig c = a + b;} -> comb {sig d = c + a;};
}
"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        //assert!(!output.has_errors());
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());

                builder.propagate2();
                //println!("{:?}", builder.g);
                //dbg!(builder.g);
                print_dot(&builder.g);
                assert!(all_needs_met(&mut builder.g));
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }

    #[test]
    fn propagate_gcd() {
        let source = r###"
{
chan loop_s;

input chan a : {sig a : logic[12:0]};
input chan b : {sig b : logic[12:0]};

[[a, b] -> join(), chan loop_val] -> mux(loop_s) -> fork() ->
[
        comb {sig s : logic[0:0] = a != b;} -> fork() -> [reg(sig s : logic[0:0] = 1) -> loop_s, chan output_s],
        demux(output_s) -> [output chan o : {sig a : logic[12:0], sig b : logic[12:0]},
                           fork() -> [comb {sig s : logic[0:0] = a > b;} -> chan branch_s,
                                     demux(branch_s) -> [
                                                        comb {a[7:0] = a[7:0] - b[7:0];},
                                                        comb {b = b - a;}
                                                        ]
                                     -> merge() -> loop_val
                                     ]
                           ]
];
}
"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        //assert!(!output.has_errors());
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());

                builder.propagate2();
                //println!("{:?}", builder.g);
                //dbg!(builder.g);
                print_dot(&builder.g);
                assert!(all_needs_met(&mut builder.g));
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }

    #[test]
    fn propagate_fork() {
        let source = r###"
{
input chan a : {sig a : logic[12:0]};
output chan b : {sig a : logic[12:0]};
output chan c : {sig a : logic[12:0]};

a -> fork() -> [b, c];
}

"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        //assert!(!output.has_errors());
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());

                builder.propagate2();
                //println!("{:?}", builder.g);
                //dbg!(builder.g);
                print_dot(&builder.g);
                //dbg!(&builder.g);
                assert!(all_needs_met(&mut builder.g));
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }

    #[test]
    fn propagate_join() {
        let source = r###"
{
input chan a : {sig a : logic[12:0]};
input chan b : {sig b : logic[12:0]};
output chan c : {sig a : logic[12:0], sig b : logic[12:0]};

[a, b] -> join() -> c;
}

"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());

                builder.propagate2();
                print_dot(&builder.g);
                assert!(all_needs_met(&mut builder.g));
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }

    #[test]
    fn propagate_fun_decl() {
        let source = r###"

fn my_join[chan x, chan y]()[chan z] {
[x, y] -> join() -> z;
}
{
input chan a : {sig a : logic[12:0]};
input chan b : {sig b : logic[12:0]};
output chan c : {sig a : logic[12:0], sig b : logic[12:0]};

[a, b] -> my_join() -> c;
}

"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());

                builder.propagate2();
                print_dot(&builder.g);
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }

    #[test]
    fn propagate_blackbox() {
        let source = r###"

fn my_join[chan x, chan y]()[chan z] {
[x, y] -> join() -> z;
}
{
input chan a : {sig a : logic[12:0]};
input chan b : {sig b : logic[12:0]};
output chan c : {sig a : logic[12:0], sig b : logic[12:0]};

[a, b] -> blackbox(foo, input chan i : {sig a: logic[12:0]}, input chan k : {sig b: logic[12:0]},
                        output chan j : {sig a : logic[12:0]}, output chan l : { sig b: logic[12:0]}) -> my_join() -> c;
}

"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        println!("{:?}", tokens);
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());

                builder.propagate2();
                builder.propagate2();
                print_dot(&builder.g);
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }
}
