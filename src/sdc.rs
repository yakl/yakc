use serde_json::{Result, Value};

use std::collections::BTreeMap;
use std::collections::HashMap;
use std::collections::VecDeque;
use std::fmt::format;

use crate::cg;
use crate::cg::ComputeNode;
use crate::cg::Type;
use crate::cst::Channel;
use crate::cst::Controller;
use crate::cst::ControllerKind;
use crate::cst::Signal;
use crate::graph::Graph;
use crate::graph::NodeViewRef;
use crate::token;

use super::cst;

use super::ast;
use super::ast::Node;
use super::graph;
use super::graph::{EdgeIdx, EdgesView, GraphEdge, GraphNode, NodeIdx, NodeView};
use super::token::Op;
use super::util::ScopedSymTable;
use ariadne::ReportBuilder;
use ariadne::{Color, ColorGenerator, Fmt, Label, Report, ReportKind, Source};
use by_address::ByAddress;
use chumsky::container::Container;
use itertools::izip;

type SpannedNode = super::ast::Spanned<ast::Node>;

pub trait GenerateSdc {
    fn generate_sdc_from(cst: &Graph<Controller, Channel>) -> String;
}

pub struct DecoupledClickBuilder {}

impl DecoupledClickBuilder {
    fn setup(
        sdc: &mut String,
        l_phase: &str,
        node_idx: NodeIdx,
        cst: &Graph<Controller, Channel>,
        prev_node: NodeIdx,
    ) {
        use std::fmt::Write;
        let node = cst.node_ref(node_idx);

        let instance = format!("{}_{}", node.data.kind.type_string(), node_idx.0);

        let idx = node_idx.0;
        use ControllerKind::*;
        match &node.data.kind {
            Reg { .. } => {
                let _ = writeln!(
                    sdc,
                    "create_generated_clock -name {l_phase}_capture_{idx} \
				  -edges {{1 2 3}} -source [get_pins {instance}/clickElement/xrleft/A1] \
				  [get_pin {instance}/clickelement/clkAnd/Z] -master {l_phase} -add"
                );
            }
            Fork {} => {
                node.edges
                    .outs_iter()
                    .for_each(|edge| Self::setup(sdc, l_phase, edge.dst, cst, node_idx));
            }
            Join {} => {
                let new_phase = format!("{l_phase}_{idx}");
                let _ = writeln!(
                    sdc,
                    "create_generated_clock -name {new_phase} -edges {{1 1 3}} -source \
				       [get_pin {instance}/po_reg_CP] \
				       [get_pin {instance}/po_reg/Q] -master {l_phase}"
                );
                node.edges
                    .outs_iter()
                    .for_each(|edge| Self::setup(sdc, &new_phase, edge.dst, cst, node_idx));
            }
            Demux {} => {
                let base = format!("{l_phase}_{instance}");
                let _ = writeln!(
                    sdc,
                    "create_generated_clock -name {base} -edges {{1 2 3}} \
				  -source [get_pin {instance}/g_var_in/A1] \
				  [get_pin {instance}/g_in/Z] -master {l_phase} -add"
                );
                node.edges
                    .outs_iter()
                    .enumerate()
                    .for_each(|(port_idx, edge)| {
                        let new_phase = format!("{base}_o{port_idx}");
                        let _ = writeln!(
                            sdc,
                            "create_generated_clock -name {new_phase} -edges {{1 1 3}} \
					  -source [get_pin {instance}/po{port_idx}_reg/CP] \
					  [get_pin {instance}/po{port_idx}_reg/Q] \
					  -master {base} -add"
                        );
                        Self::setup(sdc, &new_phase, edge.dst, cst, node_idx);
                    });
            }
            Mux {} => {
                let new_phase = format!("{l_phase}_{instance}");
                let mut offset = 0;
                node.edges
                    .incs_iter()
                    .enumerate()
                    .for_each(|(mut port_idx, edge)| {
                        port_idx -= offset;
                        if edge.src == prev_node {
                            if matches!(edge.data.dst_port, cst::Port::Select()) {
                                offset += 1;
                                let _ = writeln!(
                                    sdc,
                                    "create_generated_clock -name {new_phase} -edges {{1 1 3}} \
					    -source [get_pin {instance}/selChannelAck_g/A1] \
					    [get_pin {instance}/phase_o/Q] \
					    -master {l_phase} -add"
                                );
                            } else {
                                let _ =
                                    writeln!(sdc,
                                    "create_generated_clock -name {new_phase} -edges {{1 1 3}} \
						-source [get_pin {instance}/channel{port_idx}_g/A1] \
						[get_pin {instance}/phase_o/Q] \
						-master {l_phase} -add"
                                );
                            }
                        }
                    });

                node.edges
                    .outs_iter()
                    .for_each(|edge| Self::setup(sdc, &new_phase, edge.dst, cst, node_idx));
            }

            Merge {} => {
                let new_phase = format!("{l_phase}_{instance}");
                let mut offset = 0;
                node.edges
                    .incs_iter()
                    .enumerate()
                    .for_each(|(mut port_idx, edge)| {
                        port_idx -= offset;
                        if edge.src == prev_node {
                            if matches!(edge.data.dst_port, cst::Port::Select()) {
                                offset += 1;
                                let _ = writeln!(sdc,
					 "create_generated_clock -name {new_phase}_capture -edges {{1 1 3}} \
					  -source [get_pin {instance}/g_sel/A1] \
					  [get_pin {instance}/g_in/Z] \
					  -master {l_phase} -add");
                            } else {
                                let _ = writeln!(
                                    sdc,
                                    "create_generated_clock -name {new_phase} -edges {{1 1 3}} \
				     -source [get_pin {instance}/g_sel_{port_idx}/A1] \
				     [get_pin {instance}/po_reg/Q] \
				     -master {l_phase} -add"
                                );
                            }
                        }
                    });

                node.edges
                    .outs_iter()
                    .for_each(|edge| Self::setup(sdc, &new_phase, edge.dst, cst, node_idx));
            }
            _ => {
                node.edges
                    .outs_iter()
                    .for_each(|edge| Self::setup(sdc, l_phase, edge.dst, cst, node_idx));
            }
        }
    }

    fn hold(
        sdc: &mut String,
        r_phase: &str,
        node_idx: NodeIdx,
        cst: &Graph<Controller, Channel>,
        prev_node: NodeIdx,
    ) {
        use std::fmt::Write;
        let node = cst.node_ref(node_idx);

        let instance = format!("{}_{}", node.data.kind.type_string(), node_idx.0);

        let idx = node_idx.0;
        use ControllerKind::*;
        match &node.data.kind {
            Reg { .. } => {
                let _ = writeln!(
                    sdc,
                    "create_generated_clock -name {r_phase}_launch_{idx} \
				  -edges {{1 2 3}} -source [get_pins {instance}/clickElement/xnrright/A2] \
				  [get_pin {instance}/clickelement/clkAnd/Z] -master {r_phase} -add"
                );
            }
            Fork {} => {
                let new_phase = format!("{r_phase}_{idx}");
                let _ = writeln!(
                    sdc,
                    "create_generated_clock -name {new_phase} -edges {{1 1 3}} -source \
				       [get_pin {instance}/pi_reg_CP] \
				       [get_pin {instance}/pi_reg/Q] -master {r_phase}"
                );
                node.edges
                    .outs_iter()
                    .for_each(|edge| Self::hold(sdc, &new_phase, edge.src, cst, node_idx));
            }
            Join {} => {
                node.edges
                    .incs_iter()
                    .for_each(|edge| Self::hold(sdc, r_phase, edge.src, cst, node_idx));
            }
            Demux {} => {
                node.edges
                    .incs_iter()
                    .enumerate()
                    .for_each(|(port_idx, edge)| {
                        if !matches!(edge.data.dst_port, cst::Port::Select()) {
                            let new_phase = format!("{r_phase}_{instance}");
                            let _ = writeln!(
                                sdc,
                                "create_generated_clock -name {new_phase} -edges {{1 1 3}} \
				 -source [get_pin {instance}/pi_reg_reg/CP] \
				 [get_pin {instance}/pi_reg/Q] \
				 -master {r_phase} -add"
                            );
                            Self::hold(sdc, &new_phase, edge.src, cst, node_idx);
                        }
                    });
            }
            Mux {} => {
                let mut offset = 0;
                node.edges
                    .incs_iter()
                    .enumerate()
                    .for_each(|(mut port_idx, edge)| {
                        port_idx -= offset;
                        if matches!(edge.data.dst_port, cst::Port::Select()) {
                            offset += 1;
                            let new_phase = format!("{r_phase}_{instance}_is");
                            let _ = writeln!(
                                sdc,
                                "create_generated_clock -name {new_phase} -edges {{1 1 3}} \
					    -source [get_pin {instance}/phase_is/CP] \
					    [get_pin {instance}/phase_is/Q] \
					    -master {r_phase} -add"
                            );
                            Self::setup(sdc, &new_phase, edge.src, cst, node_idx);
                        } else {
                            let new_phase = format!("{r_phase}_{instance}_i{port_idx}");
                            let _ = writeln!(
                                sdc,
                                "create_generated_clock -name {new_phase} -edges {{1 1 3}} \
				     -source [get_pin {instance}/phase_i{port_idx}/CP] \
				     [get_pin {instance}/phase_i{port_idx}/Q] \
						-master {r_phase} -add"
                            );
                            Self::setup(sdc, &new_phase, edge.src, cst, node_idx);
                        }
                    });
            }
            Merge {} => {
                node.edges
                    .incs_iter()
                    .enumerate()
                    .for_each(|(port_idx, edge)| {
                        let new_phase = format!("{r_phase}_{instance}_i{port_idx}");
                        let _ = writeln!(
                            sdc,
                            "create_generated_clock -name {new_phase} -edges {{1 1 3}} \
				-source [get_pin {instance}/g_in/A2] \
				[get_pin {instance}/p{port_idx}_reg/Q] \
				-master {r_phase} -add"
                        );
                        Self::setup(sdc, r_phase, edge.src, cst, node_idx);
                    });
            }
            _ => {
                node.edges
                    .incs_iter()
                    .for_each(|edge| Self::setup(sdc, r_phase, edge.src, cst, node_idx));
            }
        }
    }
}

impl GenerateSdc for DecoupledClickBuilder {
    fn generate_sdc_from(cst: &Graph<Controller, Channel>) -> String {
        use std::fmt::Write;
        let mut sdc = String::new();

        let mut stage_nodes: Vec<_> = Vec::new();

        cst.nodes.iter().enumerate().for_each(|(idx, node)| {
            // In the first pass we do the preamble of dont_touch that we need
            // for our implementation. We also save the root nodes
            let type_string = &node.data.kind.type_string();
            use ControllerKind::*;
            match &node.data.kind {
                Reg { .. } => {
                    let _ = writeln!(sdc, "set_dont_touch {type_string}_{idx}/clickElement");
                    stage_nodes.push(NodeIdx(idx));
                }
                Fork {} => {
                    let _ = writeln!(sdc, "set_dont_touch {type_string}_{idx}/po_reg");
                }
                Join {} => {
                    let _ = writeln!(sdc, "set_dont_touch {type_string}_{idx}/pi_reg");
                }
                Demux {} => {
                    let _ = writeln!(
                        sdc,
                        "set_dont_touch {type_string}_{idx}/pi_reg\n\
			 set_dont_touch {type_string}_{idx}/g_out\n\
			 set_dont_touch {type_string}_{idx}/po0_reg\n\
			 set_dont_touch {type_string}_{idx}/po1_reg\n\
			 set_dont_touch {type_string}_{idx}/g_xor_in\n\
			 set_dont_touch {type_string}_{idx}/g_sel"
                    );
                }
                Mux {} => {
                    let _ = writeln!(
                        sdc,
                        "set_dont_touch {type_string}_{idx}/g_ro\n\
			 set_dont_touch {type_string}_{idx}/selChannelAck_g\n\
			 set_dont_touch {type_string}_{idx}/channel0_g\n\
			 set_dont_touch {type_string}_{idx}/channel1_g"
                    );
                }
                Merge {} => {
                    let _ = writeln!(
                        sdc,
                        "set_dont_touch {type_string}_{idx}/g_out\n\
			 set_dont_touch {type_string}_{idx}/g_in\n\
			 set_dont_touch {type_string}_{idx}/g_sel_b\n\
			 set_dont_touch {type_string}_{idx}/g_sel_a"
                    );
                }
                _ => {}
            }
        });

        // Now we can generate our root clocks and phase clocks
        let mut root_clocks: Vec<_> = Vec::new();
        for idx in stage_nodes.iter() {
            let node = cst.node_ref(*idx);

            let idx = idx.0;
            let instance = format!("{}_{}", node.data.kind.type_string(), idx);
            let _ = writeln!(
                sdc,
                "create_clock -name stage_{idx}_clk -period 0.001 \
				   [get_pin {instance}/clickElement/clkand/Z] -add"
            );

            root_clocks.push(format!("stage_{idx}_clk"));

            node.edges.outs_iter().for_each(|edge| {
                let forward_phase = format!("stage_{idx}_phaseR");
                let _ = writeln!(
                    sdc,
                    "create_generated_clock -name {forward_phase} -edges {{1 1 3}} \
			       -source [get_pin {instance}/clickElement/genblk2.poreg/CP] \
			       [get_pin {instance}/clickElement/genblk2.poreg/Q] \
			       -master stage_{idx}_clk -add"
                );

                Self::setup(&mut sdc, &forward_phase, edge.dst, cst, NodeIdx(idx));
            });

            node.edges.outs_iter().for_each(|edge| {
                let backward_phase = format!("stage_{idx}_phaseL");
                let _ = writeln!(
                    sdc,
                    "create_generated_clock -name {backward_phase} -edges {{1 1 3}} \
			       -source [get_pin {instance}/clickElement/genblk1.pireg/CP] \
			       [get_pin {instance}/clickElement/genblk1.pireg/Q] \
			       -master stage_{idx}_clk -add"
                );

                Self::setup(&mut sdc, &backward_phase, edge.src, cst, NodeIdx(idx));
            });
        }

        // Now we can add the multicycle and false path constraints that will apply to all clocks
        let _ = write!(
            sdc,
            "set_multicycle_path -1 -hold -from [get_clocks *] -to [get_clocks *]\n\
	     set_multicycle_path 0 -setup -from [get_clocks *] -to [get_clocks *]\n\
	     set_clock_groups -asynchronous "
        );

        root_clocks.iter().for_each(|root| {
            let _ = write!(sdc, "-group [get_clocks {root}*] ");
        });
        let _ = write!(
            sdc,
            "set_false_path -from [get_clocks *phase*]\n\
	     set_false_to -to [get_clocks *phase*]\n\
	     set_false_path -from [get_clocks *capture*]\n\
	     set_false_path -to [get_clocks *launch*]\n\
	     set_false_path -setup -from [get_clocks *launch*]\n\
	     set_false_path -hold -to [get_clocks *capture**]\n"
        );

        root_clocks.iter().for_each(|root| {
            let _ = writeln!(
                sdc,
                "set_false_path -from [get_clocks {root}] -to [get_clocks {root}]"
            );
        });

        sdc
    }
}
