module Mux
  #(parameter DATA_WIDTH = 1)
  (input req_in0, req_in1, ack_out0, 
   input [DATA_WIDTH - 1 : 0] data_in0,
   input [DATA_WIDTH - 1 : 0] data_in1,
   input req_sel0, data_sel0,
   output req_out0, ack_sel0, ack_in0, ack_in1,
   output reg [DATA_WIDTH - 1 : 0] data_out0,
   input rst);

   reg                           phase_i0_P;
   reg                           phase_i1_P;
   reg                           phase_is_P;
   reg                           phase_o_P;
   wire                          phase_o_N;
   wire                          phase_i_clk;
   wire                          phase_o_clk;
   wire                          channel0Xor;
   wire                          channel1Xor;
   wire                          selChannelXor;

   assign channel0Xor = req_in0 ^ ack_in0;
   assign channel1Xor = req_in1 ^ ack_in1;
   assign selChannelXor = req_sel0 ^ ack_sel0;

   assign ack_in0 = phase_i0_P;
   assign ack_in1 = phase_i1_P;
   assign ack_sel0 = phase_is_P;

   assign #5 phase_i_clk = req_out0 ~^ ack_out0;
   assign #5 phase_o_clk = (selChannelXor & ~data_sel0 & channel0Xor) | (selChannelXor & data_sel0 & channel1Xor);

   assign phase_o_N = ~phase_o_P;

   assign req_out0 = phase_o_P;

   always @(*) begin
      if (data_sel0 == 1'b0)
        data_out0 <= data_in0;
      else
        data_out0 <= data_in1;
   end

   always @ (posedge phase_i_clk, posedge rst) begin
      if (rst == 1'b1) begin
         phase_i0_P <= 0;
         phase_i1_P <= 0;
         phase_is_P <= 0;
      end
      else begin
         phase_i0_P <= ack_in0 ^ ~data_sel0;
         phase_i1_P <= ack_in1 ^ data_sel0;
         phase_is_P <= req_sel0;
      end
   end

   always @ (posedge phase_o_clk, posedge rst) begin
      if (rst == 1'b1) begin
         phase_o_P <= 0;
      end
      else begin
         phase_o_P <= phase_o_N;
      end
   end

endmodule
