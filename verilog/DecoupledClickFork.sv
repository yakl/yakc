module Fork
  #(parameter DATA_WIDTH = 1)
   ( input req_in0, ack_out0, ack_out1, rst,
     input [DATA_WIDTH - 1:0] data_in0,
     output req_out0, req_out1, ack_in0,
     output [DATA_WIDTH - 1:0] data_out0,
     output [DATA_WIDTH - 1:0] data_out1 );

   reg                             Phase_P;
   wire                            Phase_N;
   wire                            click;
   assign req_out0 = req_in0;
   assign req_out1 = req_in0;
   assign data_out0 = data_in0;
   assign data_out1 = data_in0;
   assign ack_in0 = Phase_P;

   assign click = (~ack_out0 & ~ack_out1 & ack_in0) | (ack_out0 & ack_out1 & ~ack_in0);
   assign Phase_N = ~Phase_P;

   always @ (posedge click, posedge rst) begin
      if (rst == 1'b1)
        Phase_P <= 1'b0;
      else
        Phase_P <= Phase_N;
   end
endmodule // DecoupledClickFork
