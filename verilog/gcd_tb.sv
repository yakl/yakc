module gcd_tb;

   wire req_o, ack_a, ack_b;
   reg req_a, req_b, ack_o, clk, rst;
   wire [15:0] data_o;
   reg [7:0] data_a;
   reg [7:0] data_b;

   top dut (.req_a(req_a), .data_a(data_a), .ack_a(ack_a),
	    .req_b(req_b), .data_b(data_b), .ack_b(ack_b),
	    .req_o(req_o), .data_o(data_o), .ack_o(ack_o),
	    .rst(rst));

   initial
     begin
        $dumpfile("foo.vcd");
        $dumpvars(0, dut);
	data_a = 8'h0C;
	data_b = 8'h08;
	req_a = 0;
	req_b = 0;
	ack_o = 0;
        rst = 1;
        #25 rst = 0;
        #50 req_a = 1;
        #50 req_b = 1;
        //wait (req_o == 1'b1);
	//#25 ack_o = 1;
        #1000 $finish;
     end
endmodule
