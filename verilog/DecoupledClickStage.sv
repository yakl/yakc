module Stage
  #(parameter DATA_WIDTH = 1,
    parameter PI = 0,
    parameter PO = 0,
    parameter INITIAL_VAL = 0)
   (input req_in, ack_out, rst,
    input [DATA_WIDTH - 1:0] data_in,
    output req_out, ack_in,
    output [DATA_WIDTH - 1:0] data_out);

   wire                            clk;
   reg                             [DATA_WIDTH-1:0] D_reg;

   DecoupledClickElement #(.PI(PI), .PO(PO)) clickElement(req_in, ack_out, rst, req_out, ack_in, clk);

   assign data_out = D_reg;

   always @(posedge clk, posedge rst)
     begin
        if (rst == 1'b1)
          D_reg = INITIAL_VAL;
        else
          D_reg = data_in;
     end

endmodule // DecoupledClickStage
