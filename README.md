# Yakc

Yak is a language aimed at describing asynchronous bundled data circuits with minimal boilerplate. 
This repository contains the Yak compiler which targets Verilog as a backend and can generate SDC
constraints for the decoupled click element asynchronous controller.

# Getting started

Yakc is written in Rust which makes installation easy on most platforms with the help of the
Rust package manager Cargo. 

## Prerequisites 
 - Cargo
 - rustc
 

## Building the project

With Cargo installed, building Yak should be as simple as running
```
cargo build --release
```

Cargo will automatically fetch the needed dependencies and build the project. The `yakc` will be located
in the `target/release/` directory when building with `--release`.

## Running tests
Tests can be run by executing
```
cargo test
```

Specific tests can be executed by passing the test name to `cargo test`. A list of tests can be obtained
with `cargo test --list`.

## Running yakc
`yakc` can be invoked on the command line with the following options:
```
Usage: yakc [OPTIONS]

Options:
  -f, --file <FILE>                  Yak file
  -v, --verilog <FILE>               Generate Verilog into FILE
      --gen-sdc-demo <GEN_SDC_DEMO>  Generate SDC into FILE (only for decoupled click element implementation)
  -h, --help                         Print help
  -V, --version                      Print version
```

`yakc` can also be run by executing `cargo run -- [OPTIONS]`. Note the space between the double dash and the options.

# Language Overview

## Channels

Channels are a core concept in Yak. Channels can be created with a type specification stating they shall carry
a certain set of signals, and connected to control flow components or other channels.

Channels can be declared in structural blocks using the `chan` keyword. For example
```
chan a; // declares a channel named a
```
Type specification for a channel must be done inside `{}` brackets. Channels may carry any number of named signals.
For example
```
chan a: {sig b};
```
Declares that channel `a` shall carry a signal named b. Signals inside the channel type specifiers may include their
owns type specifier.
```
chan a: {sig b: logic[31:0], sig c}; // b must be logic[31:0], but c can be anything
```

Channels can be connected using the pipeline operator `->` which connects the output of one channel, or pipeline,
to the input of another one.

## Structured bindings

Most of the bundled data control flow elements take multiple channels as inputs or have multiple channels on their
outputs. In Yak this is represented using the `[]` structured binding syntax. Structured bindings allow
efficient description of connections in an arbitrary graph.

The way the `->` operator binds is illustrated here
```
[a, c] -> [b, d]; // equivalent to a -> b; c -> d; 
[a -> b, e -> f] -> [c -> d, g -> h] // a -> b -> c -> d; e -> f -> g -> h;
[[a, c], e] -> [[b, d], f]; // a -> b; c -> d; e -> f;
```

## Signals

Yak currently supports unsigned logic vectors. Signals may be declared in:
 - Channel type specifiers
 - Comb blocks
 - Reg initializers
 
 Signal declaration follows the pattern
 ```
 sig <name> [: <typename>];
 ```
 For example
 ```
 sig a : logic[7:0];
 ```
 declares a signal of type `logic[7:0]`, the unsigned logic type. If the signal is declared without the optional
 type specifier then it will assume the type of the first signal or expression that is assigned to it.
 For example
 ```
 sig b : logic[7:0];
 sig c = logic[7:0];
 sig a = b + c; // a becomes logic[7:0]
 sig x = 7; // x becomes logic[2:0] = 3'b111
 ```


## Components

Yak supports some pre-defined modules. When generating Verilog, Yak expects that these are provided
to the Verilog elaboration process separately. Yak places some constraints on the names and port maps
of the components that it expects.

### Merge
The merge component accepts tokens on two input channels and provides tokens on one output channel.
The merge assumes that tokens can only flow in one channel at a time.

Using structured bindings:
```
[in0, in1] -> merge() -> out0
```

When generating Verilog. Yak generates Verilog instances of a `Merge` module. Such a module shall have
the module definition:
```
module Merge
  #(parameter DATA_WIDTH = 1)
   (input req_in0, req_in1, 
    input [DATA_WIDTH - 1 : 0] data_in0, 
    input [DATA_WIDTH - 1 : 0] data_in1, 
    input ack_out0,
    output req_out0, ack_in0, ack_in1, 
    output reg [DATA_WIDTH - 1 : 0] data_out0,
    input rst);
```

### Mux
The mux component accepts input tokens on two channels like the merge, but it additional accepts a
select token which is used to determine the channel from which the next token shall be consumed.

Using structured bindings:
```
[in0, in1] -> mux(sel0) -> out0
```

When using `Mux` and `Demux` components it is required to have a signal named `s` on the channel 
used for selecting the input channel. This is easily accomplished with a comb block prior to
declaring the select channel
```
comb {sig s = a < b;} -> chan select;
...
... -> mux(select) -> ...
```

When generating Verilog. Yak generates Verilog instances of a `Mux` module. Such a module shall have
the module definition:
```
  #(parameter DATA_WIDTH = 1)
  (input req_in0, req_in1, ack_out0, 
   input [DATA_WIDTH - 1 : 0] data_in0,
   input [DATA_WIDTH - 1 : 0] data_in1,
   input req_sel0, data_sel0,
   output req_out0, ack_sel0, ack_in0, ack_in1,
   output reg [DATA_WIDTH - 1 : 0] data_out0,
   input rst);
```

## Demux
Like mux, but accepts tokens on one channel forwards it to one of two selectable output channels

Using structured bindings:
```
in0 -> demux(sel0) -> [out0, out1]
```
When generating Verilog. Yak generates Verilog instances of a `Demux` module. Such a module shall have
the module definition:

```
  #(parameter DATA_WIDTH = 1)
  (input req_in0, req_in1, ack_out0, 
   input [DATA_WIDTH - 1 : 0] data_in0,
   input [DATA_WIDTH - 1 : 0] data_in1,
   input req_sel0, data_sel0,
   output req_out0, ack_sel0, ack_in0, ack_in1,
   output reg [DATA_WIDTH - 1 : 0] data_out0,
   input rst);
```

## Fork
Splits a token on one channel into two copies of that token on two output channels.

Using structured bindings:
```
in0 -> fork() -> [out0, out1]
```

When generating Verilog. Yak generates Verilog instances of a `Demux` module. Such a module shall have
the module definition:
```
  #(parameter DATA_WIDTH = 1)
   ( input req_in0, ack_out0, ack_out1, rst,
     input [DATA_WIDTH - 1:0] data_in0,
     output req_out0, req_out1, ack_in0,
     output [DATA_WIDTH - 1:0] data_out0,
     output [DATA_WIDTH - 1:0] data_out1 );
```
## Join
Merges two tokens on two input channels into one token on one output channel
```
[in0, in1] -> join() -> out0
```

When generating Verilog. Yak generates Verilog instances of a `Demux` module. Such a module shall have
the module definition:
```
  #(parameter DATA0_WIDTH = 1,
    parameter DATA1_WIDTH = 1)
   ( input req_in0, req_in1, ack_out0, rst, 
     input [DATA0_WIDTH-1:0] data_in0,
     input [DATA1_WIDTH-1:0] data_in1,
     output req_out0, ack_in0, ack_in1, 
     output [DATA0_WIDTH + DATA1_WIDTH - 1:0] data_out0 );
```

## Reg
A stateful pipeline element. `Reg` elements are special because they can contain initial tokens.
This is specified by providing the initial values for the signals of the `Reg` component as arguments.
```
Reg(sig a : logic[7:0] = 1, sig b : ...)
```
It is important to note that if any of the signals have a specified initial value, then all signals must
have an initial value. However, it is not necessary to specify initial values. If no initial values are
specified the `Reg` component will accept whatever signals are on its input.
```
chan a : {sig b: ..., sig c: ...} -> Reg() // OK!
```

When generating Verilog. Yak generates Verilog instances of a `Reg` module. Such a module shall have
the module definition:

```
module Stage
  #(parameter DATA_WIDTH = 1,
    parameter PO = 0,
    parameter INITIAL_VAL = 0)
   (input req_in, ack_out, rst,
    input [DATA_WIDTH - 1:0] data_in,
    output req_out, ack_in,
    output [DATA_WIDTH - 1:0] data_out);
```
The `PO` parameter is used to indicate whether the stage element contains an initial token.

## Comb
`Comb` blocks are how logic is implemented in Yak. `Comb` blocks work much like Verilog processes, with a
currently reduced set of supported operations. Inside the block, the logic has access to the signals defined
on the input channel to the block. Signals declared inside the top scope of the comb block automatically become
available on the output channel of the comb block.
```
 chan a: {sig b: logic[31:0], sig c: logic[7:0]} ->
 comb {
     sig d = b + c;
	 sig x;
	 if (d > 10) {
		 x = 10;
	 } else {
	     x = 0;
	 }
 } -> chan y;
 ```
 
 `Comb` blocks synthesize to Verilog processes and do not require instancing any modules.


# Language Tutorial
This tutorial will cover the essential language constructs through construction of a circuit that performs 
Euclid's greatst common divisor (GCD) algorithm. In pseudocode the algorithm is
```
while (a != b) {
    if a > b {
	    a = a - b;
	}
	else {
	    b = b - a;
	}
}
```

We will implement an asynchronous bundled data circuit that performs the same functionality and simulate it
using Icarus Verilog

![GCD circuit](docs/figures/gcd.png)

Starting on the left we have our input channels which we declare in the top anonymous scope that will become
the `top` module.
```
input chan a : {sig a: logic[7:0]};
input chan b : {sig b: logic[7:0]};
```
Because we are in the top scope we have access to the `input` and `output` keywords which will become
inputs and outputs on our `top` Verilog module.

We put some registers on these channels to match the schematic we are given. Note that we don't need to specify the
type. At the same time we will use a structured binding to join the channels. This avoids having to specify 
additional temporary channels.
```
[a -> reg(), b -> reg()] -> join() -> chan gcd_input;
```
Now we get to the multiplexer. We take advantage of the fact that we can declare channels in place, even
inside the instantiation of the multiplexer. We throw in the `Reg` and `Fork` for good measure.
```
[gcd_input, chan loop_token] -> mux(chan input_select) -> reg() -> fork() -> [chan up, chan right];
```
Now we can do the `Comb` block that will generate our loop control token. We're generating this from
the temporary `up` channel that we created.
```
up -> comb {sig s = a != b;} -> fork() -> [chan in_control, chan out_control];
```
The most interesting piece is the pipeline stage with the initial token which we will describe like this
```
in_control -> Reg(sig s: logic[0:0] = 0) -> input_select;
```
Since we defined the input select channel earlier we don't need to do it again. On the other side of the circuit
we have the demultiplexer that determines whether we will send out result token out, or through the loop again for
further processing. Here we need two of the channels we defined earlier.
```
right -> demux(out_control) -> [output o: {sig a, sig b}, chan down];
```
We directly declared the output inside the structured binding, which we can do because we are in the top scope.

Now we actually have all syntactic pieces to construct the rest of the circuits, so we will collect a bit more
in the source blocks. Starting with the pipeline stage.
```
down -> Reg() -> fork() -> [comb {sig s = a > b;} -> chan subtract_control, 
                            demux(subtract_control) ->
	                        [
	                        	comb { a = a - b; },
								comb { b = b - a; }
	                        ] -> merge() -> loop_token;
```

Here is the full code, with temporaries removed.
```
{
chan loop_s;

input chan a : {sig a : logic[7:0]};
input chan b : {sig b : logic[7:0]};

[[a, b] -> join(), chan loop_val] -> mux(loop_s) -> reg() -> fork() ->
[
        comb {sig s = a != b;} -> fork() -> [reg(sig s : logic[0:0] = 0) -> loop_s, chan output_s],
        demux(output_s) -> [reg() -> output chan o : {sig a, sig b},
                           reg() -> fork() -> [comb {sig s = a < b;} -> chan branch_s,
                                     demux(branch_s) -> [
                                                        comb {a = a - b;},
                                                        comb {b = b - a;}
                                                        ]
                                     -> merge() -> loop_val
                                     ]
                           ]
];
}
```

Now we're going to generate the verilog. Save the above code to a file like `gcd.yk` and invoke `yakc`
```
yakc -f gcd.yk -v gcd.v
```

we can simulate the generated verilog using Icarus verilog. The `verilog/` directory includes some example
implementations of asynchronous controllers to use. For example we can use the decoupled click element controllers
with the following `file_list.txt` for Icarus
```
../verilog/DecoupledClickMux.sv
../verilog/DecoupledClickDemux.sv
../verilog/DecoupledClickMerge.sv
../verilog/DecoupledClickFork.sv
../verilog/DecoupledClickJoin.sv
../verilog/DecoupledClickStage.sv
../verilog/DecoupledClickElement.sv
../verilog/gcd_tb.sv
./gcd.v
```

Which should be placed in the `target` directory, assuming that is where you ran `yakc`.

```
iverilog -o top -c file_list.txt
vvp top
```

