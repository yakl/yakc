module Demux
  #(parameter DATA_WIDTH = 1)
   (input req_in0, ack_out0, ack_out1, req_sel0, 
    input [DATA_WIDTH - 1 : 0] data_in0,
    input data_sel0,
    output [DATA_WIDTH - 1 : 0] data_out0, 
    output [DATA_WIDTH - 1 : 0] data_out1, 
    output ack_in0, ack_sel0, req_out0, req_out1,
    input rst);

   reg                           phase_o0_P;
   reg                           phase_o1_P;
   reg                           phase_i_P;
   wire                          phase_i_clk;
   wire                          phase_o_clk;

   assign req_out0 = phase_o0_P;
   assign req_out1 = phase_o1_P;

   assign data_out0 = data_in0;
   assign data_out1 = data_in0;

   assign ack_in0 = phase_i_P;
   assign ack_sel0 = phase_i_P;

   assign #5 phase_i_clk = (req_out0 ~^ ack_out0) & (req_out1 ~^ ack_out1);
   assign #5 phase_o_clk = (req_in0 ^ ack_in0) & (req_sel0 ^ ack_sel0);

   always @ (posedge phase_i_clk, posedge rst) begin
      if (rst == 1'b1) begin
         phase_i_P <= 0;
      end
      else begin
         phase_i_P <= ~phase_i_P;
      end
   end

   always @ (posedge phase_o_clk, posedge rst) begin
      if (rst == 1'b1) begin
         phase_o0_P <= 0;
         phase_o1_P <= 0;
      end
      else begin
         phase_o0_P <= phase_o0_P ^ ~data_sel0;
         phase_o1_P <= phase_o1_P ^ data_sel0;
      end
   end

endmodule
