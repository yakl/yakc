use std::collections::{HashMap, HashSet};

#[derive(Clone)]
pub struct ScopedSymTable<T> {
    // TODO make this &str instead of String
    tables: Vec<HashMap<String, T>>,
}

impl<T> ScopedSymTable<T> {
    pub fn insert_sym(&mut self, name: String, sym: T) {
        self.tables
            .last_mut()
            .expect("No symbol table available")
            .insert(name, sym);
    }

    pub fn get(&self, name: &String) -> Option<&T> {
        self.tables.iter().rev().find_map(|table| table.get(name))
    }

    pub fn get_mut(&mut self, name: &String) -> Option<&mut T> {
        self.tables
            .iter_mut()
            .rev()
            .find_map(|table| table.get_mut(name))
    }

    pub fn enter_scope(&mut self) {
        self.tables.push(HashMap::new());
    }

    pub fn leave_scope(&mut self) {
        self.tables.pop();
    }

    pub fn get_all_names(&self) -> HashSet<String> {
        let mut names = HashSet::<String>::new();
        self.tables
            .iter()
            .for_each(|map| names.extend(map.keys().cloned()));

        names
    }

    pub fn new() -> Self {
        Self { tables: Vec::new() }
    }
}
