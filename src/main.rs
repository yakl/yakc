use clap::Parser;
use std::path::PathBuf;

#[derive(Parser)]
#[command(author, version, about, long_about = None, arg_required_else_help = true)]
struct Cli {
    /// Yak file
    #[arg(short, long, value_name = "FILE")]
    file: Option<PathBuf>,

    /// Generate Verilog into FILE
    #[arg(short, long, value_name = "FILE")]
    verilog: Option<PathBuf>,

    /// Generate SDC into FILE (only for decoupled click element implementation)
    #[arg(long)]
    gen_sdc_demo: Option<PathBuf>,
}

fn main() {
    let cli = Cli::parse();

    let Some(yak_file) = cli.file else {unreachable!("Can't get here because of clap");};
    let source = std::fs::read_to_string(yak_file).unwrap();

    yakc::run(&source, cli.verilog, cli.gen_sdc_demo);
}
