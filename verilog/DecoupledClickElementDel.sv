module DecoupledClickElement
  #(parameter PI = 0,
    parameter PO = 0)
   (input Ri, Ao, rst,
    output Ro, Ai, clk );

   wire                               xorleft;
   wire                               xnorright;
   wire                               clickAnd;
   wire                               Pin, Pon;
   reg                                Pi, Po;
   wire clk_;
   wire clk_1;
   wire clk_2;
   wire clk_3;
   wire clk_4;
   wire clk_5;
   wire clk_6;

   assign xorleft = Ri ^ Ai;
   assign xnorright = Ro ~^ Ao;
   assign clickAnd = xorleft & xnorright;
   DEL4 c_element_delay (.I(clickAnd), .Z(clk_));
   DEL4 c_element_delay1 (.I(clk_), .Z(clk_1));
   DEL4 c_element_delay2 (.I(clk_1), .Z(clk_2));
   DEL4 c_element_delay3 (.I(clk_2), .Z(clk_3));
   DEL4 c_element_delay4 (.I(clk_3), .Z(clk_4));
   DEL4 c_element_delay5 (.I(clk_4), .Z(clk_5));
   DEL4 c_element_delay6 (.I(clk_5), .Z(clk_6));
   assign clk = clk_6;
   assign Ai = Pi;
   assign Ro = Po;
   assign Pin = ~Pi;
   assign Pon = ~Po;

   always_ff @ (posedge clk_6 or posedge rst) begin
      if (rst == 1'b1)
        begin
           Pi <= PI;
           Po <= PO;
        end
      else
        begin
           Pi <= Pin;
           Po <= Pon;
        end
   end

endmodule
