module DecoupledClickElement
  #(parameter PI = 0,
    parameter PO = 0)
   (input Ri, Ao, rst,
    output Ro, Ai, clk );

   wire                               xorleft;
   wire                               xnorright;
   wire                               clickAnd;
   wire                               Pin, Pon;
   reg                                Pi, Po;

   assign xorleft = Ri ^ Ai;
   assign xnorright = Ro ~^ Ao;
   assign clickAnd = xorleft & xnorright;
   assign clk = clickAnd;
   assign Ai = Pi;
   assign Ro = Po;
   assign Pin = ~Pi;
   assign Pon = ~Po;

   always_ff @ (posedge clickAnd or posedge rst) begin
      if (rst == 1'b1)
        begin
           Pi <= PI;
           Po <= PO;
        end
      else
        begin
           Pi <= Pin;
           Po <= Pon;
        end
   end

endmodule
