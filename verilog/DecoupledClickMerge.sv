module Merge
  #(parameter DATA_WIDTH = 1)
   (input req_in0, req_in1, 
    input [DATA_WIDTH - 1 : 0] data_in0, 
    input [DATA_WIDTH - 1 : 0] data_in1, 
    input ack_out0,
    output req_out0, ack_in0, ack_in1, 
    output reg [DATA_WIDTH - 1 : 0] data_out0,
    input rst);

   reg                           phase_i0_P;
   reg                           phase_i1_P;
   reg                           phase_o_P;
   wire                          phase_o_N;
   wire                          phase_i_clk;
   wire                          phase_o_clk;
   wire                          select0, select1;

   assign ack_in0 = phase_i0_P;
   assign ack_in1 = phase_i1_P;
   assign req_out0 = phase_o_P;

   assign #5 phase_i_clk = req_out0 ~^ ack_out0;

   assign select0 = req_in0 ^ ack_in0;
   assign select1 = req_in1 ^ ack_in1;
   assign #5 phase_o_clk = select0 | select1;

   assign phase_o_N = ~phase_o_P;

   always @(*) begin
      if (select0 == 1'b1)
        data_out0 <= data_in0;
      else
        data_out0 <= data_in1;
   end

   always @ (posedge phase_i_clk, posedge rst) begin
      if (rst == 1'b1) begin
         phase_i0_P <= 0;
         phase_i1_P <= 0;
      end
      else begin
         phase_i0_P <= req_in0;
         phase_i1_P <= req_in1;
      end
   end

   always @ (posedge phase_o_clk, posedge rst) begin
      if (rst == 1'b1) begin
         phase_o_P <= 0;
      end
      else begin
         phase_o_P <= phase_o_N;
      end
   end

endmodule // DecoupledClickMerge
