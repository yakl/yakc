use chumsky::prelude::*;
use extra::Err;
use std::{fmt, ops::Range};

pub type Span = SimpleSpan<usize>;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Op {
    Pipe,
    Assign,
    Lt,
    Gt,
    Eq,
    Neq,
    Lte,
    Gte,
    Plus,
    Minus,
    Div,
    Mul,
    Negate,
    And,
    Or,
    Bor,
    Band,
}

impl fmt::Display for Op {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Op::*;
        match self {
            Pipe => write!(f, "->"),
            Assign => write!(f, "="),
            Lt => write!(f, "<"),
            Gt => write!(f, ">"),
            Eq => write!(f, "=="),
            Neq => write!(f, "!="),
            Lte => write!(f, "<="),
            Gte => write!(f, ">="),
            Plus => write!(f, "+"),
            Minus => write!(f, "-"),
            Div => write!(f, "/"),
            Mul => write!(f, "*"),
            Negate => write!(f, "!"),
            And => write!(f, "&&"),
            Or => write!(f, "||"),
            Bor => write!(f, "|"),
            Band => write!(f, "&"),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Hash, Eq)]
pub enum Keyword {
    If,
    Else,
    Sig,
    Fn,
    Chan,
    Input,
    Output,
    Comb,
}

impl fmt::Display for Keyword {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Keyword::*;
        match self {
            If => write!(f, "if"),
            Else => write!(f, "else"),
            Sig => write!(f, "Sig"),
            Fn => write!(f, "Fn"),
            Chan => write!(f, "Chan"),
            Input => write!(f, "Input"),
            Output => write!(f, "Output"),
            Comb => write!(f, "Comb"),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Hash, Eq)]
pub enum Token<'src> {
    Ident(&'src str),
    Nat(i32),
    Op(Op),
    Keyword(Keyword),
    Colon,
    SemiColon,
    Comma,
    Dot,
    Rbrace,
    Lbrace,
    Rbrack,
    Lbrack,
    Rparen,
    Lparen,
}

impl<'src> fmt::Display for Token<'src> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Token::*;
        match self {
            Ident(s) => write!(f, "{}", s),
            Op(op) => write!(f, "{}", op),
            Keyword(kw) => write!(f, "{}", kw),
            Colon => write!(f, ":"),
            SemiColon => write!(f, ";"),
            Comma => write!(f, ","),
            Dot => write!(f, "."),
            Rbrace => write!(f, "}}"),
            Lbrace => write!(f, "{{"),
            Rbrack => write!(f, "]"),
            Lbrack => write!(f, "["),
            Rparen => write!(f, ")"),
            Lparen => write!(f, "("),
            Nat(n) => write!(f, "{}", n),
        }
    }
}

pub fn make_lexer<'src>(
) -> impl Parser<'src, &'src str, Vec<(Token<'src>, Span)>, extra::Err<Rich<'src, char, Span>>> {
    let op = choice((
        just("->").to(Op::Pipe),
        just("==").to(Op::Eq),
        just("=").to(Op::Assign),
        just("<=").to(Op::Lte),
        just("<").to(Op::Lt),
        just(">=").to(Op::Gte),
        just(">").to(Op::Gt),
        just("!=").to(Op::Neq),
        just("!").to(Op::Negate),
        just("*").to(Op::Mul),
        just("/").to(Op::Div),
        just("+").to(Op::Plus),
        just("-").to(Op::Minus),
        just("&&").to(Op::And),
        just("&").to(Op::Band),
        just("||").to(Op::Or),
        just("|").to(Op::Bor),
    ))
    .map(Token::Op);

    let num = text::int(10).slice().from_str().unwrapped().map(Token::Nat);

    let paren = choice((
        just("(").to(Token::Lparen),
        just(")").to(Token::Rparen),
        just("[").to(Token::Lbrack),
        just("]").to(Token::Rbrack),
        just("{").to(Token::Lbrace),
        just("}").to(Token::Rbrace),
    ));

    let word = text::ident().map(|ident: &str| match ident {
        "if" => Token::Keyword(Keyword::If),
        "else" => Token::Keyword(Keyword::Else),
        "sig" => Token::Keyword(Keyword::Sig),
        "fn" => Token::Keyword(Keyword::Fn),
        "chan" => Token::Keyword(Keyword::Chan),
        "input" => Token::Keyword(Keyword::Input),
        "output" => Token::Keyword(Keyword::Output),
        "comb" => Token::Keyword(Keyword::Comb),
        _ => Token::Ident(ident),
    });

    let token = choice((
        op,
        num,
        paren,
        word,
        just(";").to(Token::SemiColon),
        just(":").to(Token::Colon),
        just(",").to(Token::Comma),
        just(".").to(Token::Dot),
    ));

    token
        .map_with_span(|tok, span| (tok, span))
        .padded()
        .repeated()
        .collect()
}
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn lex_ident() {
        let source = "ident".to_string();
        let lex = make_lexer();
        let output = lex.parse(source.as_str());

        let expected_span = chumsky::span::SimpleSpan::new(0, 5);
        assert_eq!(
            output.output(),
            Some(&vec![(Token::Ident("ident"), expected_span)])
        );
    }

    #[test]
    fn lex_pipe() {
        let source = "ident -> ident -> ident".to_string();
        let lex = make_lexer();
        let output = lex.parse(source.as_str());

        assert_eq!(
            output.output(),
            Some(&vec![
                (Token::Ident("ident"), SimpleSpan::new(0, 5)),
                (Token::Op(Op::Pipe), SimpleSpan::new(6, 8)),
                (Token::Ident("ident"), SimpleSpan::new(9, 14)),
                (Token::Op(Op::Pipe), SimpleSpan::new(15, 17)),
                (Token::Ident("ident"), SimpleSpan::new(18, 23)),
            ])
        );
    }
    #[test]
    fn lex_something() {
        let source = r###"chan; g : {a : logic[12:0], b : logic[12:0]};
                          g -> comb {if (a > b) {a = a - b;}} -> sink(sig a : logic[12:0], sig b : logic[12:0]);"###;

        let output = make_lexer().parse(source);
        println!("{:?}", output);
        assert!(!output.has_errors());
    }

    #[test]
    fn lex_gcd_naive() {
        let source = r###"
{
chan a : {a : logic[12:0]};
chan b : {b : logic[12:0]};
chan loop_s;

input(a, sig a : logic[12:0]) -> a;
input(b, sig b : logic[12:0]) -> b;

[[a, b] -> join(), chan loop_val] -> mux(loop_s) -> fork() ->
[
        comb {sig s : logic[0:0] = a != b;} -> fork() -> [reg(sig s : logic[0:0] = 1) -> loop_s, chan output_s],
        demux(output_s) -> [output(o, sig a : logic[12:0], sig b : logic[12:0]),
                           fork() -> [comb {sig s : logic[0:0] = a > b;} -> chan branch_s,
                                     demux(branch_s) -> [
                                                        comb {a[7:0] = a[7:0] - b[7:0];},
                                                        comb {b = b - a;}
                                                        ]
                                     -> merge() -> loop_val
                                     ]
                           ]
];
}
"###;

        let output = make_lexer().parse(source);
        println!("{:?}", output);
        assert!(!output.has_errors());
    }

    #[test]
    fn lex_gcd_opt() {
        let source = r###"
{
chan a : {a : logic[12:0]};
chan b : {b : logic[12:0]};
chan loop_s;

input(a, sig a : logic[12:0]) -> a;
input(b, sig b : logic[12:0]) -> b;

[[a, b] -> join(), chan loop_val] -> mux(loop_s) -> reg() -> fork() ->
[
        comb {sig s : logic[0:0] = a != b;} -> fork() -> [reg(sig s : logic[0:0] = 0) -> loop_s, chan output_s],
        demux(output_s) -> [output(o, sig a : logic[12:0], sig b : logic[12:0]),
                            reg() -> comb { if (a > b) {a = a - b;} else {b = b - a;} } -> loop_val
                           ]
];
}
"###;

        let output = make_lexer().parse(source);
        println!("{:?}", output);
        assert!(!output.has_errors());
    }
}
