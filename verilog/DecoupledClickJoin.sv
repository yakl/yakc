module Join
  #(parameter DATA0_WIDTH = 1,
    parameter DATA1_WIDTH = 1)
   ( input req_in0, req_in1, ack_out0, rst, 
     input [DATA0_WIDTH-1:0] data_in0,
     input [DATA1_WIDTH-1:0] data_in1,
     output req_out0, ack_in0, ack_in1, 
     output [DATA0_WIDTH + DATA1_WIDTH - 1:0] data_out0 );

   reg                             Phase_P;
   wire                            Phase_N;
   wire                            click;
   assign ack_in0 = ack_out0;
   assign ack_in1 = ack_out0;
   assign data_out0 = {data_in1, data_in0};
   assign req_out0 = Phase_P;


   assign #5 click = (~req_out0 & req_in0 & req_in1) | (req_out0 & ~req_in0 & ~req_in1);
   assign Phase_N = ~Phase_P;


   always @ (posedge click, posedge rst) begin
      if (rst == 1'b1)
        Phase_P <= 1'b0;
      else
        Phase_P <= Phase_N;
   end
endmodule
