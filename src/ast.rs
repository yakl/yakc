use ariadne::{Color, Label, Report, ReportKind, Source};
use chumsky::prelude::*;
use extra::Err;
use std::{fmt, ops::Range};

use super::token;
use super::token::{Op, Span, Token};

#[derive(Clone, Debug)]
pub enum Node {
    Stmt {},
    CompoundStmt {
        stmts: Vec<Spanned<Node>>,
    },
    BinOpExpr {
        op: token::Op,
        lhs: Box<Spanned<Node>>,
        rhs: Box<Spanned<Node>>,
    },
    NumLitExpr {
        val: i32,
    },
    VarExpr {
        name: String,
    },
    Slice {
        high: Box<Spanned<Node>>,
        low: Box<Spanned<Node>>,
    },
    SliceExpr {
        expr: Box<Spanned<Node>>,
        slice: Box<Spanned<Node>>,
    },
    AggrExpr {
        exprs: Vec<Spanned<Node>>,
    },
    TypeSpec {
        ident: String,
        slice: Option<Box<Spanned<Node>>>,
    },
    ChanDeclExpr {
        name: String,
        sigs: Option<Vec<Spanned<Node>>>,
    },
    AggrType {
        typespec: Vec<Spanned<Node>>,
    },
    FunDeclExpr {
        name: String,
        inputs: Vec<Spanned<Node>>,
        sides: Vec<Spanned<Node>>,
        outputs: Vec<Spanned<Node>>,
        body: Box<Spanned<Node>>,
    },
    IfStmt {
        cond: Box<Spanned<Node>>,
        body: Box<Spanned<Node>>,
        elsebody: Option<Box<Spanned<Node>>>,
    },
    FunCall {
        name: String,
        args: Vec<Spanned<Node>>,
    },
    Comb {
        stmt: Box<Spanned<Node>>,
    },
    SigDeclExpr {
        ident: String,
        typespec: Option<Box<Spanned<Node>>>,
        default: Option<Box<Spanned<Node>>>,
        slice: Option<Box<Spanned<Node>>>,
    },
    InputDecl {
        chan: Box<Spanned<Node>>,
    },
    OutputDecl {
        chan: Box<Spanned<Node>>,
    },
    ErrorNode {},
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum UnspannedNode {
    Stmt {},
    CompoundStmt {
        stmts: Vec<UnspannedNode>,
    },
    BinOpExpr {
        op: token::Op,
        lhs: Box<UnspannedNode>,
        rhs: Box<UnspannedNode>,
    },
    NumLitExpr {
        val: i32,
    },
    VarExpr {
        name: String,
    },
    Slice {
        high: Box<UnspannedNode>,
        low: Box<UnspannedNode>,
    },
    AggrExpr {
        exprs: Vec<UnspannedNode>,
    },
    SliceExpr {
        expr: Box<UnspannedNode>,
        slice: Box<UnspannedNode>,
    },
    TypeSpec {
        ident: String,
        slice: Option<Box<UnspannedNode>>,
    },
    ChanDeclExpr {
        name: String,
        sigs: Option<Vec<UnspannedNode>>,
    },
    AggrType {
        typespec: Vec<UnspannedNode>,
    },
    FunDeclExpr {
        name: String,
        inputs: Vec<UnspannedNode>,
        sides: Vec<UnspannedNode>,
        outputs: Vec<UnspannedNode>,
        body: Box<UnspannedNode>,
    },
    IfStmt {
        cond: Box<UnspannedNode>,
        body: Box<UnspannedNode>,
        elsebody: Option<Box<UnspannedNode>>,
    },
    FunCall {
        name: String,
        args: Vec<UnspannedNode>,
    },
    Comb {
        stmt: Box<UnspannedNode>,
    },
    SigDeclExpr {
        ident: String,
        typespec: Option<Box<UnspannedNode>>,
        default: Option<Box<UnspannedNode>>,
        slice: Option<Box<UnspannedNode>>,
    },
    InputDecl {
        chan: Box<UnspannedNode>,
    },
    OutputDecl {
        chan: Box<UnspannedNode>,
    },
    ErrorNode {},
}

impl From<Node> for UnspannedNode {
    fn from(value: Node) -> Self {
        let vec_convert = |x: Vec<Spanned<Node>>| x.into_iter().map(|el| el.inner.into()).collect();
        match value {
            Node::Stmt {} => UnspannedNode::Stmt {},
            Node::CompoundStmt { stmts } => UnspannedNode::CompoundStmt {
                stmts: vec_convert(stmts),
            },
            Node::BinOpExpr { op, lhs, rhs } => UnspannedNode::BinOpExpr {
                op,
                lhs: Box::new(lhs.inner.into()),
                rhs: Box::new(rhs.inner.into()),
            },
            Node::NumLitExpr { val } => UnspannedNode::NumLitExpr { val },
            Node::VarExpr { name } => UnspannedNode::VarExpr { name },
            Node::Slice { high, low } => UnspannedNode::Slice {
                high: Box::new(high.inner.into()),
                low: Box::new(low.inner.into()),
            },
            Node::SliceExpr { expr, slice } => UnspannedNode::SliceExpr {
                expr: Box::new(expr.inner.into()),
                slice: Box::new(slice.inner.into()),
            },
            Node::AggrExpr { exprs } => UnspannedNode::AggrExpr {
                exprs: vec_convert(exprs),
            },
            Node::TypeSpec { ident, slice } => UnspannedNode::TypeSpec {
                ident,
                slice: slice.map(|x| Box::new(x.inner.into())),
            },
            Node::ChanDeclExpr { name, sigs } => UnspannedNode::ChanDeclExpr {
                name,
                sigs: sigs.map(vec_convert),
            },
            Node::AggrType { typespec } => UnspannedNode::AggrType {
                typespec: typespec.into_iter().map(|x| x.inner.into()).collect(),
            },
            Node::FunDeclExpr {
                name,
                inputs,
                sides,
                outputs,
                body,
            } => UnspannedNode::FunDeclExpr {
                name,
                inputs: vec_convert(inputs),
                sides: vec_convert(sides),
                outputs: vec_convert(outputs),
                body: Box::new(body.inner.into()),
            },
            Node::IfStmt {
                cond,
                body,
                elsebody,
            } => UnspannedNode::IfStmt {
                cond: Box::new(cond.inner.into()),
                body: Box::new(body.inner.into()),
                elsebody: elsebody.map(|x| Box::new(x.inner.into())),
            },
            Node::FunCall { name, args } => UnspannedNode::FunCall {
                name,
                args: vec_convert(args),
            },
            Node::Comb { stmt } => UnspannedNode::Comb {
                stmt: Box::new(stmt.inner.into()),
            },
            Node::SigDeclExpr {
                ident,
                typespec,
                default,
                slice,
            } => UnspannedNode::SigDeclExpr {
                ident,
                typespec: typespec.map(|x| Box::new(x.inner.into())),
                default: default.map(|x| Box::new(x.inner.into())),
                slice: slice.map(|x| Box::new(x.inner.into())),
            },
            Node::InputDecl { chan } => UnspannedNode::InputDecl {
                chan: Box::new(chan.inner.into()),
            },
            Node::OutputDecl { chan } => UnspannedNode::OutputDecl {
                chan: Box::new(chan.inner.into()),
            },
            Node::ErrorNode {} => UnspannedNode::ErrorNode {},
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Spanned<T> {
    pub inner: T,
    pub span: Span,
}

impl<T> Spanned<T> {
    pub fn new(inner: T, span: Span) -> Self {
        Self { inner, span }
    }
}

impl<T> From<(T, Span)> for Spanned<T> {
    fn from(value: (T, Span)) -> Self {
        Self {
            inner: value.0,
            span: value.1,
        }
    }
}

type SpanNode = Spanned<Node>;

type ParserInput<'tokens, 'src> =
    chumsky::input::SpannedInput<Token<'src>, Span, &'tokens [(Token<'src>, Span)]>;

// Grammar
//
// Structural Yak follows the grammar (whitespace is ignored and only included
// here to make the grammar copy-pastable into EBNF testers)
//
// prog ::= decl+
// desc ::= stmt
// compound_stmt ::= "{"[whitespace+] stmt* "}"
// stmt ::= expr";"[whitespace+] | compound_stmt
// expr ::= primary | binary_expr
// aggregate_expr ::= "["[[expr] [", " expr]+]"]"
// primary ::= {identifier | number | "("expr")" | fun_call | aggregate_expr | chandecl | sigdecl} [slice]
// chandecl ::= chan identifier [: {typespec}]
// sigdecl ::= sig identifier [: typespec]
// typespec ::= identifier slice
// slice ::= ["["number ":" number"]"]
// typeident ::= auto | identifier[<identifier>] ##currently unused. chandecl only uses the chan keyword now
// fun_call ::= identifier"("[[expr] [", " expr]+]")"
// fun_decl ::= def [chandecl*]({sigdecl, chandecl)*)[chandecl*]
// unary_expr ::= [unary_op] primary
// unary_op ::= "-"
// binary_expr ::= primary (binary_op primary)*
// binary_op ::= "+" | "-" | "/" | "*" | "->"
// number ::= #'[1-9][0-9]*'
// identifier ::= #'[A-Za-z][A-Za-z0-9_+]*'
// whitespace ::= #'\s+'

pub fn parse_comb_expr<'tokens, 'src: 'tokens>() -> impl Parser<
    'tokens,
    ParserInput<'tokens, 'src>,
    Spanned<Node>,
    extra::Err<Rich<'tokens, Token<'src>, Span>>,
> + Clone {
    recursive(|expr| {
        let val = select! {
        Token::Nat(x) => Node::NumLitExpr { val: x }
        }
        .map_with_span(SpanNode::new);

        let ident = select! {
            Token::Ident(s) => s.to_string()
        };

        let slice = just(Token::Lbrack)
            .ignore_then(val)
            .then_ignore(just(Token::Colon))
            .then(val)
            .then_ignore(just(Token::Rbrack))
            .map_with_span(|(high, low), span| {
                SpanNode::new(
                    Node::Slice {
                        high: Box::new(high),
                        low: Box::new(low),
                    },
                    span,
                )
            });

        let typespec = ident
            .then(slice.or_not())
            .map_with_span(|(ident, slice), span| {
                SpanNode::new(
                    Node::TypeSpec {
                        ident,
                        slice: slice.map(Box::new),
                    },
                    span,
                )
            });

        let sig_decl = just(Token::Keyword(token::Keyword::Sig))
            .ignore_then(ident)
            .then(just(Token::Colon).ignore_then(typespec).or_not())
            .then(
                just(Token::Op(Op::Assign))
                    .ignore_then(expr.clone())
                    .or_not(),
            )
            .map_with_span(|((ident, typespec), default), span| {
                SpanNode::new(
                    Node::SigDeclExpr {
                        ident,
                        typespec: typespec.map(Box::new),
                        default: default.map(Box::new),
                        slice: None,
                    },
                    span,
                )
            });

        let primary = choice((sig_decl, val))
            .or(ident.map_with_span(|name, span| (Node::VarExpr { name }, span).into()))
            .or(expr
                .clone()
                .delimited_by(just(Token::Lparen), just(Token::Rparen)))
            .then(slice.or_not())
            .map_with_span(|(mut prim, slice), span| {
                if let Some(slice) = slice {
                    (
                        if let Node::SigDeclExpr {
                            slice: sig_decl_slice,
                            ..
                        } = &mut prim.inner
                        {
                            *sig_decl_slice = Some(Box::new(slice));
                            prim.inner
                        } else {
                            Node::SliceExpr {
                                expr: Box::new(prim),
                                slice: Box::new(slice),
                            }
                        },
                        span,
                    )
                        .into()
                } else {
                    prim
                }
            })
            .boxed();

        let bin_maker = |op| {
            move |lhs, rhs| Node::BinOpExpr {
                op,
                lhs: Box::new(lhs),
                rhs: Box::new(rhs),
            }
        };

        let product = primary.clone().foldl(
            just(Token::Op(Op::Mul))
                .to(bin_maker(Op::Mul))
                .or(just(Token::Op(Op::Div)).to(bin_maker(Op::Mul)))
                .then(primary.clone())
                .repeated(),
            |lhs, (op, rhs)| {
                let span = SimpleSpan::new(lhs.span.start, rhs.span.end);
                (op(lhs, rhs), span).into()
            },
        );

        let sum = product.clone().foldl(
            just(Token::Op(Op::Plus))
                .to(bin_maker(Op::Plus))
                .or(just(Token::Op(Op::Minus)).to(bin_maker(Op::Minus)))
                .then(product)
                .repeated(),
            |lhs, (op, rhs)| {
                let span = SimpleSpan::new(lhs.span.start, rhs.span.end);
                (op(lhs, rhs), span).into()
            },
        );

        let comparison = sum.clone().foldl(
            just(Token::Op(Op::Lt))
                .to(bin_maker(Op::Lt))
                .or(just(Token::Op(Op::Gt)).to(bin_maker(Op::Gt)))
                .or(just(Token::Op(Op::Lte)).to(bin_maker(Op::Lte)))
                .or(just(Token::Op(Op::Gte)).to(bin_maker(Op::Gte)))
                .then(sum)
                .repeated(),
            |lhs, (op, rhs)| {
                let span = SimpleSpan::new(lhs.span.start, rhs.span.end);
                (op(lhs, rhs), span).into()
            },
        );

        let equality = comparison.clone().foldl(
            just(Token::Op(Op::Eq))
                .to(bin_maker(Op::Eq))
                .or(just(Token::Op(Op::Neq)).to(bin_maker(Op::Neq)))
                .then(comparison)
                .repeated(),
            |lhs, (op, rhs)| {
                let span = SimpleSpan::new(lhs.span.start, rhs.span.end);
                (op(lhs, rhs), span).into()
            },
        );

        let assignment = equality
            .clone()
            .then(
                just(Token::Op(Op::Assign))
                    .to(bin_maker(Op::Assign))
                    .then(expr.clone())
                    .or_not(),
            )
            .map(|(lhs, op_rhs)| match op_rhs {
                Some((op, rhs)) => {
                    let span = SimpleSpan::new(lhs.span.start, rhs.span.end);
                    (op(lhs, rhs), span).into()
                }
                None => lhs,
            });

        assignment
    })
}

pub fn parse_comb_stmt<'tokens, 'src: 'tokens>() -> impl Parser<
    'tokens,
    ParserInput<'tokens, 'src>,
    Spanned<Node>,
    extra::Err<Rich<'tokens, Token<'src>, Span>>,
> + Clone {
    recursive(|stmt| {
        let if_stmt = just(Token::Keyword(token::Keyword::If))
            .ignore_then(parse_comb_expr().delimited_by(just(Token::Lparen), just(Token::Rparen)))
            .then(stmt.clone())
            .then(
                just(Token::Keyword(token::Keyword::Else))
                    .ignore_then(stmt.clone())
                    .or_not(),
            )
            .map_with_span(|((cond, body), elsebody), span| {
                SpanNode::new(
                    Node::IfStmt {
                        cond: Box::new(cond),
                        body: Box::new(body),
                        elsebody: elsebody.map(Box::new),
                    },
                    span,
                )
            });

        let block = stmt
            .repeated()
            .collect()
            .delimited_by(just(Token::Lbrace), just(Token::Rbrace))
            .map_with_span(|stmts, span| SpanNode::new(Node::CompoundStmt { stmts }, span));

        let expr_stmt = parse_comb_expr()
            .then_ignore(just(Token::SemiColon))
            .map(|x| x);

        choice((if_stmt, block, expr_stmt))
    })
}

pub fn parse_structural_expr<'tokens, 'src: 'tokens>() -> impl Parser<
    'tokens,
    ParserInput<'tokens, 'src>,
    Spanned<Node>,
    extra::Err<Rich<'tokens, Token<'src>, Span>>,
> + Clone {
    recursive(|expr| {
        let val = select! {
        Token::Nat(x) => Node::NumLitExpr { val: x }
        }
        .map_with_span(SpanNode::new);

        let ident = select! {
            Token::Ident(s) => s.to_string()
        };

        let slice = just(Token::Lbrack)
            .ignore_then(val)
            .then_ignore(just(Token::Colon))
            .then(val)
            .then_ignore(just(Token::Rbrack))
            .map_with_span(|(high, low), span| {
                SpanNode::new(
                    Node::Slice {
                        high: Box::new(high),
                        low: Box::new(low),
                    },
                    span,
                )
            });

        let typespec = ident
            .then(slice.or_not())
            .map_with_span(|(ident, slice), span| {
                SpanNode::new(
                    Node::TypeSpec {
                        ident,
                        slice: slice.map(Box::new),
                    },
                    span,
                )
            });

        let sig_decl = just(Token::Keyword(token::Keyword::Sig))
            .ignore_then(ident)
            .then(just(Token::Colon).ignore_then(typespec).or_not())
            .then(
                just(Token::Op(Op::Assign))
                    .ignore_then(parse_comb_expr())
                    .or_not(),
            )
            .map_with_span(|((ident, typespec), default), span| {
                SpanNode::new(
                    Node::SigDeclExpr {
                        ident,
                        typespec: typespec.map(Box::new),
                        default: default.map(Box::new),
                        slice: None,
                    },
                    span,
                )
            });

        let chan_decl = just(Token::Keyword(token::Keyword::Chan))
            .ignore_then(ident)
            .then(
                just(Token::Colon)
                    .ignore_then(
                        sig_decl
                            .clone()
                            .separated_by(just(Token::Comma))
                            .allow_trailing()
                            .collect()
                            .delimited_by(just(Token::Lbrace), just(Token::Rbrace)),
                    )
                    .or_not(),
            )
            .map_with_span(|(name, sigs), span| (Node::ChanDeclExpr { name, sigs }, span).into());

        let aggr = expr
            .clone()
            .separated_by(just(Token::Comma))
            .allow_trailing()
            .collect()
            .delimited_by(just(Token::Lbrack), just(Token::Rbrack))
            .map_with_span(|exprs, span| (Node::AggrExpr { exprs }, span).into());

        let fun_call = ident
            .then(
                expr.clone()
                    .separated_by(just(Token::Comma))
                    .allow_trailing()
                    .collect()
                    .delimited_by(just(Token::Lparen), just(Token::Rparen)),
            )
            .map_with_span(|(name, args), span| (Node::FunCall { name, args }, span).into());

        let input = just(Token::Keyword(token::Keyword::Input))
            .ignore_then(chan_decl.clone())
            .map_with_span(|chan, span| {
                (
                    Node::InputDecl {
                        chan: Box::new(chan),
                    },
                    span,
                )
                    .into()
            });

        let output = just(Token::Keyword(token::Keyword::Output))
            .ignore_then(chan_decl.clone())
            .map_with_span(|chan, span| {
                (
                    Node::OutputDecl {
                        chan: Box::new(chan),
                    },
                    span,
                )
                    .into()
            });

        let comb = just(Token::Keyword(token::Keyword::Comb))
            .ignore_then(parse_comb_stmt())
            .map_with_span(|stmt, span| {
                (
                    Node::Comb {
                        stmt: Box::new(stmt),
                    },
                    span,
                )
                    .into()
            });

        let primary = choice((
            fun_call, input, output, comb, sig_decl, aggr, chan_decl, val,
        ))
        .or(ident.map_with_span(|name, span| (Node::VarExpr { name }, span).into()))
        .or(expr
            .clone()
            .delimited_by(just(Token::Lparen), just(Token::Rparen)))
        .boxed();

        let pipe = primary.clone().foldl(
            just(Token::Op(Op::Pipe))
                .to(Op::Pipe)
                .then(primary)
                .repeated(),
            |lhs, (op, rhs)| {
                let span = lhs.span.start()..rhs.span.end();
                (
                    Node::BinOpExpr {
                        op,
                        lhs: Box::new(lhs),
                        rhs: Box::new(rhs),
                    },
                    span.into(),
                )
                    .into()
            },
        );

        pipe
    })
}

pub fn parse_structural_stmt<'tokens, 'src: 'tokens>() -> impl Parser<
    'tokens,
    ParserInput<'tokens, 'src>,
    Spanned<Node>,
    extra::Err<Rich<'tokens, Token<'src>, Span>>,
> + Clone {
    let stmt = recursive(|stmt| {
        let ident = select! {
            Token::Ident(s) => s.to_string()
        };
        let expr = parse_structural_expr();

        let block = stmt
            .repeated()
            .collect()
            .delimited_by(just(Token::Lbrace), just(Token::Rbrace))
            .map_with_span(|stmts, span| SpanNode::new(Node::CompoundStmt { stmts }, span));

        let expr_stmt = expr.clone().then_ignore(just(Token::SemiColon)).map(|x| x);

        let fun_decl = just(Token::Keyword(token::Keyword::Fn))
            .ignore_then(ident)
            .then(
                expr.clone()
                    .separated_by(just(Token::Comma))
                    .allow_trailing()
                    .collect()
                    .delimited_by(just(Token::Lbrack), just(Token::Rbrack)),
            )
            .then(
                expr.clone()
                    .separated_by(just(Token::Comma))
                    .allow_trailing()
                    .collect()
                    .delimited_by(just(Token::Lparen), just(Token::Rparen)),
            )
            .then(
                expr.clone()
                    .separated_by(just(Token::Comma))
                    .allow_trailing()
                    .collect()
                    .delimited_by(just(Token::Lbrack), just(Token::Rbrack)),
            )
            .then(block.clone())
            .map_with_span(|((((name, inputs), sides), outputs), body), span| {
                (
                    Node::FunDeclExpr {
                        name,
                        inputs,
                        sides,
                        outputs,
                        body: Box::new(body),
                    },
                    span,
                )
                    .into()
            });

        choice((block, expr_stmt, fun_decl))
    });

    // Have to handle top level becoming a single node
    stmt.repeated()
        .collect()
        .map_with_span(|stmts, span| (Node::CompoundStmt { stmts }, span).into())
}

#[cfg(test)]
mod test {
    use std::matches;

    use super::*;

    #[test]
    fn parse_simple_pipe() {
        let source = r###"
a -> b -> c
"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        println!("{:?}", tokens);
        let output = parse_structural_expr().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        println!("{:?}", output);
        assert!(!output.has_errors());
    }

    fn match_without_span(lhs: &Spanned<Node>, rhs: &Spanned<Node>) -> bool {
        use Node::*;
        match &lhs.inner {
            Stmt {} => matches!(rhs.inner, Stmt {}),
            CompoundStmt { stmts: lhs } => {
                if let CompoundStmt { stmts: ref rhs } = rhs.inner {
                    if lhs.len() != rhs.len() {
                        false
                    } else {
                        lhs.iter()
                            .zip(rhs.iter())
                            .map(|(a, b)| match_without_span(a, b))
                            .all(|x| x)
                    }
                } else {
                    false
                }
            }
            _ => false,
        }
    }

    #[test]
    fn parse_comb_statements() {
        let source = r###"
        {
        sig a : logic[7:0];
        sig b : logic[7:0] = 0;
        a + b * c;
        b = a + c;
        if (a == 0) {
        b = c;
        } else if (a == 1) {
        c = b;
        } else a = b;
        }
        "###;

        let tokens = token::make_lexer().parse(source).unwrap();
        println!("{:?}", tokens);
        let output = parse_comb_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        println!("{:?}", output);
        assert!(!output.has_errors());
    }

    fn parse_comb_plus() {
        let source = r###"
a + b
        "###;

        let tokens = token::make_lexer().parse(source).unwrap();
        println!("{:?}", tokens);
        let output = parse_comb_expr().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        println!("{:?}", output);
        assert!(!output.has_errors());

        let output = output.output().unwrap();
        assert_eq!(
            UnspannedNode::from(output.inner.clone()),
            UnspannedNode::BinOpExpr {
                op: Op::Plus,
                lhs: Box::new(UnspannedNode::VarExpr {
                    name: "a".to_string()
                }),
                rhs: Box::new(UnspannedNode::VarExpr {
                    name: "b".to_string()
                })
            }
        )
    }

    #[test]
    fn parse_chandecl() {
        let source = r###"
chan foo : {sig a : logic = b};
        "###;

        let tokens = token::make_lexer().parse(source).unwrap();
        println!("{:?}", tokens);
        let output = parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        println!("{:?}", output);
        assert!(!output.has_errors());

        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(sexpr) => {}
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }
}
