use std::collections::BTreeMap;
use std::collections::HashMap;
use std::collections::VecDeque;
use std::fmt::format;

use crate::cg;
use crate::cg::ComputeNode;
use crate::cg::Type;
use crate::cst::Channel;
use crate::cst::Controller;
use crate::cst::ControllerKind;
use crate::cst::Signal;
use crate::graph::Graph;
use crate::graph::NodeViewRef;
use crate::token;

use super::cst;

use super::ast;
use super::ast::Node;
use super::graph;
use super::graph::{EdgeIdx, EdgesView, GraphEdge, GraphNode, NodeIdx, NodeView};
use super::token::Op;
use super::util::ScopedSymTable;
use ariadne::ReportBuilder;
use ariadne::{Color, ColorGenerator, Fmt, Label, Report, ReportKind, Source};
use by_address::ByAddress;
use chumsky::container::Container;
use itertools::izip;

type SpannedNode = super::ast::Spanned<ast::Node>;

#[derive(Debug, Clone)]
pub enum ProcessNode {
    CompoundStmt {
        stmts: Vec<ProcessNode>,
    },
    BinOpExpr {
        op: token::Op,
        lhs: Box<ProcessNode>,
        rhs: Box<ProcessNode>,
    },
    NumLitExpr {
        val: i32,
    },
    VarExpr {
        name: String,
    },
    SliceExpr {
        expr: Box<ProcessNode>,
        high: u32,
        low: u32,
    },
    IfStmt {
        cond: Box<ProcessNode>,
        body: Box<ProcessNode>,
        elsebody: Option<Box<ProcessNode>>,
    },
    SigDeclExpr {
        ident: String,
        size: u32,
    },
}

fn write_process_nodes(source: &mut String, node: &ProcessNode) {
    use std::fmt::Write;
    match node {
        ProcessNode::BinOpExpr { op, lhs, rhs } => {
            write_process_nodes(source, lhs);
            let _ = write!(source, " {op} ");
            write_process_nodes(source, rhs);
        }
        ProcessNode::CompoundStmt { stmts } => {
            stmts.iter().for_each(|stmt| {
                write_process_nodes(source, stmt);
                let _ = writeln!(source, ";");
            });
        }
        ProcessNode::NumLitExpr { val } => {
            let _ = write!(source, "{val}");
        }
        ProcessNode::SigDeclExpr { ident, size } => unreachable!("Compiler bug"),
        ProcessNode::SliceExpr { expr, high, low } => {
            write_process_nodes(source, expr);
            let _ = write!(source, "[{high}:{low}]");
        }
        ProcessNode::VarExpr { name } => {
            let _ = write!(source, "{name}");
        }
        ProcessNode::IfStmt {
            cond,
            body,
            elsebody,
        } => {
            let _ = write!(source, "if (");
            write_process_nodes(source, cond);
            let _ = writeln!(source, ") begin");
            write_process_nodes(source, body);
            let _ = writeln!(source, ") end");
            if let Some(elsebody) = elsebody {
                let _ = writeln!(source, ") else");
                write_process_nodes(source, elsebody);
            }
        }
    }
}

struct ProcessBuilder<'a> {
    comb_types: &'a HashMap<ByAddress<&'a SpannedNode>, NodeIdx>,
    // These will be used later to allow nested scopes in comb blocks
    // TODO allow nested scopes in comb blocks when generating verilog
    cg: &'a Graph<ComputeNode<'a>, (EdgeIdx, &'a str)>,
    vars: HashMap<String, Type>,
}

impl<'a> ProcessBuilder<'a> {
    fn from_comb<'b>(
        comb_node: &'a NodeViewRef<cst::Channel, Controller<'b>>,
        comb_types: &'b HashMap<ByAddress<&'b SpannedNode>, NodeIdx>,
        cg: &'a Graph<ComputeNode<'b>, (EdgeIdx, &'b str)>,
        sig_table: &'b HashMap<(EdgeIdx, &str), NodeIdx>,
    ) -> ProcessBuilder<'b>
    where
        'a: 'b,
    {
        let vars: HashMap<String, Type> = comb_node
            .edges
            .incs_iter_indexed()
            .chain(comb_node.edges.outs_iter_indexed())
            .flat_map(|(edge, edge_idx)| {
                edge.data.provides.iter().map(move |(name, sig)| {
                    (
                        name.to_string(),
                        cg[*sig_table.get(&(edge_idx, name)).unwrap()]
                            .data
                            .type_
                            .as_ref()
                            .unwrap(),
                    )
                })
            })
            .map(|(name, type_)| (name, type_.clone()))
            .collect();

        let mut builder = Self {
            comb_types,
            cg,
            vars,
        };

        builder.parse_comb_rec(comb_node.data.ast_node);

        builder
    }

    fn parse_comb_rec(&mut self, ast_node: &'a SpannedNode) -> ProcessNode {
        match &ast_node.inner {
            ast::Node::CompoundStmt { stmts } => ProcessNode::CompoundStmt {
                stmts: stmts.iter().map(|stmt| self.parse_comb_rec(stmt)).collect(),
            },
            ast::Node::BinOpExpr { op, lhs, rhs } => {
                let lhs = self.parse_comb_rec(lhs);
                let rhs = self.parse_comb_rec(rhs);

                if op == &Op::Assign {
                    // it is only valid to assign to VarExpr or SliceExpr(var) in Verilog
                    match &lhs {
                        ProcessNode::VarExpr { name } => {
                            if let Some(var_type) = self.vars.get(name) {
                                // everything OK, we have already seen this one
                            } else {
                                // Grab the type of the binary expression from the type graph.
                                // and use it as the type of the var being assigned to for the first time
                                let Some(cg_idx) = self
                                        .comb_types
                                        .get(&ByAddress(ast_node))
				    else { unreachable!("Compiler bug"); };

                                let Some(var_type) = &self.cg[*cg_idx].data.type_ else {unreachable!("Compiler bug");};

                                self.vars.insert(name.to_string(), var_type.clone());
                            }
                        }
                        ProcessNode::SliceExpr { expr, high, low } => {
                            match expr.as_ref() {
                                ProcessNode::VarExpr { name } => {
                                    if let Some(var_type) = self.vars.get(name) {
                                        // everything OK, we have already seen this one
                                    } else {
                                        // Grab the type of the binary expression from the type graph.
                                        // and use it as the type of the var being assigned to for the first time
                                        let Some(cg_idx) = self
						.comb_types
						.get(&ByAddress(ast_node))
					    else { unreachable!("Compiler bug"); };

                                        let Some(var_type) = &self.cg[*cg_idx].data.type_ else {unreachable!("Compiler bug");};

                                        self.vars.insert(name.to_string(), var_type.clone());
                                    }
                                }
                                _ => unreachable!("Compiler bug"),
                            }
                        }
                        _ => unreachable!("Compiler bug"),
                    }
                }

                ProcessNode::BinOpExpr {
                    op: *op,
                    lhs: Box::new(lhs),
                    rhs: Box::new(rhs),
                }
            }
            ast::Node::NumLitExpr { val } => ProcessNode::NumLitExpr { val: *val },
            ast::Node::VarExpr { name } => ProcessNode::VarExpr { name: name.clone() },
            ast::Node::SliceExpr { expr, slice } => {
                if let ast::Node::Slice { high, low } = &slice.inner {
                    let slice_high = if let ast::Node::NumLitExpr { val } = high.inner {
                        val as u32
                    } else {
                        unreachable!("Compiler bug");
                    };

                    let slice_low = if let ast::Node::NumLitExpr { val } = low.inner {
                        val as u32
                    } else {
                        unreachable!("Compiler bug");
                    };

                    ProcessNode::SliceExpr {
                        expr: Box::new(self.parse_comb_rec(expr)),
                        high: slice_high,
                        low: slice_low,
                    }
                } else {
                    unreachable!("Compiler bug")
                }
            }
            ast::Node::IfStmt {
                cond,
                body,
                elsebody,
            } => ProcessNode::IfStmt {
                cond: Box::new(self.parse_comb_rec(cond)),
                body: Box::new(self.parse_comb_rec(body)),
                elsebody: elsebody.as_ref().map(|x| Box::new(self.parse_comb_rec(x))),
            },
            ast::Node::SigDeclExpr {
                ident,
                typespec,
                default,
                slice,
            } => {
                // Verilog requires all process vars to be specified upfront so we collect and rename them as declared
                // Since signals may be declared without type in Yak, we will only know the type on first assignemnt.
                // At that point we can collect the signal we need to declare at the beginning of the process, so this
                // sig decl can instead become a var reference. The validity has already been checked in cst building.

                // Small complicatino from the default initialization parameter which we will here turn into
                // an assignment for reprocessing
                if let Some(default) = default {
                    let Some(cg_idx) = self
			    .comb_types
			    .get(&ByAddress(ast_node))
			else { unreachable!("Compiler bug"); };

                    let Some(var_type) = &self.cg[*cg_idx].data.type_ else {unreachable!("Compiler bug");};

                    self.vars.insert(ident.to_string(), var_type.clone());
                    ProcessNode::BinOpExpr {
                        op: Op::Assign,
                        lhs: Box::new(ProcessNode::VarExpr {
                            name: ident.to_string(),
                        }),
                        rhs: Box::new(self.parse_comb_rec(default)),
                    }
                } else {
                    ProcessNode::VarExpr {
                        name: ident.to_string(),
                    }
                }
            }
            _ => {
                unreachable!("Illegal node in comb should have gotten this far. Compiler bug")
            }
        }
    }
}

#[derive(Debug, Clone)]
pub enum HdlNode<'a> {
    Module {
        inputs: Vec<HdlNode<'a>>,
        outputs: Vec<HdlNode<'a>>,
        channels: Vec<HdlNode<'a>>,
        processes: Vec<HdlNode<'a>>,
        instances: Vec<HdlNode<'a>>,
        assigns: Vec<HdlNode<'a>>,
    },
    CombProcess {
        // TODO Fix having both NodeIdx and reference to the same node.
        cst_node: (NodeIdx, &'a cst::ControllerNode<'a>),
        input: Box<HdlNode<'a>>,
        output: Box<HdlNode<'a>>,
        process_ast: ProcessNode,
        vars: HashMap<String, Type>,
    },

    ControllerInstance {
        cst_node: (NodeIdx, &'a cst::ControllerNode<'a>),
        name: String,
        ports: HashMap<String, HdlNode<'a>>,
        params: HashMap<String, String>,
    },
    Channel {
        cst_edge: (EdgeIdx, &'a cst::ChannelEdge<'a>),
        name: String,
        high: u32,
        low: u32,
        slices: BTreeMap<&'a str, (u32, u32)>,
    },
    Assign {
        from: Box<HdlNode<'a>>,
        to: Box<HdlNode<'a>>,
    },
}

pub struct HdlBuilder<'a> {
    pub comb_types: &'a HashMap<ByAddress<&'a SpannedNode>, NodeIdx>,
    pub cg: &'a Graph<ComputeNode<'a>, (EdgeIdx, &'a str)>,
    pub sig_table: &'a HashMap<(EdgeIdx, &'a str), NodeIdx>,
}

impl<'a> HdlBuilder<'a> {
    pub fn build_from_cst(&mut self, cst: &'a Graph<Controller, Channel>) -> HdlNode<'_> {
        use std::fmt::Write;
        let mut inputs: Vec<HdlNode> = Vec::new();
        let mut outputs: Vec<HdlNode> = Vec::new();
        let mut channels: HashMap<EdgeIdx, HdlNode> = HashMap::new();
        let mut processes: Vec<HdlNode> = Vec::new();
        let mut instances: Vec<HdlNode> = Vec::new();
        let mut assigns: Vec<HdlNode> = Vec::new();

        cst.edges.iter().enumerate().for_each(|(idx, edge)| {
            let mut chan = HdlNode::Channel {
                cst_edge: (EdgeIdx(idx), edge),
                name: format!("edge_{}_{}", edge.src.0, edge.dst.0),
                high: 0,
                low: 0,
                slices: BTreeMap::new(),
            };

            if let HdlNode::Channel {
                cst_edge,
                name,
                ref mut high,
                ref mut low,
                ref mut slices,
            } = &mut chan
            {
                use itertools::Itertools;
                edge.data
                    .provides
                    .iter()
                    .sorted_by(|(lhs, _), (rhs, _)| Ord::cmp(lhs, rhs))
                    .for_each(|(name, sig)| {
                        let typ = self.cg[*self.sig_table.get(&(EdgeIdx(idx), name)).unwrap()]
                            .data
                            .type_
                            .as_ref()
                            .unwrap();

                        if let Type::Logic(slice_high, slice_low) = typ {
                            slices.insert(
                                *name,
                                (*slice_high as u32 + *high, *slice_low as u32 + *high),
                            );
                            *high += *slice_high as u32 + 1;
                        } else {
                            unreachable!("Compiler bug");
                        }
                    });

                // Account for Verilog vectors of slice [0:0] having size 1
                *high -= 1;
            } else {
                unreachable!("Compiler bug");
            }

            channels.insert(EdgeIdx(idx), chan);
        });

        for nidx in 0..cst.nodes.len() {
            let node = cst.node_ref(NodeIdx(nidx));

            use ControllerKind::*;
            match &node.data.kind {
                Fork {} | Join {} | Demux {} | Mux {} | Merge {} | Source { .. } | Sink { .. } => {
                    let name = format!("{}_{nidx}", node.data.kind.type_string());
                    let cst_node = (NodeIdx(nidx), &cst[NodeIdx(nidx)]);
                    let mut ports: HashMap<String, HdlNode<'a>> = HashMap::new();
                    let mut params: HashMap<String, String> = HashMap::new();
                    node.edges
                        .incs_iter_indexed()
                        .filter(|(edge, idx)| !matches!(edge.data.dst_port, cst::Port::Select()))
                        .enumerate()
                        .for_each(|(n, (edge, idx))| {
                            let chan = channels.get(&idx).unwrap().clone();
                            if let HdlNode::Channel {
                                cst_edge,
                                name,
                                high,
                                low,
                                slices,
                            } = &chan
                            {
                                ports.insert(format!("in{n}"), chan.clone());
                                if matches!(&node.data.kind, Join {}) {
                                    params.insert(
                                        format!("DATA{n}_WIDTH"),
                                        format!("{}", high - low + 1),
                                    );
                                } else {
                                    params.insert(
                                        "DATA_WIDTH".to_string(),
                                        format!("{}", high - low + 1),
                                    );
                                }
                            } else {
                                unreachable!("Compiler bug");
                            }
                        });

                    node.edges
                        .incs_iter_indexed()
                        .filter(|(edge, idx)| matches!(edge.data.dst_port, cst::Port::Select()))
                        .enumerate()
                        .for_each(|(n, (edge, idx))| {
                            let chan = channels.get(&idx).unwrap().clone();
                            if let HdlNode::Channel {
                                cst_edge,
                                name,
                                high,
                                low,
                                slices,
                            } = &chan
                            {
                                ports.insert(format!("sel{n}"), chan.clone());
                            } else {
                                unreachable!("Compiler bug");
                            }
                        });

                    node.edges
                        .outs_iter_indexed()
                        .enumerate()
                        .for_each(|(n, (edge, idx))| {
                            ports.insert(format!("out{n}"), channels.get(&idx).unwrap().clone());
                        });

                    instances.push(HdlNode::ControllerInstance {
                        cst_node,
                        name,
                        ports,
                        params,
                    });
                }
                Comb { .. } => {
                    let cst_node = (NodeIdx(nidx), &cst[NodeIdx(nidx)]);

                    let input = node
                        .edges
                        .incs_iter_indexed()
                        .take(1)
                        .map(|(edge, idx)| Box::new(channels.get(&idx).unwrap().clone()))
                        .next()
                        .unwrap();

                    let output = node
                        .edges
                        .outs_iter_indexed()
                        .take(1)
                        .map(|(edge, idx)| Box::new(channels.get(&idx).unwrap().clone()))
                        .next()
                        .unwrap();

                    let mut proc_build =
                        ProcessBuilder::from_comb(&node, self.comb_types, self.cg, self.sig_table);

                    let process_ast = proc_build.parse_comb_rec(node.data.ast_node);

                    let vars = proc_build.vars;

                    processes.push(HdlNode::CombProcess {
                        cst_node,
                        input,
                        output,
                        process_ast,
                        vars,
                    })
                }
                Input { name, .. } => {
                    let chan = node
                        .edges
                        .outs_iter_indexed()
                        .take(1)
                        .map(|(edge, idx)| channels.get(&idx).unwrap().clone())
                        .next()
                        .unwrap();

                    // Rename the channel to get the output channel and create an assignment
                    // from the internal cryptic name to the clear output name
                    let HdlNode::Channel { cst_edge, high, low, slices, ..} = chan.clone()
		    else { unreachable!("Compiler bug");};

                    let in_name = name.to_string();

                    let in_channel = HdlNode::Channel {
                        cst_edge,
                        name: in_name,
                        high,
                        low,
                        slices,
                    };

                    inputs.push(in_channel.clone());
                    assigns.push(HdlNode::Assign {
                        to: Box::new(chan.clone()),
                        from: Box::new(in_channel),
                    });
                }
                Output { name, .. } => {
                    let chan = node
                        .edges
                        .incs_iter_indexed()
                        .take(1)
                        .map(|(edge, idx)| channels.get(&idx).unwrap().clone())
                        .next()
                        .unwrap();

                    // Rename the channel to get the output channel and create an assignment
                    // from the internal cryptic name to the clear output name
                    let HdlNode::Channel { cst_edge, high, low, slices, ..} = chan.clone()
		    else { unreachable!("Compiler bug");};

                    let out_name = name.to_string();

                    let out_channel = HdlNode::Channel {
                        cst_edge,
                        name: out_name,
                        high,
                        low,
                        slices,
                    };

                    outputs.push(out_channel.clone());
                    assigns.push(HdlNode::Assign {
                        from: Box::new(chan.clone()),
                        to: Box::new(out_channel),
                    });
                }
                NamedChan { defs } => {
                    let from = node
                        .edges
                        .incs_iter_indexed()
                        .take(1)
                        .map(|(edge, idx)| Box::new(channels.get(&idx).unwrap().clone()))
                        .next()
                        .unwrap();
                    let to = node
                        .edges
                        .outs_iter_indexed()
                        .take(1)
                        .map(|(edge, idx)| Box::new(channels.get(&idx).unwrap().clone()))
                        .next()
                        .unwrap();

                    assigns.push(HdlNode::Assign { from, to });
                }
                Reg { defs } => {
                    let name = format!("{}_{nidx}", node.data.kind.type_string());
                    let cst_node = (NodeIdx(nidx), &cst[NodeIdx(nidx)]);
                    let mut ports: HashMap<String, HdlNode<'a>> = HashMap::new();
                    let mut params: HashMap<String, String> = HashMap::new();
                    let input = node
                        .edges
                        .incs_iter_indexed()
                        .take(1)
                        .map(|(edge, idx)| channels.get(&idx).unwrap().clone())
                        .next()
                        .unwrap();

                    let output = node
                        .edges
                        .outs_iter_indexed()
                        .take(1)
                        .map(|(edge, idx)| channels.get(&idx).unwrap().clone())
                        .next()
                        .unwrap();

                    let mut default_string = String::new();

                    if let HdlNode::Channel {
                        cst_edge,
                        name,
                        high,
                        low,
                        slices,
                    } = &output
                    {
                        let mut bit_string = "0"
                            .repeat((high - low + 1) as usize)
                            .to_string()
                            .as_bytes()
                            .to_vec();

                        let mut has_defaults = false;

                        slices.iter().for_each(|(name, dims)| {
			    if let Some(def) = defs.get(name) {
				has_defaults = true;
				let ast::Node::SigDeclExpr { ident, typespec, default, slice } = &def.point.inner
				else {unreachable!("compiler bug");};

				if let Some(default) = default.as_ref() {
				    if let ast::Node::NumLitExpr { val } = default.as_ref().inner {
					// TODO support longer literals
					let lit_string_arr = format!("{:032b}", val).as_bytes().to_vec();
					let mut lit_i = lit_string_arr.len() - 1;
					for i in (dims.1..=dims.0).rev() {
					    bit_string[i as usize] = lit_string_arr[lit_i];
					    lit_i -= 1;
					}
				    }
				}
			    }
                        });

                        if has_defaults {
                            let bit_string = String::from_utf8(bit_string.to_vec()).unwrap();

                            let len = high - low + 1;
                            let _ = write!(default_string, "{len}'b{bit_string}");
                            params.insert("INITIAL_VAL".to_string(), default_string);
                            params.insert("PO".to_string(), "1".to_string());
                        }
                        params.insert("DATA_WIDTH".to_string(), format!("{}", high - low + 1));
                    } else {
                        unreachable!("Compiler bug");
                    }

                    ports.insert("in".to_string(), input);
                    ports.insert("out".to_string(), output);

                    instances.push(HdlNode::ControllerInstance {
                        cst_node,
                        name,
                        ports,
                        params,
                    });
                }
                _ => todo!(),
            }
        }

        HdlNode::Module {
            inputs,
            outputs,
            channels: channels.iter().map(|(key, val)| val.clone()).collect(),
            processes,
            instances,
            assigns,
        }
    }
}

pub struct HdlWriter {
    pub source: String,
}

impl HdlWriter {
    pub fn build_from(&mut self, node: &HdlNode) {
        use std::fmt::Write;

        use HdlNode::*;
        match node {
            Module {
                inputs,
                outputs,
                channels,
                processes,
                instances,
                assigns,
            } => {
                let _ = write!(self.source, "module top (");
                inputs.iter().for_each(|input| {
                    let HdlNode::Channel { cst_edge, name, high, low, slices } = input
		    else {unreachable!("compiler bug");};

                    let _ = writeln!(
                        self.source,
                        "input req_{name}, input [{high}:{low}] data_{name}, output ack_{name},"
                    );
                });

                outputs.iter().for_each(|output| {
                    let HdlNode::Channel { cst_edge, name, high, low, slices } = output
		    else {unreachable!("compiler bug");};

                    let _ = writeln!(
                        self.source,
                        "output req_{name}, output [{high}:{low}] data_{name}, input ack_{name},"
                    );
                });

                let _ = writeln!(self.source, " input rst);");

                channels.iter().for_each(|chan| {
                    self.build_from(chan);
                });
                assigns.iter().for_each(|assign| {
                    self.build_from(assign);
                });
                processes.iter().for_each(|proc| {
                    self.build_from(proc);
                });
                instances.iter().for_each(|instance| {
                    self.build_from(instance);
                });

                let _ = writeln!(self.source, "endmodule");
            }
            Channel {
                cst_edge,
                name,
                high,
                low,
                slices,
            } => {
                let _ = writeln!(self.source, "wire req_{name};");
                let _ = writeln!(self.source, "wire [{high}:{low}] data_{name};");
                let _ = writeln!(self.source, "wire ack_{name};");
            }
            ControllerInstance {
                cst_node,
                name,
                ports,
                params,
            } => {
                use cst::ControllerKind::*;
                let kind = match cst_node.1.data.kind {
                    Merge {} => "Merge",
                    Fork {} => "Fork",
                    Join {} => "Join",
                    Mux { .. } => "Mux",
                    Demux { .. } => "Demux",
                    Reg { .. } => "Stage",
                    Sink { .. } => "Sink",
                    Source { .. } => "Source",
                    _ => {
                        unreachable!("Encountered unsupport controller. Compiler bug");
                    }
                };

                let mut port_conns = ports
                    .iter()
                    .fold(String::new(), |mut lhs, (port_name, chan)| match chan {
                        Channel {
                            cst_edge,
                            name,
                            high,
                            low,
                            slices,
                        } => {
                            let _ = writeln!(
                                lhs,
                                " .req_{port_name}(req_{name}), .ack_{port_name}(ack_{name}), .data_{port_name}(data_{name}),"
                            );

			    lhs
                        }
			_ => { unreachable!("Encountered unsupported port connection. Compiler bug") }
                    });

                let _ = writeln!(port_conns, " .rst(rst)");

                let mut place_comma = false;
                let params = params.iter().fold(String::new(), |mut lhs, (param, val)| {
                    let _ = write!(
                        lhs,
                        "{}.{param}({val})",
                        if place_comma { ", " } else { "" }
                    );
                    place_comma = true;
                    lhs
                });

                let _ = writeln!(self.source, "{kind} #({params}) {name} ({port_conns});");
            }
            CombProcess {
                cst_node,
                input,
                output,
                vars,
                process_ast,
            } => {
                // to make the reg type signal we need to assign to at the end of the process
                if let Channel {
                    cst_edge,
                    name,
                    high,
                    low,
                    slices,
                } = output.as_ref()
                {
                    let _ = writeln!(self.source, "reg [{high}:{low}] out_{};", cst_node.0 .0);
                }
                let _ = writeln!(self.source, "always @(*) begin: comb_{}", cst_node.0 .0);
                // declare all our vars
                vars.iter().for_each(|(name, typ)| {
                    if let Type::Logic(high, low) = typ {
                        let _ = writeln!(self.source, "reg [{high}:{low}] {name};");
                    } else {
                        unreachable!("Compiler error");
                    }
                });

                // destructure the input channel
                if let Channel {
                    cst_edge,
                    name,
                    high,
                    low,
                    slices,
                } = input.as_ref()
                {
                    slices.iter().for_each(|(sig_name, (high, low))| {
                        let _ = writeln!(self.source, "{sig_name} = data_{name}[{high}:{low}];");
                    });
                } else {
                    unreachable!("Compiler bug");
                };

                write_process_nodes(&mut self.source, process_ast);

                // destructure the output channel
                if let Channel {
                    cst_edge,
                    name,
                    high,
                    low,
                    slices,
                } = output.as_ref()
                {
                    slices.iter().for_each(|(sig_name, (high, low))| {
                        let _ = writeln!(
                            self.source,
                            "out_{}[{high}:{low}] = {sig_name};",
                            cst_node.0 .0
                        );
                    });
                } else {
                    unreachable!("Compiler bug");
                };

                let _ = writeln!(self.source, "end");

                // destructure the output channel
                if let Channel {
                    cst_edge,
                    name,
                    high,
                    low,
                    slices,
                } = output.as_ref()
                {
                    let _ = writeln!(self.source, "assign data_{name} = out_{};", cst_node.0 .0);
                } else {
                    unreachable!("Compiler bug");
                };

                let HdlNode::Channel { name: ref from, .. } = input.as_ref() else {unreachable!("Compiler bug");};
                let HdlNode::Channel { name: ref to, .. } = output.as_ref() else {unreachable!("Compiler bug");};
                let _ = writeln!(self.source, "assign req_{to} = req_{from};");
                let _ = writeln!(self.source, "assign ack_{from} = ack_{to};");
            }
            Assign { from, to } => {
                let HdlNode::Channel { name: ref from, .. } = from.as_ref() else {unreachable!("Compiler bug");};
                let HdlNode::Channel { name: ref to, .. } = to.as_ref() else {unreachable!("Compiler bug");};

                let _ = writeln!(self.source, "assign req_{to} = req_{from};");
                let _ = writeln!(self.source, "assign data_{to} = data_{from};");
                let _ = writeln!(self.source, "assign ack_{from} = ack_{to};");
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::sdc::GenerateSdc;
    use crate::token;
    use chumsky::extra;
    use chumsky::prelude::*;
    use extra::Err;
    use std::eprint;
    use std::matches;

    #[test]
    fn gen_gcd() {
        let source = r###"
        {
        chan loop_s;
        
        input chan a : {sig a : logic[7:0]};
        input chan b : {sig b : logic[7:0]};
        
        [[a, b] -> join(), chan loop_val] -> mux(loop_s) -> reg() -> fork() ->
        [
                comb {sig s = a != b;} -> fork() -> [reg(sig s : logic[0:0] = 0) -> loop_s, chan output_s],
                demux(output_s) -> [reg() -> output chan o : {sig a, sig b},
                                   reg() -> fork() -> [comb {sig s = a < b;} -> chan branch_s,
                                             demux(branch_s) -> [
                                                                comb {a[7:0] = a[7:0] - b[7:0];},
                                                                comb {b = b - a;}
                                                                ]
                                             -> merge() -> loop_val
                                             ]
                                   ]
        ];
        }
"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = cst::CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());

                builder.propagate2();
                assert!(cst::test::all_needs_met(&mut builder.g));

                let mut cg_builder = cg::CgBuilder::new();
                cg_builder.build_from_cst(&builder.g);

                let mut typer = cg::TypeChecker::new();
                typer.check_types(&mut cg_builder.cg);
                let mut hdl_builder = HdlBuilder {
                    comb_types: &cg_builder.comb_types,
                    cg: &cg_builder.cg,
                    sig_table: &cg_builder.sig_table,
                };

                let top_module = hdl_builder.build_from_cst(&builder.g);
                let mut writer = HdlWriter {
                    source: String::new(),
                };
                writer.build_from(&top_module);
                use std::fs::File;
                use std::io::Write;
                let mut f = File::create("top.v").unwrap();
                f.write_all(writer.source.as_bytes()).unwrap();
                print!("{}", writer.source);
                print!(
                    "{}",
                    crate::sdc::DecoupledClickBuilder::generate_sdc_from(&builder.g)
                );
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }
}
