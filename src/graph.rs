#[derive(Hash, Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct NodeIdx(pub usize);

#[derive(Hash, Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct EdgeIdx(pub usize);

#[derive(Default, Debug)]
pub struct GraphNode<T> {
    pub incs: Vec<EdgeIdx>,
    pub outs: Vec<EdgeIdx>,
    pub data: T,
}

impl<T> GraphNode<T> {
    pub fn new(data: T) -> Self {
        Self {
            incs: Vec::new(),
            outs: Vec::new(),
            data,
        }
    }
}

#[derive(Default, Debug)]
pub struct GraphEdge<T> {
    pub src: NodeIdx,
    pub dst: NodeIdx,
    pub data: T,
}

#[derive(Debug)]
pub struct Graph<N, E> {
    pub nodes: Vec<GraphNode<N>>,
    pub edges: Vec<GraphEdge<E>>,
    edge_marks: Vec<bool>,
}

pub struct EdgesView<'a, E> {
    edges: &'a mut [GraphEdge<E>],
    incs: &'a mut Vec<EdgeIdx>,
    outs: &'a mut Vec<EdgeIdx>,
    edge_marks: &'a mut [bool],
}

pub struct NodeView<'a, E, N> {
    pub data: &'a mut N,
    pub edges: EdgesView<'a, E>,
}

pub struct EdgesViewRef<'a, E> {
    edges: &'a [GraphEdge<E>],
    incs: &'a Vec<EdgeIdx>,
    outs: &'a Vec<EdgeIdx>,
}

pub struct NodeViewRef<'a, E, N> {
    pub data: &'a N,
    pub edges: EdgesViewRef<'a, E>,
}

impl<'a, E> EdgesViewRef<'a, E> {
    pub fn incs_iter(&self) -> EdgeIter<'_, E, impl Iterator<Item = &EdgeIdx>> {
        EdgeIter {
            edges: self.edges,
            it: self.incs.iter(),
        }
    }

    pub fn outs_iter(&self) -> EdgeIter<'_, E, impl Iterator<Item = &EdgeIdx>> {
        EdgeIter {
            edges: self.edges,
            it: self.outs.iter(),
        }
    }

    pub fn incs_iter_indexed(&self) -> EdgeIterIndexed<'_, E, impl Iterator<Item = &EdgeIdx>> {
        EdgeIterIndexed {
            edges: self.edges,
            it: self.incs.iter(),
        }
    }

    pub fn outs_iter_indexed(&self) -> EdgeIterIndexed<'_, E, impl Iterator<Item = &EdgeIdx>> {
        EdgeIterIndexed {
            edges: self.edges,
            it: self.outs.iter(),
        }
    }
}

impl<'a, E> EdgesView<'a, E> {
    pub fn incs_iter_mut(&mut self) -> EdgeIterMut<'_, E> {
        EdgeIterMut {
            edges: self.edges,
            marks: self.edge_marks,
            idxs: self.incs.as_slice(),
            cur: 0,
        }
    }

    pub fn outs_iter_mut(&mut self) -> EdgeIterMut<'_, E> {
        EdgeIterMut {
            edges: self.edges,
            marks: self.edge_marks,
            idxs: self.outs.as_slice(),
            cur: 0,
        }
    }

    pub fn incs_iter(&self) -> EdgeIter<'_, E, impl Iterator<Item = &EdgeIdx>> {
        EdgeIter {
            edges: self.edges,
            it: self.incs.iter(),
        }
    }

    pub fn outs_iter(&self) -> EdgeIter<'_, E, impl Iterator<Item = &EdgeIdx>> {
        EdgeIter {
            edges: self.edges,
            it: self.outs.iter(),
        }
    }

    pub fn incs_iter_mut_indexed(&mut self) -> EdgeIterMutIndexed<'_, E> {
        EdgeIterMutIndexed {
            edges: self.edges,
            marks: self.edge_marks,
            idxs: self.incs.as_slice(),
            cur: 0,
        }
    }

    pub fn outs_iter_mut_indexed(&mut self) -> EdgeIterMutIndexed<'_, E> {
        EdgeIterMutIndexed {
            edges: self.edges,
            marks: self.edge_marks,
            idxs: self.outs.as_slice(),
            cur: 0,
        }
    }

    pub fn incs_iter_indexed(&self) -> EdgeIterIndexed<'_, E, impl Iterator<Item = &EdgeIdx>> {
        EdgeIterIndexed {
            edges: self.edges,
            it: self.incs.iter(),
        }
    }

    pub fn outs_iter_indexed(&self) -> EdgeIterIndexed<'_, E, impl Iterator<Item = &EdgeIdx>> {
        EdgeIterIndexed {
            edges: self.edges,
            it: self.outs.iter(),
        }
    }
}

pub struct EdgeIter<'a, E, T: Iterator<Item = &'a EdgeIdx>> {
    edges: &'a [GraphEdge<E>],
    it: T,
}

impl<'a, E, T: Iterator<Item = &'a EdgeIdx>> Iterator for EdgeIter<'a, E, T> {
    type Item = &'a GraphEdge<E>;

    fn next(&mut self) -> Option<Self::Item> {
        self.it.next().map(|e| &self.edges[e.0])
    }
}

//TODO make a `with_index` trait implementation for EdgeIter instead...
pub struct EdgeIterIndexed<'a, E, T: Iterator<Item = &'a EdgeIdx>> {
    edges: &'a [GraphEdge<E>],
    it: T,
}

impl<'a, E, T: Iterator<Item = &'a EdgeIdx>> Iterator for EdgeIterIndexed<'a, E, T> {
    type Item = (&'a GraphEdge<E>, EdgeIdx);

    fn next(&mut self) -> Option<Self::Item> {
        self.it.next().map(|e| (&self.edges[e.0], *e))
    }
}

pub struct EdgeIterMut<'a, E> {
    edges: &'a mut [GraphEdge<E>],
    marks: &'a mut [bool],
    idxs: &'a [EdgeIdx],
    cur: usize,
}

impl<'a, E> Iterator for EdgeIterMut<'a, E> {
    type Item = &'a mut GraphEdge<E>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cur == self.idxs.len() {
            return None;
        }

        let cur_idx = self.idxs[self.cur].0;
        // marks is used to check for disjointness in linear time.
        // If the next element is already marked, then returning it would
        // possibly alias
        if self.marks[cur_idx] {
            panic!("Edge indexes not disjoint!");
        }

        // We mark the edge about to be returned so it can never be returned again
        // Marks will be reset in self.drop()
        self.marks[cur_idx] = true;

        self.cur += 1;

        // This is safe because we checked that we never returned this index
        // before
        Some(unsafe { &mut *(&mut self.edges[cur_idx] as *mut _) })
    }
}

impl<'a, E> Drop for EdgeIterMut<'a, E> {
    fn drop(&mut self) {
        for idx in self.idxs {
            self.marks[idx.0] = false;
        }
    }
}

pub struct EdgeIterMutIndexed<'a, E> {
    edges: &'a mut [GraphEdge<E>],
    marks: &'a mut [bool],
    idxs: &'a [EdgeIdx],
    cur: usize,
}

impl<'a, E> Iterator for EdgeIterMutIndexed<'a, E> {
    type Item = (&'a mut GraphEdge<E>, EdgeIdx);

    fn next(&mut self) -> Option<Self::Item> {
        if self.cur == self.idxs.len() {
            return None;
        }

        let edge_idx = self.idxs[self.cur];
        let cur_idx = self.idxs[self.cur].0;
        // marks is used to check for disjointness in linear time.
        // If the next element is already marked, then returning it would
        // possibly alias
        if self.marks[cur_idx] {
            panic!("Edge indexes not disjoint!");
        }

        // We mark the edge about to be returned so it can never be returned again
        // Marks will be reset in self.drop()
        self.marks[cur_idx] = true;

        self.cur += 1;

        // This is safe because we checked that we never returned this index
        // before
        Some((
            unsafe { &mut *(&mut self.edges[cur_idx] as *mut _) },
            edge_idx,
        ))
    }
}

impl<'a, E> Drop for EdgeIterMutIndexed<'a, E> {
    fn drop(&mut self) {
        for idx in self.idxs {
            self.marks[idx.0] = false;
        }
    }
}

impl<N, E> Graph<N, E> {
    pub fn new() -> Self {
        Self {
            nodes: Vec::new(),
            edges: Vec::new(),
            edge_marks: Vec::new(),
        }
    }
    pub fn add_node(&mut self, data: N) -> NodeIdx {
        self.nodes.push(GraphNode::new(data));

        NodeIdx(self.nodes.len() - 1)
    }

    pub fn add_edge(&mut self, src: NodeIdx, dst: NodeIdx, data: E) -> EdgeIdx {
        self.edges.push(GraphEdge { src, dst, data });
        self.edge_marks.push(false);

        let idx = EdgeIdx(self.edges.len() - 1);

        self[src].outs.push(idx);
        self[dst].incs.push(idx);

        idx
    }

    pub fn inc_edges_iter_mut(&mut self, node: NodeIdx) -> EdgeIterMut<'_, E> {
        EdgeIterMut {
            edges: &mut self.edges,
            marks: &mut self.edge_marks,
            idxs: self.nodes[node.0].incs.as_slice(),
            cur: 0,
        }
    }

    pub fn out_edges_iter_mut(&mut self, node: NodeIdx) -> EdgeIterMut<'_, E> {
        EdgeIterMut {
            edges: &mut self.edges,
            marks: &mut self.edge_marks,
            idxs: self.nodes[node.0].outs.as_slice(),
            cur: 0,
        }
    }

    pub fn inc_edges_iter(&self, node: NodeIdx) -> EdgeIter<'_, E, impl Iterator<Item = &EdgeIdx>> {
        EdgeIter {
            edges: &self.edges,
            it: self[node].incs.iter(),
        }
    }

    pub fn out_edges_iter(&self, node: NodeIdx) -> EdgeIter<'_, E, impl Iterator<Item = &EdgeIdx>> {
        EdgeIter {
            edges: &self.edges,
            it: self[node].outs.iter(),
        }
    }

    pub fn get_node_with_edges_mut(
        &mut self,
        node_idx: NodeIdx,
    ) -> (
        Vec<&mut GraphEdge<E>>,
        &mut GraphNode<N>,
        Vec<&mut GraphEdge<E>>,
    ) {
        let node = &mut self.nodes[node_idx.0];
        let idxs = node.incs.iter().chain(node.outs.iter()).copied().collect();
        let (muts_inc, muts_out) = split_multiple(&mut self.edges, idxs, node_idx);

        (muts_inc, node, muts_out)
    }

    pub fn foreach_node_with_edges(
        &mut self,
        f: impl Fn(Vec<&mut GraphEdge<E>>, &mut GraphNode<N>, Vec<&mut GraphEdge<E>>),
    ) {
        for i in 0..self.nodes.len() {
            let (muts_inc, node, muts_out) = self.get_node_with_edges_mut(NodeIdx(i));
            f(muts_inc, node, muts_out);
        }
    }

    pub fn node(&mut self, idx: NodeIdx) -> NodeView<'_, E, N> {
        let node = &mut self.nodes[idx.0];
        NodeView {
            edges: EdgesView {
                edges: &mut self.edges,
                incs: &mut node.incs,
                outs: &mut node.outs,
                edge_marks: &mut self.edge_marks,
            },
            data: &mut node.data,
        }
    }

    pub fn node_ref(&self, idx: NodeIdx) -> NodeViewRef<'_, E, N> {
        let node = &self.nodes[idx.0];
        NodeViewRef {
            edges: EdgesViewRef {
                edges: &self.edges,
                incs: &node.incs,
                outs: &node.outs,
            },
            data: &node.data,
        }
    }
}

impl<N, E> std::ops::Index<NodeIdx> for Graph<N, E> {
    type Output = GraphNode<N>;

    fn index(&self, index: NodeIdx) -> &Self::Output {
        &self.nodes[index.0]
    }
}

impl<N, E> std::ops::IndexMut<NodeIdx> for Graph<N, E> {
    fn index_mut(&mut self, index: NodeIdx) -> &mut Self::Output {
        &mut self.nodes[index.0]
    }
}

impl<N, E> std::ops::Index<EdgeIdx> for Graph<N, E> {
    type Output = GraphEdge<E>;

    fn index(&self, index: EdgeIdx) -> &Self::Output {
        &self.edges[index.0]
    }
}

impl<N, E> std::ops::IndexMut<EdgeIdx> for Graph<N, E> {
    fn index_mut(&mut self, index: EdgeIdx) -> &mut Self::Output {
        &mut self.edges[index.0]
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::assert_eq;

    fn setup_container() -> Vec<GraphEdge<i32>> {
        vec![
            GraphEdge {
                src: NodeIdx(0),
                dst: NodeIdx(1),
                data: 0,
            },
            GraphEdge {
                src: NodeIdx(0),
                dst: NodeIdx(2),
                data: 1,
            },
            GraphEdge {
                src: NodeIdx(1),
                dst: NodeIdx(2),
                data: 2,
            },
        ]
    }

    #[test]
    fn split_multiple_single_inc() {
        let mut edges = setup_container();

        let muts = split_multiple(&mut edges, vec![EdgeIdx(0)], NodeIdx(1));
        assert_eq!(muts.0.len(), 1);
        assert_eq!(muts.0[0].data, 0);
    }

    #[test]
    fn split_multiple_single_out() {
        let mut edges = setup_container();

        let muts = split_multiple(&mut edges, vec![EdgeIdx(2)], NodeIdx(1));
        assert_eq!(muts.1.len(), 1);
        assert_eq!(muts.1[0].data, 2);
    }

    #[test]
    fn split_multiple_double() {
        let mut edges = setup_container();

        let muts = split_multiple(&mut edges, vec![EdgeIdx(0), EdgeIdx(2)], NodeIdx(1));
        assert_eq!(muts.0.len(), 1);
        assert_eq!(muts.1.len(), 1);
        assert_eq!(muts.0[0].data, 0);
        assert_eq!(muts.1[0].data, 2);
    }

    #[test]
    fn split_multiple_triple() {
        let mut edges = setup_container();

        let muts = split_multiple(
            &mut edges,
            vec![EdgeIdx(0), EdgeIdx(2), EdgeIdx(1)],
            NodeIdx(1),
        );
        assert_eq!(muts.0.len(), 1);
        assert_eq!(muts.1.len(), 2);
        assert_eq!(muts.0[0].data, 0);
        assert_eq!(muts.1[0].data, 1);
        assert_eq!(muts.1[1].data, 2);
    }
}

pub fn split_multiple<E>(
    mut edges: &mut [GraphEdge<E>],
    mut idxs: Vec<EdgeIdx>,
    n_id: NodeIdx,
) -> (Vec<&mut GraphEdge<E>>, Vec<&mut GraphEdge<E>>) {
    idxs.sort();
    idxs.dedup();

    let mut muts_inc = Vec::new();
    let mut muts_out = Vec::new();

    let mut idx = 0;

    for i in idxs.iter() {
        let new_idx = i.0 - idx;
        let (l, r) = edges
            .split_at_mut(new_idx)
            .1
            .split_first_mut()
            .expect("Out of bounds access to edges list");
        idx += new_idx + 1;
        if l.dst == n_id {
            muts_inc.push(l);
        } else {
            muts_out.push(l);
        }
        edges = r;
    }

    (muts_inc, muts_out)
}
