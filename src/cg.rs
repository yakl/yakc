use std::collections::HashMap;
use std::collections::VecDeque;

use crate::cst::ControllerKind;
use crate::graph::Graph;
use crate::token;
use by_address::ByAddress;

use super::cst;

use super::ast;
use super::ast::Node;
use super::graph;
use super::graph::{EdgeIdx, EdgesView, GraphEdge, GraphNode, NodeIdx, NodeView};
use super::token::Op;
use super::util::ScopedSymTable;
use ariadne::ReportBuilder;
use ariadne::{Color, ColorGenerator, Fmt, Label, Report, ReportKind, Source};
use chumsky::container::Container;
use itertools::izip;

type SpannedNode = super::ast::Spanned<ast::Node>;

#[derive(Debug)]
pub enum ComputeOp {
    Merge,
    SelectOn(NodeIdx),
    BinOp(token::Op),
    Def,
    UnDef,
    Slice(i32, i32),
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Type {
    Logic(i32, i32),
    NumLit(i32), // todo support bigger numbers
}

#[derive(Debug)]
pub struct ComputeNode<'a> {
    pub ast_node: &'a SpannedNode,
    pub type_: Option<Type>,
    pub op: ComputeOp,
}

impl<'a> ComputeNode<'a> {
    fn new_without_type(ast_node: &'a SpannedNode, op: ComputeOp) -> Self {
        Self {
            ast_node,
            type_: None,
            op,
        }
    }
}

pub struct CgBuilder<'a> {
    pub cg: Graph<ComputeNode<'a>, (EdgeIdx, &'a str)>,
    pub controller_queue: VecDeque<NodeIdx>,
    pub sig_table: HashMap<(EdgeIdx, &'a str), NodeIdx>,
    pub revisit_list: Vec<NodeIdx>,
    pub comb_types: HashMap<ByAddress<&'a SpannedNode>, NodeIdx>,
}

impl<'a> CgBuilder<'a> {
    pub fn new() -> Self {
        Self {
            cg: Graph::new(),
            controller_queue: VecDeque::new(),
            sig_table: HashMap::new(),
            revisit_list: Vec::new(),
            comb_types: HashMap::new(),
        }
    }

    fn revisit(&mut self, cst: &'a Graph<cst::Controller, cst::Channel>) {
        self.revisit_list.iter().for_each(|nidx| {
            let node = cst.node_ref(*nidx);
            use cst::ControllerKind::*;

            match &node.data.kind {
                Mux {} => {
                    // Grab the select signal
                    node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
                        if let cst::Port::Select() = edge.data.dst_port {
                            if let Some(pred_idx) = self.sig_table.get(&(edge_idx, "s")) {
                                let t = ComputeNode::new_without_type(
                                    node.data.ast_node,
                                    ComputeOp::BinOp(Op::Eq),
                                );
                                let t_idx = self.cg.add_node(t);

                                let mut s_def = ComputeNode::new_without_type(
                                    node.data.ast_node,
                                    ComputeOp::Def,
                                );

                                s_def.type_ = Some(Type::Logic(0, 0));

                                let s_def_idx = self.cg.add_node(s_def);

                                self.cg.add_edge(s_def_idx, t_idx, (edge_idx, "s"));
                                self.cg.add_edge(*pred_idx, t_idx, (edge_idx, "s"));
                            } else {
                                todo!("Nice error on missing s signal");
                            }
                        }
                    });

                    node.edges
                        .outs_iter_indexed()
                        .for_each(|(out_edge, out_idx)| {
                            out_edge.data.provides.iter().for_each(|(name, sig)| {
                                // All type nodes should be defined at this point otherwise
                                // something went horribly wrong and we should crash.
                                let post_idx = self.sig_table.get(&(out_idx, *name)).unwrap();
                                let inc_idxs: Vec<_> = self.cg.nodes[post_idx.0]
                                    .incs
                                    .iter()
                                    .map(|edge_idx| self.cg.edges[edge_idx.0].src)
                                    .collect();

                                node.edges
                                    .incs_iter_indexed()
                                    .filter(|(in_edge, in_idx)| {
                                        !matches!(in_edge.data.dst_port, cst::Port::Select())
                                    })
                                    .for_each(|(in_edge, in_idx)| {
                                        let Some(pred_idx) =
                                            self.sig_table.get(&(in_idx, *name))
					else { dbg!(*name); dbg!(&in_edge); panic!(); };

                                        if !inc_idxs.contains(pred_idx) {
                                            self.cg.add_edge(*pred_idx, *post_idx, (in_idx, *name));
                                        }
                                    });
                            });
                        });
                }
                Merge {} => {
                    node.edges
                        .outs_iter_indexed()
                        .for_each(|(out_edge, out_idx)| {
                            out_edge.data.provides.iter().for_each(|(name, sig)| {
                                // All type nodes should be defined at this point otherwise
                                // something went horribly wrong and we should crash.
                                let post_idx = self.sig_table.get(&(out_idx, *name)).unwrap();
                                let inc_idxs: Vec<_> = self.cg.nodes[post_idx.0]
                                    .incs
                                    .iter()
                                    .map(|edge_idx| self.cg.edges[edge_idx.0].src)
                                    .collect();

                                node.edges
                                    .incs_iter_indexed()
                                    .for_each(|(in_edge, in_idx)| {
                                        let pred_idx =
                                            self.sig_table.get(&(in_idx, *name)).unwrap();

                                        if !inc_idxs.contains(pred_idx) {
                                            self.cg.add_edge(*pred_idx, *post_idx, (in_idx, *name));
                                        }
                                    });
                            });
                        });
                }
                Demux {} => {
                    node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
                        if let cst::Port::Select() = edge.data.dst_port {
                            if let Some(pred_idx) = self.sig_table.get(&(edge_idx, "s")) {
                                let t = ComputeNode::new_without_type(
                                    node.data.ast_node,
                                    ComputeOp::BinOp(Op::Eq),
                                );
                                let t_idx = self.cg.add_node(t);

                                let mut s_def = ComputeNode::new_without_type(
                                    node.data.ast_node,
                                    ComputeOp::Def,
                                );

                                s_def.type_ = Some(Type::Logic(0, 0));

                                let s_def_idx = self.cg.add_node(s_def);

                                self.cg.add_edge(s_def_idx, t_idx, (edge_idx, "s"));
                                self.cg.add_edge(*pred_idx, t_idx, (edge_idx, "s"));
                            } else {
                                todo!("Nice error on missing s signal");
                            }
                        }
                    });
                }
                _ => todo!(),
            }
        });
    }

    pub fn build_from_cst(&mut self, cst: &'a Graph<cst::Controller, cst::Channel>) {
        cst.nodes.iter().enumerate().for_each(|(id, node)| {
            if node.incs.is_empty() {
                self.controller_queue.push_back(NodeIdx(id));
            }
        });

        let mut visited = vec![false; cst.nodes.len()];

        while let Some(nidx) = self.controller_queue.pop_front() {
            if visited[nidx.0] {
                continue;
            }

            // This may be set to false again for join components with incomplete
            // inputs
            visited[nidx.0] = true;

            let node = cst.node_ref(nidx);
            use cst::ControllerKind::*;

            match &node.data.kind {
                Mux {} => {
                    let mut must_revisit = false;

                    node.edges.outs_iter_indexed().for_each(|(edge, out_idx)| {
                        edge.data.provides.iter().for_each(|(name, sig)| {
                            let t =
                                ComputeNode::new_without_type(node.data.ast_node, ComputeOp::Merge);

                            let t_idx = self.cg.add_node(t);

                            node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
                                if let cst::Port::Flow(..) = edge.data.dst_port {
                                    if let Some(pred_idx) = self.sig_table.get(&(edge_idx, *name)) {
                                        self.cg.add_edge(*pred_idx, t_idx, (edge_idx, *name));
                                    } else {
                                        must_revisit = true;
                                    }
                                }
                            });

                            self.sig_table.insert((out_idx, *name), t_idx);
                        });

                        node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
                            if let cst::Port::Select() = edge.data.dst_port {
                                if let Some(pred_idx) = self.sig_table.get(&(edge_idx, "s")) {
                                    let t = ComputeNode::new_without_type(
                                        node.data.ast_node,
                                        ComputeOp::BinOp(Op::Eq),
                                    );
                                    let t_idx = self.cg.add_node(t);

                                    let mut s_def = ComputeNode::new_without_type(
                                        node.data.ast_node,
                                        ComputeOp::Def,
                                    );

                                    s_def.type_ = Some(Type::Logic(0, 0));

                                    let s_def_idx = self.cg.add_node(s_def);

                                    self.cg.add_edge(s_def_idx, t_idx, (edge_idx, "s"));
                                    self.cg.add_edge(*pred_idx, t_idx, (edge_idx, "s"));
                                } else {
                                    must_revisit = true;
                                }
                            }
                        });

                        self.controller_queue.push_back(edge.dst);
                    });

                    if must_revisit {
                        self.revisit_list.push(nidx)
                    }

                    // special handling of select channel
                }
                Merge {} => {
                    let mut must_revisit = false;
                    node.edges.outs_iter_indexed().for_each(|(edge, out_idx)| {
                        edge.data.provides.iter().for_each(|(name, sig)| {
                            let t =
                                ComputeNode::new_without_type(node.data.ast_node, ComputeOp::Merge);

                            let t_idx = self.cg.add_node(t);

                            node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
                                if let Some(pred_idx) = self.sig_table.get(&(edge_idx, *name)) {
                                    self.cg.add_edge(*pred_idx, t_idx, (edge_idx, *name));
                                    self.sig_table.insert((out_idx, *name), t_idx);
                                } else {
                                    must_revisit = true;
                                }
                            });
                        });

                        self.controller_queue.push_back(edge.dst);
                    });

                    if must_revisit {
                        self.revisit_list.push(nidx)
                    }
                }
                NamedChan { .. } | Fork {} => {
                    node.edges.outs_iter_indexed().for_each(|(edge, out_idx)| {
                        edge.data.provides.iter().for_each(|(name, sig)| {
                            node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
                                if let Some(pred_idx) = self.sig_table.get(&(edge_idx, *name)) {
				    self.sig_table.insert((out_idx, *name), *pred_idx);
                                } else {
				    todo!("Report error when Fork/NamedChan doesn't have needed input signal in type graph gen");
                                }
                            });
                        });

                        self.controller_queue.push_back(edge.dst);
                    });
                }
                Demux {} => {
                    node.edges.outs_iter_indexed().for_each(|(edge, out_idx)| {
                        edge.data.provides.iter().for_each(|(name, sig)| {
                            node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
				if let cst::Port::Select() =  edge.data.dst_port {
				    return;
				}
                                if let Some(pred_idx) = self.sig_table.get(&(edge_idx, *name)) {
				    self.sig_table.insert((out_idx, *name), *pred_idx);
                                } else {
				    todo!("Report error when Demux doesn't have needed input signal in type graph gen");
                                }
                            });
                        });

                        self.controller_queue.push_back(edge.dst);
                    });

                    let mut must_revisit = false;
                    node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
                        if let cst::Port::Select() = edge.data.dst_port {
                            if let Some(pred_idx) = self.sig_table.get(&(edge_idx, "s")) {
                                let t = ComputeNode::new_without_type(
                                    node.data.ast_node,
                                    ComputeOp::BinOp(Op::Eq),
                                );
                                let t_idx = self.cg.add_node(t);

                                let mut s_def = ComputeNode::new_without_type(
                                    node.data.ast_node,
                                    ComputeOp::Def,
                                );

                                s_def.type_ = Some(Type::Logic(0, 0));

                                let s_def_idx = self.cg.add_node(s_def);

                                self.cg.add_edge(s_def_idx, t_idx, (edge_idx, "s"));
                                self.cg.add_edge(*pred_idx, t_idx, (edge_idx, "s"));
                            } else {
                                must_revisit = true;
                            }
                        }
                    });

                    if must_revisit {
                        self.revisit_list.push(nidx)
                    }
                }
                Join {} => {
                    // Every output signal must exist on exactly one of the inputs.  This is
                    // verified in the scope propagation stage so we don't check it here.  However,
                    // Join components must have all their inputs defined, and input edges to a join
                    // cannot be target edges for breaking cycles. Therefore we know that if we do
                    // not find a signal on the inputs, then we are sure to be coming back to this
                    // join.
                    node.edges.outs_iter_indexed().for_each(|(edge, out_idx)| {
                        edge.data.provides.iter().for_each(|(name, sig)| {
                            node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
                                if let Some(pred_idx) = self.sig_table.get(&(edge_idx, *name)) {
                                    self.sig_table.insert((out_idx, *name), *pred_idx);
                                } else {
                                    visited[nidx.0] = false;
                                }
                            });
                        });

                        self.controller_queue.push_back(edge.dst);
                    });
                }
                Output { uses, .. } | Sink { uses } => {
                    uses.iter().for_each(|(name, sig)| {
                        let sig_decl_idx = self.build_comb(sig.point).unwrap();

                        node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
                            if let Some(pred_idx) = self.sig_table.get(&(edge_idx, *name)) {
                                let t = ComputeNode::new_without_type(
                                    node.data.ast_node,
                                    ComputeOp::Merge,
                                );

                                let t_idx = self.cg.add_node(t);

                                self.cg.add_edge(*pred_idx, t_idx, (edge_idx, *name));
                                self.cg.add_edge(sig_decl_idx, t_idx, (edge_idx, *name));
                            } else {
                                dbg!(&self.sig_table);
                                dbg!(&self.cg);
                                unreachable!();
                            }
                        });
                    });
                }
                Input { defs, .. } | Source { defs } => {
                    // TODO invert this iteration order
                    defs.iter().for_each(|(name, sig)| {
                        let sig_decl_idx = self.build_comb(sig.point).unwrap();

                        node.edges.outs_iter_indexed().for_each(|(edge, edge_idx)| {
                            if let Some(need) = edge.data.provides.get(*name) {
                                self.sig_table.insert((edge_idx, *name), sig_decl_idx);
                            } else {
                                // Defined signal ended up not being used
                            }

                            // TODO does this need to be here?
                            self.controller_queue.push_back(edge.dst);
                        });
                    });
                }
                ControllerKind::Reg { defs } => {
                    node.edges.outs_iter_indexed().for_each(|(edge, out_idx)| {
                        edge.data.provides.iter().for_each(|(name, sig)| {
                            node.edges.incs_iter_indexed().for_each(|(edge, edge_idx)| {
				let def_decl_idx = defs.get(*name).map(|def| self.build_comb(def.point).unwrap());
                                if let Some(pred_idx) = self.sig_table.get(&(edge_idx, *name)) {
				    if let Some(def_decl_idx) = def_decl_idx {

					let t = ComputeNode::new_without_type(
					    node.data.ast_node,
					    ComputeOp::Merge,
					);

					let t_idx = self.cg.add_node(t);

					self.cg.add_edge(*pred_idx, t_idx, (edge_idx, *name));
					self.cg.add_edge(def_decl_idx, t_idx, (edge_idx, *name));
					self.sig_table.insert((out_idx, *name), t_idx);
				    } else {
					self.sig_table.insert((out_idx, *name), *pred_idx);
				    }
                                } else {
				    todo!("Report error when Fork/NamedChan doesn't have needed input signal in type graph gen");
                                }
                            });
                        });

                        self.controller_queue.push_back(edge.dst);
                    });
                }
                ControllerKind::Comb { defs, uses } => {
                    let mut sym_table: ScopedSymTable<NodeIdx> = ScopedSymTable::new();
                    sym_table.enter_scope();
                    node.edges
                        .incs_iter_indexed()
                        .take(1)
                        .for_each(|(edge, edge_idx)| {
                            edge.data.provides.iter().for_each(|(name, sig)| {
                                sym_table.insert_sym(
                                    name.to_string(),
                                    *self.sig_table.get(&(edge_idx, *name)).unwrap(),
                                );
                            });
                        });

                    // We descend into a potential block statement because signals
                    // declared inside the top level block of the comb node must be
                    // inject into the symbol table we made above. If we didn't remove
                    // the initial block stmt then a new scope would be made which
                    // would be dropped at the end of the block statement leaving our
                    // outer symbol table empty.
                    match &node.data.ast_node.inner {
                        ast::Node::CompoundStmt { stmts } => {
                            stmts.iter().for_each(|stmt| {
                                self.build_comb_rec(stmt, &mut sym_table);
                            });
                        }
                        _ => {
                            self.build_comb_rec(node.data.ast_node, &mut sym_table);
                        }
                    }

                    node.edges.outs_iter_indexed().for_each(|(edge, edge_idx)| {
                        edge.data.provides.iter().for_each(|(name, sig)| {
                            self.sig_table.insert(
                                (edge_idx, name),
                                *sym_table.get(&name.to_string()).unwrap(),
                            );
                        });

                        self.controller_queue.push_back(edge.dst);
                    });
                }
                _ => todo!(),
            }
        }

        self.revisit(cst);
    }

    fn build_comb(&mut self, ast_node: &'a SpannedNode) -> Option<NodeIdx> {
        let mut sym_table: ScopedSymTable<NodeIdx> = ScopedSymTable::new();
        sym_table.enter_scope();
        self.build_comb_rec(ast_node, &mut sym_table)
    }

    fn build_comb_rec(
        &mut self,
        ast_node: &'a SpannedNode,
        sym_table: &mut ScopedSymTable<NodeIdx>,
    ) -> Option<NodeIdx> {
        use ast::Node::*;

        match &ast_node.inner {
            Stmt {} => {
                panic!("statement doesn't exist any more")
            }
            CompoundStmt { stmts } => {
                sym_table.enter_scope();
                stmts.iter().for_each(|ast_node| {
                    self.build_comb_rec(ast_node, sym_table);
                });
                sym_table.leave_scope();

                None
            }
            NumLitExpr { val } => {
                let t = ComputeNode {
                    ast_node,
                    op: ComputeOp::Def,
                    type_: Some(Type::NumLit(*val)),
                };

                let idx = self.cg.add_node(t);
                Some(idx)
            }
            BinOpExpr { op, lhs, rhs } => {
                let lhs_idx = self.build_comb_rec(lhs, sym_table).unwrap();
                let rhs_idx = self.build_comb_rec(rhs, sym_table).unwrap();
                let t = ComputeNode {
                    ast_node,
                    op: ComputeOp::BinOp(*op),
                    type_: None,
                };

                let t_idx = self.cg.add_node(t);
                self.cg.add_edge(lhs_idx, t_idx, (EdgeIdx(0), ""));
                self.cg.add_edge(rhs_idx, t_idx, (EdgeIdx(0), ""));

                if let Op::Assign = op {
                    // TODO possible bug here with default signal assignment handling
                    // as part of a bigger binary assignment
                    match &lhs.inner {
                        VarExpr { name } => {
                            sym_table.insert_sym(name.to_string(), t_idx);
                            self.comb_types.insert(ByAddress(ast_node), t_idx);
                        }
                        SigDeclExpr {
                            ident,
                            typespec,
                            default,
                            slice,
                        } => {
                            sym_table.insert_sym(ident.to_string(), t_idx);
                            self.comb_types.insert(ByAddress(ast_node), t_idx);
                        }
                        SliceExpr { expr, slice } => match &expr.inner {
                            VarExpr { name } => {
                                let slice_to_var = ComputeNode {
                                    ast_node,
                                    op: ComputeOp::BinOp(Op::Assign),
                                    type_: None,
                                };

                                // Order matters here. The assignee must be the first edge
                                let slice_to_var_idx = self.cg.add_node(slice_to_var);
                                let var_idx = sym_table.get(name).unwrap();
                                self.cg
                                    .add_edge(*var_idx, slice_to_var_idx, (EdgeIdx(0), name));
                                self.cg
                                    .add_edge(t_idx, slice_to_var_idx, (EdgeIdx(0), name));
                                sym_table.insert_sym(name.to_string(), slice_to_var_idx);
                                self.comb_types
                                    .insert(ByAddress(ast_node), slice_to_var_idx);
                            }
                            SigDeclExpr {
                                ident,
                                typespec,
                                default,
                                slice,
                            } => {
                                let slice_to_var = ComputeNode {
                                    ast_node,
                                    op: ComputeOp::BinOp(Op::Assign),
                                    type_: None,
                                };

                                // Order matters here. The assignee must be the first edge
                                let slice_to_var_idx = self.cg.add_node(slice_to_var);
                                let var_idx = sym_table.get(ident).unwrap();
                                self.cg
                                    .add_edge(*var_idx, slice_to_var_idx, (EdgeIdx(0), ident));
                                self.cg
                                    .add_edge(t_idx, slice_to_var_idx, (EdgeIdx(0), ident));
                                sym_table.insert_sym(ident.to_string(), slice_to_var_idx);
                                self.comb_types
                                    .insert(ByAddress(ast_node), slice_to_var_idx);
                            }
                            _ => {
                                todo!(
                                    "Provide error for invalid assignment to sliced non-identifier"
                                );
                            }
                        },
                        _ => {
                            dbg!(&ast_node);
                            todo!("Provide error for invalid assignment")
                        }
                    }
                }

                Some(t_idx)
            }
            SigDeclExpr {
                ident,
                typespec,
                default,
                slice,
            } => {
                let def = if let Some(spec) = typespec {
                    let mut high_val: i32 = 0;
                    let mut low_val: i32 = 0;
                    if let TypeSpec {
                        ident,
                        slice: Some(slice),
                    } = &spec.inner
                    {
                        if let Slice { high, low } = &slice.inner {
                            if let NumLitExpr { val } = high.inner {
                                high_val = val;
                            } else {
                                todo!("Error on slice high value not being NumLitExpr")
                            }
                            if let NumLitExpr { val } = low.inner {
                                low_val = val;
                            } else {
                                todo!("Error on slice low value not being NumLitExpr")
                            }
                        }
                    }

                    ComputeNode {
                        ast_node,
                        op: ComputeOp::Def,
                        type_: Some(Type::Logic(high_val, low_val)),
                    }
                } else {
                    ComputeNode {
                        ast_node,
                        op: ComputeOp::UnDef,
                        type_: None,
                    }
                };

                let def_idx = self.cg.add_node(def);

                if let Some(default) = default {
                    let default_idx = self.build_comb_rec(default, sym_table).unwrap();

                    let t = ComputeNode {
                        ast_node,
                        op: ComputeOp::BinOp(Op::Assign),
                        type_: None,
                    };

                    let t_idx = self.cg.add_node(t);
                    self.cg.add_edge(def_idx, t_idx, (EdgeIdx(0), ""));
                    self.cg.add_edge(default_idx, t_idx, (EdgeIdx(0), ""));

                    sym_table.insert_sym(ident.to_string(), t_idx);
                    self.comb_types.insert(ByAddress(ast_node), t_idx);
                    Some(t_idx)
                } else {
                    sym_table.insert_sym(ident.to_string(), def_idx);
                    Some(def_idx)
                }
            }
            SliceExpr { expr, slice } => {
                let expr_idx = self.build_comb_rec(expr, sym_table).unwrap();

                let mut high_val: i32 = 0;
                let mut low_val: i32 = 0;
                if let Slice { high, low } = &slice.inner {
                    if let NumLitExpr { val } = high.inner {
                        high_val = val;
                    } else {
                        todo!("Error on slice high value not being NumLitExpr")
                    }
                    if let NumLitExpr { val } = low.inner {
                        low_val = val;
                    } else {
                        todo!("Error on slice low value not being NumLitExpr")
                    }
                }

                let t = ComputeNode {
                    ast_node,
                    op: ComputeOp::Slice(high_val, low_val),
                    type_: None,
                };
                let t_idx = self.cg.add_node(t);

                self.cg.add_edge(expr_idx, t_idx, (EdgeIdx(0), ""));

                Some(t_idx)
            }
            IfStmt {
                cond,
                body,
                elsebody,
            } => {
                let cond_id = self.build_comb_rec(cond, sym_table).unwrap();
                let mut if_sym = sym_table.clone();
                let mut else_sym = sym_table.clone();

                self.build_comb_rec(body, &mut if_sym);

                if let Some(elsebody) = elsebody {
                    self.build_comb_rec(elsebody, &mut else_sym);
                }

                // TODO fix this very slow implementation that merges the potentiallly diverging sym tables
                let names = sym_table.get_all_names();
                names.iter().for_each(|name| {
                    let if_idx = if_sym.get(name).unwrap();
                    let else_idx = if_sym.get(name).unwrap();

                    if if_idx != else_idx {
                        let t = ComputeNode {
                            ast_node,
                            op: ComputeOp::SelectOn(cond_id),
                            type_: None,
                        };

                        let t_idx = self.cg.add_node(t);
                        self.cg.add_edge(*if_idx, t_idx, (EdgeIdx(0), ""));
                        self.cg.add_edge(*else_idx, t_idx, (EdgeIdx(0), ""));
                    }
                });
                None
            }
            VarExpr { name } => {
                if let Some(type_node) = sym_table.get(name) {
                    Some(*type_node)
                } else {
                    todo!("Report undefined variable access in Comb")
                }
            }
            _ => todo!("Illegal node in Comb block should have been caught earlier"),
        }
    }
}

pub struct TypeChecker {
    cg_queue: VecDeque<NodeIdx>,
    revisit_list: Vec<NodeIdx>,
}

fn sub_type(t0: &Type, t1: &Type) -> Type {
    use std::cmp::*;
    match t0 {
        Type::Logic(high0, low0) => match t1 {
            Type::Logic(high1, low1) => Type::Logic(max(*high0, *high1), min(*low1, *low0)),
            Type::NumLit(val) => Type::Logic(
                max(
                    if *val > 0 {
                        val.abs().ilog2() as i32
                    } else {
                        0
                    },
                    *high0,
                ),
                *low0,
            ),
        },
        Type::NumLit(val) => match t1 {
            Type::Logic(high1, low1) => Type::Logic(max(val.ilog2() as i32, *high1), *low1),
            Type::NumLit(val) => Type::NumLit(0),
        },
    }
}

impl TypeChecker {
    pub fn new() -> Self {
        Self {
            cg_queue: VecDeque::new(),
            revisit_list: Vec::new(),
        }
    }

    pub fn check_types(&mut self, cg: &mut Graph<ComputeNode, (EdgeIdx, &str)>) {
        cg.nodes.iter().enumerate().for_each(|(id, node)| {
            if node.incs.is_empty() {
                self.cg_queue.push_back(NodeIdx(id));
            }
        });

        let mut visited = vec![false; cg.nodes.len()];

        while let Some(nidx) = self.cg_queue.pop_front() {
            if visited[nidx.0] {
                continue;
            }

            // This may be set to false again for join components with incomplete
            // inputs
            visited[nidx.0] = true;

            let node = &cg[nidx];
            let preds: Vec<_> = node
                .incs
                .iter()
                .map(|inc_idx| &cg[cg[*inc_idx].src])
                .collect();
            use ComputeOp::*;

            match node.data.op {
                Merge => {
                    if preds.len() != 2 {
                        todo!("Error on Merge without 2 inputs");
                    }
                    if let (Some(lhs), Some(rhs)) = (&preds[0].data.type_, &preds[1].data.type_) {
                        if lhs != rhs {
                            todo!("Error unequal type merge");
                        }

                        cg[nidx].data.type_ = Some(lhs.clone());
                    } else if let Some(lhs) = &preds[0].data.type_ {
                        cg[nidx].data.type_ = Some(lhs.clone());
                    } else if let Some(rhs) = &preds[1].data.type_ {
                        cg[nidx].data.type_ = Some(rhs.clone());
                    } else {
                        todo!("Error");
                    }
                }
                ComputeOp::Def => {
                    // type must be defined
                    if node.data.type_.is_none() {
                        todo!("Report erorr undefined type in def node");
                    }
                }
                ComputeOp::SelectOn(select_node) => {
                    let sel = &cg[select_node];

                    let Some(Type::Logic(0, 0)) = sel.data.type_
		    else {
			todo!("If select is not on bool value");
		    };

                    if let (Some(lhs), Some(rhs)) = (&preds[0].data.type_, &preds[1].data.type_) {
                        cg[nidx].data.type_ = Some(sub_type(lhs, rhs));
                    } else {
                        todo!("Error");
                    }
                }
                ComputeOp::UnDef => {}
                ComputeOp::Slice(high, low) => {
                    let Some(Type::Logic(l_high, l_low)) = preds[0].data.type_
		    else {
			todo!("Error on slice on non-logic type")
		    };

                    if high > l_high {
                        todo!("Error slice high out of bounds");
                    } else if low < l_low {
                        todo!("Error slice low out of bounds");
                    }

                    if low > high {
                        todo!("Error slice low higher than high");
                    }

                    cg[nidx].data.type_ = Some(Type::Logic(high - low, 0));
                }
                ComputeOp::BinOp(op) => {
                    use Op::*;
                    match op {
                        Op::Assign => match &preds[0].data.op {
                            UnDef => {
                                let Some(rhs) = &preds[1].data.type_ else {
					todo!("Error on wrong rhs in assign");
				    };

                                cg[nidx].data.type_ = Some(rhs.clone());
                            }
                            _ => {
                                let (Some(lhs), Some(rhs)) = (&preds[0].data.type_, &preds[1].data.type_)
			    else { dbg!(&preds[0].data); dbg!(&preds[1].data); todo!("Error on nonexisting type"); };

                                if sub_type(lhs, rhs) == *lhs {
                                    cg[nidx].data.type_ = Some(lhs.clone());
                                } else {
                                    todo!("Error on assigning non subtype {:?} to {:?}", rhs, lhs);
                                }
                            }
                        },
                        Eq | Neq | Negate | Lt | Lte | Gt | Gte | And | Or => {
                            let (Some(lhs), Some(rhs)) = (&preds[0].data.type_, &preds[1].data.type_)
			    else { todo!(); };

                            if lhs != rhs {
                                todo!("Error unequal type merge");
                            }

                            cg[nidx].data.type_ = Some(Type::Logic(0, 0));
                        }
                        Op::Band | Bor | Minus | Plus | Mul | Div => {
                            let (Some(lhs), Some(rhs)) = (&preds[0].data.type_, &preds[1].data.type_)
			    else { todo!(); };

                            cg[nidx].data.type_ = Some(sub_type(lhs, rhs));
                        }
                        _ => {
                            todo!(
                                "Illegal operation in type graph should have been caught earlier"
                            );
                        }
                    }
                }
            }

            let node = &cg[nidx];
            node.outs.iter().for_each(|out_idx| {
                let target_node = &cg[cg[*out_idx].dst];
                let preds_visited = target_node
                    .incs
                    .iter()
                    .all(|inc_idx| visited[cg[*inc_idx].src.0]);
                if preds_visited || matches!(target_node.data.op, Merge) {
                    self.cg_queue.push_back(cg[*out_idx].dst);
                }
            });
        }
    }
}

fn print_dot_compute_node(c: &GraphNode<ComputeNode>, source: &str, idx: usize) {
    use ComputeOp::*;
    let op = match &c.data.op {
        Merge { .. } => "Merge".to_string(),
        SelectOn(val) => format!("Select on {:?}", val),
        ComputeOp::Def => "Def".to_string(),
        ComputeOp::Slice(high, low) => format!("[{high}:{low}"),
        ComputeOp::BinOp(op) => format!("{:?}", op),
        ComputeOp::UnDef => "UnDef".to_string(),
    };

    let node_source = &source[c.data.ast_node.span.start..c.data.ast_node.span.end];

    println!(
        "{idx} [label=\"{idx}\n{op}\n{:?}\n{node_source}\"];",
        c.data.type_
    );
}

fn print_dot_edge(e: &GraphEdge<(EdgeIdx, &str)>) {
    println!(
        "{} -> {} [label=\"{} {}\"];",
        e.src.0, e.dst.0, e.data.0 .0, e.data.1
    );
}

pub fn print_dot(g: &Graph<ComputeNode, (EdgeIdx, &str)>, source: &str) {
    println!("Digraph G {{");
    g.nodes
        .iter()
        .enumerate()
        .for_each(|(idx, node)| print_dot_compute_node(node, source, idx));
    g.edges.iter().for_each(print_dot_edge);
    println!("}}");
}

fn print_dot_controller_typed(c: &cst::ControllerNode, idx: usize) {
    use ControllerKind::*;
    let node_type = match &c.data.kind {
        Merge { .. } => "Merge",
        Join { .. } => "Join",
        Fork { .. } => "Fork",
        Mux { .. } => "Mux",
        Demux { .. } => "Demux",
        Reg { .. } => "Reg",
        Comb { .. } => "Comb",
        NamedChan { .. } => "NamedChan",
        Input { .. } => "Input",
        Output { .. } => "Output",
        Sink { .. } => "Sink",
        Source { .. } => "Source",
        Blackbox { .. } => "Blackbox",
    };
    let reqs = c
        .data
        .requires
        .iter()
        .fold("".to_string(), |lhs, (name, sig)| {
            format!("{}, {}", lhs, *name)
        });

    let provs = c
        .data
        .provides
        .iter()
        .fold("".to_string(), |lhs, (name, sig)| {
            format!("{}, {}", lhs, *name)
        });
    println!("{idx} [label=\"{node_type}\nrequires: {reqs}\nprovides: {provs}\n\"];");
}

fn print_dot_channel_typed(
    e: &cst::ChannelEdge,
    idx: EdgeIdx,
    sig_table: &HashMap<(EdgeIdx, &str), NodeIdx>,
    cg: &Graph<ComputeNode, (EdgeIdx, &str)>,
) {
    let reqs = e
        .data
        .requires
        .iter()
        .fold("".to_string(), |lhs, (name, sig)| {
            format!("{}, {}", lhs, *name)
        });

    let provs = e
        .data
        .provides
        .iter()
        .fold("".to_string(), |lhs, (name, sig)| {
            let type_string = format!(
                "{:?}",
                &cg[*sig_table.get(&(idx, *name)).unwrap()].data.type_
            );
            format!("{}, {} : {}", lhs, *name, type_string)
        });

    let port_type = match e.data.dst_port {
        cst::Port::Flow(..) => "flow",
        cst::Port::Select() => "select",
    };

    println!(
        "{} -> {} [label=\"type: {port_type}\nrequires: {reqs}\nprovides: {provs}\"];",
        e.src.0, e.dst.0
    );
}

fn print_dot_typed(
    g: &Graph<cst::Controller, cst::Channel>,
    sig_table: &HashMap<(EdgeIdx, &str), NodeIdx>,
    cg: &Graph<ComputeNode, (EdgeIdx, &str)>,
) {
    println!("Digraph G {{");
    g.nodes
        .iter()
        .enumerate()
        .for_each(|(idx, node)| print_dot_controller_typed(node, idx));
    g.edges
        .iter()
        .enumerate()
        .for_each(|(idx, edge)| print_dot_channel_typed(edge, EdgeIdx(idx), sig_table, cg));
    println!("}}");
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::token;
    use chumsky::extra;
    use chumsky::prelude::*;
    use extra::Err;
    use std::eprint;
    use std::matches;

    #[test]
    fn propagate_join_cg() {
        let source = r###"
{
input chan a : {sig a : logic[12:0]};
input chan b : {sig b : logic[12:0]};
output chan c : {sig a : logic[12:0], sig b : logic[12:0]};

[a, b] -> join() -> c;

}

"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        println!("{:?}", tokens);
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = cst::CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());

                builder.propagate2();
                cst::print_dot(&builder.g);
                assert!(cst::test::all_needs_met(&mut builder.g));

                let mut cg_builder = CgBuilder::new();
                cg_builder.build_from_cst(&builder.g);
                print_dot(&cg_builder.cg, source);

                let mut typer = TypeChecker::new();
                typer.check_types(&mut cg_builder.cg);
                print_dot(&cg_builder.cg, source);
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }

    #[test]
    fn propagate_fork_cg() {
        let source = r###"
{
output chan a : {sig a : logic[12:0]};
output chan b : {sig b : logic[12:0]};
input chan c : {sig a : logic[12:0], sig b : logic[12:0]};

c -> fork() -> [a, b];

}

"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = cst::CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());

                builder.propagate2();
                cst::print_dot(&builder.g);
                assert!(cst::test::all_needs_met(&mut builder.g));

                let mut cg_builder = CgBuilder::new();
                cg_builder.build_from_cst(&builder.g);
                print_dot(&cg_builder.cg, source);

                let mut typer = TypeChecker::new();
                typer.check_types(&mut cg_builder.cg);
                print_dot(&cg_builder.cg, source);
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }

    #[test]
    fn propagate_gcd_cg() {
        let source = r###"
        {
        chan loop_s;
        
        input chan a : {sig a : logic[12:0]};
        input chan b : {sig b : logic[12:0]};
        
        [[a, b] -> join(), chan loop_val] -> mux(loop_s) -> fork() ->
        [
                comb {sig s : logic[0:0] = a != b;} -> fork() -> [reg(sig s : logic[0:0] = 1) -> loop_s, chan output_s],
                demux(output_s) -> [output chan o : {sig a : logic[12:0], sig b : logic[12:0]},
                                   fork() -> [comb {sig s : logic[0:0] = a > b;} -> chan branch_s,
                                             demux(branch_s) -> [
                                                                comb {a[7:0] = a[7:0] - b[7:0];},
                                                                comb {b = b - a;}
                                                                ]
                                             -> merge() -> loop_val
                                             ]
                                   ]
        ];
        }
"###;

        let tokens = token::make_lexer().parse(source).unwrap();
        let output = ast::parse_structural_stmt().parse(
            tokens
                .as_slice()
                .spanned((tokens.len()..tokens.len()).into()),
        );
        // Parse the token stream with our chumsky parser
        match output.into_result() {
            // If parsing was successful, attempt to evaluate the s-expression
            Ok(ast) => {
                let mut builder = cst::CstBuilder::new();
                let _ = builder.build_from_structural_node(&ast);

                builder.diagnostics.iter().for_each(|diag| {
                    let _ = diag.print(Source::from(source));
                });
                assert!(builder.diagnostics.is_empty());

                builder.propagate2();
                cst::print_dot(&builder.g);
                assert!(cst::test::all_needs_met(&mut builder.g));

                let mut cg_builder = CgBuilder::new();
                cg_builder.build_from_cst(&builder.g);
                print_dot(&cg_builder.cg, source);

                let mut typer = TypeChecker::new();
                typer.check_types(&mut cg_builder.cg);
                print_dot(&cg_builder.cg, source);
                print_dot_typed(&builder.g, &cg_builder.sig_table, &cg_builder.cg);
            }
            // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
            // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
            // with Rust's built-in `Display` trait, but it's a little crude
            Err(errs) => {
                for err in errs {
                    Report::build(ReportKind::Error, (), err.span().start)
                        .with_code(3)
                        .with_message(err.to_string())
                        .with_label(
                            Label::new(err.span().into_range())
                                .with_message(err.reason().to_string())
                                .with_color(Color::Red),
                        )
                        .finish()
                        .eprint(Source::from(source))
                        .unwrap();
                }
            }
        }
    }
}
