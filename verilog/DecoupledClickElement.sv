module DecoupledClickElement
  #(parameter PI = 0,
    parameter PO = 0)
  ( input Ri, Ao, rst,
    output Ro, Ai, clk );

   wire                               xorleft;
   wire                               xnorright;
   wire                               clickAnd;
   reg                                Pi, Po;

   //assign #1 xorleft = Ri ^ Ai;
   //assign #1 xnorright = Ro ~^ Ao;
   //assign #1 clickAnd = xorleft & xnorright;
   assign xorleft = Ri ^ Ai;
   assign xnorright = Ro ~^ Ao;
   assign #16 clickAnd = xorleft & xnorright & ~rst;
   assign clk = clickAnd;
   assign #2 Ai = Pi;
   assign #2 Ro = Po;

   always @ (posedge clickAnd, posedge rst) begin
      if (rst == 1'b1)
        begin
           Pi <= PI;
           Po <= PO;
        end
      else
        begin
           Pi <= ~Pi;
           Po <= ~Po;
        end
   end

endmodule
