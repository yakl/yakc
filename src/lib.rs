#![allow(dead_code, unused_variables, unused_imports)]

use ariadne::{Color, Label, Report, ReportKind, Source};
use std::path::PathBuf;
use std::{fmt, ops::Range};

use chumsky::prelude::*;
pub mod ast;
pub mod cg;
pub mod cst;
pub mod graph;
pub mod hdl;
pub mod sdc;
pub mod token;
pub mod util;

pub fn run(source: &String, vlog: Option<PathBuf>, sdc: Option<PathBuf>) {
    let tokens = token::make_lexer().parse(source).unwrap();
    let output = ast::parse_structural_stmt().parse(
        tokens
            .as_slice()
            .spanned((tokens.len()..tokens.len()).into()),
    );
    // Parse the token stream with our chumsky parser
    match output.into_result() {
        // If parsing was successful, attempt to evaluate the s-expression
        Ok(ast) => {
            let mut builder = cst::CstBuilder::new();
            let _ = builder.build_from_structural_node(&ast);

            builder.diagnostics.iter().for_each(|diag| {
                let _ = diag.print(Source::from(source));
            });
            assert!(builder.diagnostics.is_empty());

            builder.propagate2();

            let mut cg_builder = cg::CgBuilder::new();
            cg_builder.build_from_cst(&builder.g);

            let mut typer = cg::TypeChecker::new();
            typer.check_types(&mut cg_builder.cg);

            use std::fs::File;
            use std::io::Write;

            let mut hdl_builder = hdl::HdlBuilder {
                comb_types: &cg_builder.comb_types,
                cg: &cg_builder.cg,
                sig_table: &cg_builder.sig_table,
            };
            let top_module = hdl_builder.build_from_cst(&builder.g);

            if let Some(verilog_file) = vlog {
                let mut writer = hdl::HdlWriter {
                    source: String::new(),
                };
                writer.build_from(&top_module);
                let mut f = File::create(verilog_file).unwrap();
                f.write_all(writer.source.as_bytes()).unwrap();
            }
            if let Some(sdc_file) = sdc {
                let mut f = File::create(sdc_file).unwrap();
                use crate::sdc::GenerateSdc;
                f.write_all(
                    crate::sdc::DecoupledClickBuilder::generate_sdc_from(&builder.g).as_bytes(),
                )
                .unwrap();
            }
        }
        // If parsing was unsuccessful, generate a nice user-friendly diagnostic with ariadne. You could also use
        // codespan, or whatever other diagnostic library you care about. You could even just display-print the errors
        // with Rust's built-in `Display` trait, but it's a little crude
        Err(errs) => {
            for err in errs {
                Report::build(ReportKind::Error, (), err.span().start)
                    .with_code(3)
                    .with_message(err.to_string())
                    .with_label(
                        Label::new(err.span().into_range())
                            .with_message(err.reason().to_string())
                            .with_color(Color::Red),
                    )
                    .finish()
                    .eprint(Source::from(source))
                    .unwrap();
            }
        }
    }
}
